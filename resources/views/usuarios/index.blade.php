@extends('principal')

@section('titulo')
  <title>Usuarios</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Usuarios</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-usuario')
        {!!link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($usuarios) == 0)
          <p class="text-info">
            No se han registrado usuarios aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($usuarios as $key => $usuario)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($usuario->name) }}
                    </td>
                    <td>
                      {{ $usuario->email }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!! $usuarios->links() !!}
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
