@extends('principal')

@section('titulo')
  <title>Nuevo Usuarios</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Usuario</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'usuarios.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nusuario'])!!}
          @include('usuarios.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
