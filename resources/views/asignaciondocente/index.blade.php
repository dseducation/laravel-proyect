@extends('principal')

@section('titulo')
  <title>Asignación Docentes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Asignación Docentes</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-asignacion')
          {!!link_to_route('asignaciondocente.create', $title = 'Nueva Asignación', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
          <br>
          <br>
        @endpermission
        @if (count($docentes) == 0)
          <p class="text-info">
            No se han registrado asignaciónes de docentes aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    DOCENTE
                  </th>
                  <th>
                    ACTUALIZAR
                  </th>
                  <th>
                    VER ASIGNACIÓN
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($docentes as $key => $docente)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona) }}
                    </td>
                    <td>
                      {!!link_to_route('asignaciondocente.edit', $title = 'Editar Asignación', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    <td>
                      {!!link_to_route('asignaciondocente.show', $title = 'Ver Cursos', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-warning'])!!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
