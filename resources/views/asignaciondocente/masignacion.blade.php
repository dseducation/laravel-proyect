@extends('principal')

@section('titulo')
  <title>Asignación Docente</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Asignación Docente</h1>
      </div>
      <div class="col-sm-12">
        <h2>Docente: <small>{{ mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona) }}</small></h2>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CURSO
                </th>
                <th>
                  GRADO
                </th>
                <th>
                  CARRERA
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  JORNADA
                </th>
                <th>
                  PLAN
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($asignaciones as $key => $asignacion)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_area) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_grado) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_carrera) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_nivel) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_jornada) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($asignacion->nombre_plan) }}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
