@extends('principal')

@section('titulo')
  <title>Nueva Respuesta</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Respuesta</h1>
        @include('mensajes.errores')
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
      </div>
      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{ $foro->titulo_foro }}</h3>
          </div>
          <div class="panel-body">
            {!! $foro->mensaje_foro !!}
          </div>
        </div>
      </div>

      <div class="col-sm-12">

        {!!Form::open(['route'=>'respuestasforos.store', 'method'=>'POST', 'class'=>'form-horizontal','id'=>'respuestasforos'])!!}
          {!!Form::hidden('foro', $foro->id_foro)!!}


          <div class="form-group">
            {!!Form::label('mensaje', 'Mensaje*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {!!Form::textarea('mensaje_respuesta', null, ['class'=>'form-control', 'placeholder'=>'Mensaje de respuesta...', 'id'=>'descripcionForo'])!!}
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>
            </div>
          </div>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
