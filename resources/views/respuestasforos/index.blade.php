@extends('principal')

@section('title')
  <title>Repsuesta del Foro</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Respuestas del Foro</h1>
      </div>
      <div class="col-sm-12">

        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title">{{ $foro->titulo_foro }}</h3>
          </div>
          <div class="panel-body">
            {!! $foro->mensaje_foro !!}
          </div>
          <div class="panel-body">
            {!! $foro->mensaje_foro !!}
          </div>
        </div>
      </div>
      @foreach ($respuestas as $respuesta)

      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{ ucwords($respuesta['nombre'].' '.$respuesta['apellido']) }}</h3>
          </div>
          <div class="panel-body">
            {!! $respuesta['respuesta'] !!}
          </div>
          <div class="panel-footer text-right">
            {{ $respuesta['fecha'] }}
          </div>
        </div>
      </div>
    @endforeach

    </div>
  </div>
@endsection
