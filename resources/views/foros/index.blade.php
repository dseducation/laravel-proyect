@extends('principal')

@section('titulo')
  <title>Foros</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Foros</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <p>
          {!!link_to_route('foros.create', $title = ' Nuevo Foro', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}
        </p>
        @if (count($foros) == 0)
          <p class="text-info">
            No se han creado foros en este curso...
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    TITULO
                  </th>
                  <th>
                    EDITAR
                  </th>
                  <th>
                    RESPONDER
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($foros as $key=>$foro)
                  <tr>
                    <td>
                        {{ $key+1 }}
                    </td>
                    <td>
                      {!!link_to_route('respuestasforos.show', $title = $foro->titulo_foro, $parameters = $foro->id_foro, $attributes= null)!!}
                    </td>
                    <td>
                      @can('owner', $foro)
                      {!!link_to_route('foros.edit', $title = ' Editar', $parameters = $foro->id_foro, $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}
                      @endcan
                    </td>
                    <td>
                      {!!link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments'])!!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
