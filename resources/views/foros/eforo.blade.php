@extends('principal')

@section('titulo')
  <title>Editar Foro</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Foro</h1>
        @include('mensajes.errores')
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
      </div>
      <div class="col-sm-12">
        {!!Form::model($foro, ['route'=>['foros.update',$foro->id_foro], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'foros'])!!}
          <input type="hidden" name="curso" value="0">
          @include('foros.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
