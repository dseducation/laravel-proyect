<div class="form-group">
  {!!Form::label('titulo', 'Titulo*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('titulo_foro', null, ['class'=>'form-control', 'placeholder'=>'Titulo del foro'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('descripcion', 'Descripción*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::textarea('mensaje_foro', null, ['class'=>'form-control', 'placeholder'=>'Descripción del foro...', 'id'=>'descripcionForo'])!!}
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>
  </div>
</div>
