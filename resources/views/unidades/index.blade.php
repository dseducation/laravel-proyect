@extends('principal')

@section('titulo')
  <title>Unidades</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Unidades</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        {!!link_to_route('unidades.create', $title = ' Nueva Unidad', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}
        <br>
        <br>
      </div>
      <div class="col-sm-12">
        @if(count($unidades) ==  0)
          <p class="text-info">
            No se han registrado unidades aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                  <th>
                    FECHA_INICIO
                  </th>
                  <th>
                    FECHA_FINAL
                  </th>
                  <th>
                    EDITAR
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($unidades as $key => $unidad)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ ucwords($unidad->nombre_unidad) }}
                    </td>
                    <td>
                      {{ $unidad->fecha_inicio }}
                    </td>
                    <td>
                      {{ $unidad->fecha_final }}
                    </td>
                    <th>
                      {!!link_to_route('unidades.edit', $title = ' Editar', $parameters = $unidad->id_unidad, $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}
                    </th>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
