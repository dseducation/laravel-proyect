 <ul class="{{ $class }}" id="side-menu">
   <li>
       <a href="{{ url('/') }}"><i class="fa fa-home fa-fw"></i> Página de Inicio</a>
   </li>
    @foreach ($items as $item)
        <li @if ($item['class']) class="{{ $item['class'] }}" @endif id="menu_{{ $item['id'] }}">
            @if (empty($item['submenu']))
                <a href="#">
                    {{ $item['title'] }}
                </a>
            @else
              {{--   <a href="{{ $item['url'] }}" class="dropdown-toggle" data-toggle="dropdown"> --}}
              <a href="{{ url('/').$item['url'] }}">
                    {{ $item['title'] }}
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    @foreach ($item['submenu'] as $subitem)
                        <li>
                            <a href="{{ $subitem['url'] }}">{{ $subitem['title'] }}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>
