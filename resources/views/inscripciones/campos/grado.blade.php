<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">Grado Estudiante</div>
  <div class="panel-body">

    <div class="form-group">
      {!!Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label'])!!}
      <div class="col-sm-10">
        {!!Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione plan...'])!!}
      </div>
    </div>
    <div class="form-group">
      {!!Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label'])!!}
      <div class="col-sm-10">
        {!!Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione jornada...'])!!}
      </div>
    </div>
    <div class="form-group">
      {!!Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label'])!!}
      <div class="col-sm-10">
        {!!Form::select('id_nivel', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione nivel...', 'id'=>'nIEstudiante'])!!}
      </div>
    </div>
    <div class="form-group">
      {!!Form::label('grado', 'Grado*', ['class'=>'col-sm-2 control-label'])!!}
      <div class="col-sm-10">
        {!!Form::select('id_grado', array(), null, ['class'=>'form-control', 'placeholder'=>'Seleccione grado...'])!!}
      </div>
    </div>


  </div>
</div>
