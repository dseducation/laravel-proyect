@extends('principal')

@section('titulo')
  <title>Datos Estudiante</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Datos Estudiante</h1>
      </div>
      <div class="col-sm-12">
          <ul>
              <li><strong>Nombre(s):</strong> {{ ucwords($estudiante->nombre_estudiante) }}</li>
              <li><strong>Apellido(s):</strong> {{ ucwords($estudiante->apellidos_estudiante) }}</li>
              <li><strong>FechaNacimiento:</strong> {{ ucwords($estudiante->fecha_nacimiento_estudiante) }}</li>
              <li><strong>Genero:</strong> {{ ucwords($estudiante->genero_estudiante) }}</li>
              <li><strong>Direccion:</strong> {{ $estudiante->direccion_estudiante }}</li>
              <li><strong>Telefono de casa:</strong> {{ $estudiante->telefono_casa }}</li>
              <li><strong>Empresa Telefonica:</strong> {{ $estudiante->empresa_telefonica }}</li>
          </ul>
      </div>
    </div>
  </div>
@endsection
