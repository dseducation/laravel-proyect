@extends('principal')

@section('titulo')
  <title>Inscripciones Estudiantes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Inscripciones Estudiantes</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        {!!link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}

        <table class="table table-hover">
            <thead>
              <tr>
              <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                <th>
                  APELLIDO 
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  CORREO 
                </th>
                @permission('editar-grado')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-grado')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody >
              @foreach($cursos as $key => $grado)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($grado->nombre_estudiante) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->apellidos_estudiante) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->telefono_casa) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->correo_estudiante) }}
                  </td>
                  @permission('editar-grado')
                  <td>
                    {!!link_to_route('estudiantes.edit', $title = 'Editar', $parameters = $grado->id_estudiante, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-grado')
                  <td>
                    @if($grado->estado_grado == true)
                      {!!link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado])!!}
                    @else
                      {!!link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>


          </table>
           <div class="text-center">
            {!! $cursos->links() !!}
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
      
      </div>
    </div>
  </div>
@endsection