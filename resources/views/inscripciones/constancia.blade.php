@extends('principal')



@section('titulo')

  <title>Constancia de Inscripción</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Constancia de Inscripción</h1>

        @include('mensajes.msg');

      </div>

        <div id="visualizador-pdf" style="height: 28em; padding:0; border: 10px solid rgba(0,0,0,.2);" class="col-sm-12">

          <!-- visualizador de pdf incrustado en la pagina -->



        </div>

    

      <script>PDFObject.embed("{{ url('/') }}/report", "#visualizador-pdf");</script>

    </div>

  </div>

@endsection

