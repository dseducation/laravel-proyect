@extends('principal')

@section('titulo')
  <title>Carreras</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Carreras</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-carrera')
          {!!link_to_route('carreras.create', $title = 'Nueva Carrera', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        @endpermission
        <br>
        <br>
        @if(count($carreras) == 0)
          <p class="text-info">
            No se han registrado carreras aun.
          </p>
        @else
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CARRERA
                </th>
                @permission('editar-carrera')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                <th>
                  ESTADO
                </th>
              </tr>
            </thead>
            <tbody id="datosCarreras">
              @foreach($carreras as $key => $carrera)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($carrera->nombre_carrera) }}
                  </td>
                  @permission('editar-carrera')
                  <td>
                    {!!link_to_route('carreras.edit', $title = 'Editar', $parameters = $carrera->id_carrera, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  <td>
                    @if($carrera->estado_carrera)
                      {!!link_to_route('carreras.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarCarrera', 'data-id'=>$carrera->id_carrera, 'data-estado'=>$carrera->estado_carrera])!!}
                    @else
                      {!!link_to_route('carreras.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarCarrera', 'data-id'=>$carrera->id_carrera, 'data-estado'=>$carrera->estado_carrera])!!}
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>
    </div>
  </div>
@endsection
