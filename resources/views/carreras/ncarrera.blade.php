@extends('principal')

@section('titulo')
  <title>Nueva Carrera</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Carrera</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'carreras.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'ncarrera'])!!}
          @include('carreras.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
