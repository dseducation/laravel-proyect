@extends('principal')

@section('titulo')
  <title>Nuevo Nivel - Plan</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Nivel - Plan</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'asignarniveles.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nasignarnivel'])!!}
          @include('nivelesplanesjornadas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
