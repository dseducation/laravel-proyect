@extends('principal')

@section('titulo')
  <title>Planes Niveles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Planes Niveles</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-plannivel')
        {!!link_to_route('asignarniveles.create', $title = 'Nuevo Plan - Nivel', $parameters = null, $attributes =['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($datos) == 0)
          <p class="text-info">
            No se han registrado asignacioónes de de niveles a planes y jornadas.
          </p>
        @else
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  JORNADA
                </th>
                @permission('editar-plannivel')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosAsignacion">
              @foreach($datos as $key => $row)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_nivel) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_plan) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_jornada) }}
                  </td>
                  @permission('editar-plannivel')
                  <td>
                    {!!link_to_route('asignarniveles.edit', $title = 'Editar', $parameters = $row->id_nivel_plan_jornada, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
        @endif
      </div>
    </div>
  </div>
@endsection
