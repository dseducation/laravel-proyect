@extends('principal')

@section('titulo')
  <title>Editar Plan - Nivel</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Plan - Nivel</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($dato, ['route'=>['asignarniveles.update', $dato->id_nivel_plan_jornada], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nasignarnivel'])!!}
          @include('nivelesplanesjornadas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
