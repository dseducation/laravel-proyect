@extends('principal')

@section('titulo')
  <title>Asignar Notas a Estudiantes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Asignar Notas</h1>
        <h3 class="text-center text-info">{{ mb_strtoupper($unidad->nombre_unidad) }}</h3>
      </div>
      <div class="col-sm-12">
        <ul>
          @foreach($datos as $key => $row)
            <li>{{ $row }}</li>
          @endforeach
        </ul>
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              
              <tr>
                <th>
                  NO
                </th>
                <th>
                  ESTUDIANTE
                </th>
                <th>
                  ZONA
                </th>
                <th>
                  EXAMEN
                </th>
                <th>
                  TOTAL
                </th>
                 <th>
                  OPCIONES
                </th>

              </tr>
            </thead>
            <tbody>
              @foreach($nomina as $key => $estudiante)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ ucwords($estudiante->apellidos_estudiante.' '.$estudiante->nombre_estudiante) }}
                  </td>
                  <td>   
                  </td>
                  <td>        
                  </td>
                  <td>
                  </td>
                  <td>
                     {!!link_to_route('docente.edit', $title = 'Editar', $parameters = $estudiante->id_estudiante, $attributes = ['class'=>'btn btn-primary'])!!}
              
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
