@extends('principal')

@section('titulo')
  <title>Editar Curso</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Curso</h1>
        <p>
          Nota: Todos los campos con (*) son olbigatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($area, ['route'=>['areas.update', $area->id_area], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'narea'])!!}
          @include('areas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
