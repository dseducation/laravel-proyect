@extends('principal')

@section('titulo')
  <title>Nuevo Curso</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Curso</h1>
        <p>
          Nota: Todos los campos con (*) son oolbigatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'areas.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'narea'])!!}
          @include('areas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
