@extends('principal')

@section('titulo')
  <title>Cursos</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Cursos</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-curso')
        {!!link_to_route('areas.create', $title = 'Nuevo Curso', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($areas) == 0)
          <p class="text-info">
            No se han registrado cursos aun.
          </p>
        @else
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                @permission('editar-curso')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-curso')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosAreas">
              @foreach($areas as $key => $area)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($area->nombre_area) }}
                  </td>
                  @permission('editar-curso')
                  <td>
                    {!!link_to_route('areas.edit', $title = 'Editar', $parameters = $area->id_area, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-curso')
                  <td>
                    @if($area->estado_area == true)
                      {!!link_to_route('areas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarArea', 'data-id'=>$area->id_area, 'data-estado'=>$area->estado_area])!!}
                    @else
                      {!!link_to_route('areas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarArea', 'data-id'=>$area->id_area, 'data-estado'=>$area->estado_area])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
          <div class="text-center">
            {!! $areas->links() !!}
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
