@extends('principal')

@section('titulo')
  <title>Editar Jornada</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Jornada</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($jornada, ['route'=>['jornadas.update', $jornada->id_jornada], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'njornada'])!!}
          @include('jornadas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
    @include('mensajes.carga')
  </div>
@endsection
