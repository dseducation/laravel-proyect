@extends('principal')

@section('titulo')
  <title>Nueva Jornada</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Jornada</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'jornadas.store', 'methd'=>'POST', 'class'=>'form-horizontal', 'id'=>'njornada'])!!}
          @include('jornadas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
