@extends('principal')

@section('titulo')
  <title>Jornadas</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Jornadas</h1>
          @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-jornada')
        {!!link_to_route('jornadas.create', $title = 'Nueva Jornada', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($jornadas) == 0)
          <p class="text-info">
            No se han registrado jornadas aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    JORNADA
                  </th>
                  @permission('editar-jornada')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado_jornada')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosJornadas">
                @foreach($jornadas as $key => $jornada)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($jornada->nombre_jornada) }}
                    </td>
                    @permission('editar-jornada')
                    <td>
                      {!!link_to_route('jornadas.edit', $title = 'Editar', $parameters = $jornada->id_jornada, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    @endpermission
                    @permission('estado_jornada')
                    <td>
                      @if($jornada->estado_jornada == true)
                          {!!link_to_route('jornadas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarJornada', 'data-id'=>$jornada->id_jornada, 'data-estado'=>$jornada->estado_jornada])!!}
                      @else
                          {!!link_to_route('jornadas.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarJornada', 'data-id'=>$jornada->id_jornada, 'data-estado'=>$jornada->estado_jornada])!!}
                      @endif
                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
    </div>
    <!-- Mensaje de carga-->
    @include('mensajes.carga')
  </div>
@endsection
