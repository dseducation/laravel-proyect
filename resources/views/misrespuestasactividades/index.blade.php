@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">TAREAS RESPONDIDAS</h1>

        @include('mensajes.msg')

      </div>

      <div class="col-sm-12">

        <p>

          

        </p>

        @if (count($ractividades) == 0)

          <p class="text-info">

            No se han encontrado actividades para este curso...

          </p>

        @else

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th class="glyphicon glyphicon-user">

                    NOMBRE

                  </th>

                  <th>

                    ESTADO DE ENTREGA

                  </th>

                  <th class="btn-warning" id="nota">

                    CALIFICACION

                  </th>

                  <th class="btn-danger" id="nota_max">

                    NOTA MAXIMA 

                  </th>




                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    COMENTARIOS

                  </th>

                   <th>

                    ARCHIVO

                  </th>

                 

                  <th >

                    CALIFICAR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach ($ractividades as $key=>$actividad)

                @foreach ($notas as $key=>$maximanota)

                  <tr>

                    <td>

                        {{ $key+1 }}

                    </td>

                  <td>
                    {{ mb_strtoupper($actividad->name) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->estado_entrega_actividad) }}

                  </td>

                  <td>

                    {{ $nota=mb_strtoupper($actividad->calificacion) }}

                  </td>


                  <td>

                    {{ $nota_total=mb_strtoupper($maximanota->nota_total) }}

                  </td>



                  @if ($nota>$nota_total)

                  <p> solo uno </p>

                  @else

                  <p> nota total mayor nota </p>

                  @endif





                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->comentarios) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->ruta_actividad) }}

                  </td>

                    

                    <td>

                      {!!link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['a'=>$actividad->id_respuesta_actividad,'b'=>$maximanota->nota_total], $attributes = ['class'=>'btn btn-info '])!!}

                    </td>

                  </tr>

                @endforeach

                @endforeach


              </tbody>

            </table>

               

             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          </div>

        @endif
      
      </div>

    </div>

  </div>

@endsection

