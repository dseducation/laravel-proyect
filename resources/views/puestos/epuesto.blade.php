@extends('principal')

@section('titulo')
  <title>Editar Puesto</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-hader text-center">Editar Puesto</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($puesto, ['route'=>['puestos.update', $puesto->id_puesto], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'npuesto'])!!}
          @include('puestos.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
