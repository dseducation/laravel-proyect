@extends('principal')

@section('titulo')
  <title>Puestos</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Puestos</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-puesto')
        {!!link_to_route('puestos.create', $title = 'Nuevo Puesto', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($puestos) == 0)
          <p class="text-info">
            No se han registrado puestos aun.
          </p>
        @else
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                @permission('editar-puesto')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-puesto')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosPuestos">
              @foreach($puestos as $key => $puesto)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <th>
                    {{ mb_strtoupper($puesto->nombre_puesto) }}
                  </th>
                  @permission('editar-puesto')
                  <th>
                    {!!link_to_route('puestos.edit', $title = 'Editar', $parameters = $puesto->id_puesto, $attributes = ['class'=>'btn btn-primary'])!!}
                  </th>
                  @endpermission
                  @permission('estado-puesto')
                  <th>
                    @if($puesto->estado_puesto == true)
                      {!!link_to_route('puestos.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto])!!}
                    @else
                      {!!link_to_route('puestos.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto])!!}
                    @endif
                  </th>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
      @include('mensajes.carga')
    </div>
  </div>
@endsection
