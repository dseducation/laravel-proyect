@extends('principal')

@section('titulo')
  <title>Nuevo Puesto</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-heade text-center">Nuevo Puesto</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'puestos.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'npuesto'])!!}
          @include('puestos.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
