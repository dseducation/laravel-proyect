@extends('principal')

@section('titulo')
  <title>Editar Pensum Grado</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Pensum Grado</h1>
        <h3 class="text-center">{{ ucwords($pensum[0]->nombre_grado.' '.$pensum[0]->nombre_nivel.' '.$pensum[0]->nombre_carrera) }}</h3>
      </div>
      <div class="col-sm-12">
        <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#formModal"><span class="fa fa-plus-circle"></span> Agregar Curso</button>

        <br>
        <br>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>
                NO
              </th>
              <th>
                CURSO
              </th>
              <th>
                SALON
              </th>
              <th>
                ESTADO
              </th>
            </thead>
            <tbody id="datosPensum">
              @foreach($pensum as $key => $p)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($p->nombre_area) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($p->nombre_salon) }}
                  </td>
                  <td>
                    @if ($p->estado_asignacion_area == TRUE)
                      <input type="checkbox" name="_estado" value="1" checked="TRUE" class="toggleEstado" data-id="{{ $p->id_asignacion_area }}" data-estado="{{ $p->estado_asignacion_area }}">
                    @else
                      <input type="checkbox" name="_estado" value="1" class="toggleEstado" data-id="{{ $p->id_asignacion_area }}" data-estado="{{ $p->estado_asignacion_area }}">
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
          {!!Form::hidden('_id', $pensum[0]->id_nivel_grado, ['id'=>'nivelGrado'])!!}
        </div>
      </div>
    </div>
    <!-- Formulario Modal para agregar más cursos -->
    <!-- Modal -->
      <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Cursos</h4>
          </div>
          <div class="modal-body">
            <form>
              <p>
                Nota: Todos los campos con (*) son requeridos.
              </p>
              <div class="form-group">
                {!!Form::label('salon', 'Salon*')!!}
                {!!Form::select('id_salon', $salones, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un salon...', 'id'=>'salon'])!!}
              </div>
              <div class="form-group">
                {!!Form::label('buscar', 'Buscar*', ['class'=>'col-sm-2 control-label'])!!}

                  <div class="input-group">
                    {!!Form::text('buscar', null, ['class'=>'form-control', 'placeholder'=>'Nombre del curso a buscar...', 'id'=>'autocomplete', 'data-id'=>''])!!}
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button" id="borrarBuscar"><span class="glyphicon glyphicon-remove"></span></button>
                      <button class="btn btn-default" type="button" id="agregarCurso"><span class="fa fa-check"></span> Agregar</button>
                    </span>
                  </div>

              </div>

              <!-- Lugar donde se mostraran los cursos que se van a asignar al pensum del grado -->
              <div class="col-sm-6" id="cursosPensum">

              </div>

            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" id="agregarNuevoCurso"><span class="fa fa-save"></span> Guardar</button>
          </div>
        </div>
      </div>
      </div>
    <!-- Fin del formumario para agregar más cursos -->
  </div>
@endsection
