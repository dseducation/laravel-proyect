@extends('principal')

@section('titulo')
  <title>Pensum Grados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Pensum Grados</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-pensum')
        {!!link_to_route('pensum.create', $title = 'Nuevo Pensum', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($pensum) == 0)
          <p class="text-info">
            No se ha registrado ningun pensum aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    GRADO
                  </th>
                  <th>
                    NIVEL
                  </th>
                  <th>
                    CARRERA
                  </th>
                  @permission('editar-pensum')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('ver-pensum')
                  <th>
                    VER
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosPensum">
                @foreach($pensum as $key => $p)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($p->nombre_grado) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($p->nombre_nivel) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($p->nombre_carrera) }}
                    </td>
                    @permission('editar-pensum')
                    <td>
                      {!!link_to_route('pensum.edit', $title = 'Editar', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    @endpermission
                    @permission('ver-pensum')
                    <td>
                      {!!link_to_route('pensum.show', $title = 'Ver Pensum', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-warning'])!!}
                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
