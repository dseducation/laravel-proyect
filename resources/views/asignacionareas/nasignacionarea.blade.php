@extends('principal')

@section('titulo')
  <title>Nuevo Pensum</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Pensum</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#bgrados">Paso Uno</a></li>
          <li><a data-toggle="tab" href="#acurso">Paso Dos</a></li>
          <li><a data-toggle="tab" href="#gcambios">Paso Tres</a></li>
        </ul>

            {!!Form::open(['route'=>'pensum.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'pensum'])!!}
              @include('asignacionareas.form.campos')

            {!!Form::close()!!}
          @include('mensajes.carga')
      </div>
    </div>
  </div>
@endsection
