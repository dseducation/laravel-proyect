
    <div class="tab-content">
      <div id="bgrados" class="tab-pane fade in active">
        <h3>GRADOS</h3>
        <div class="form-group">
          {!!Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            {!!Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Selecione un plan...', 'id'=>'pnpensum'])!!}
          </div>
        </div>
        <div class="form-group">
          {!!Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            {!!Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Selecione una jornada...', 'id'=>'jnpensum'])!!}
          </div>
        </div>
        <div class="form-group">
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    GRADO
                  </th>
                  <th>
                    NIVEL
                  </th>
                  <th>
                    CARRERA
                  </th>
                  <th>
                    SECCION
                  </th>
                  <th>
                    ASIGNAR PENSUM
                  </th>
                </tr>
              </thead>
              <tbody id="grados">

              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="acurso" class="tab-pane fade">
        <h3>CURSOS</h3>
        <div class="form-group">
          {!!Form::label('salon', 'Salon*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            {!!Form::select('id_salon', $salones, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un salon...', 'id'=>'salon'])!!}
          </div>
        </div>
        <div class="form-group">
          {!!Form::label('buscar', 'Buscar*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            <div class="input-group">
              {!!Form::text('buscar', null, ['class'=>'form-control', 'placeholder'=>'Nombre del curso a buscar...', 'id'=>'autocomplete', 'data-id'=>''])!!}
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="borrarBuscar"><span class="glyphicon glyphicon-remove"></span></button>
                <button class="btn btn-default" type="button" id="agregarCurso"><span class="fa fa-check"></span> Agregar</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Lugar donde se mostraran los cursos que se van a asignar al pensum del grado -->
        <div class="col-sm-6" id="cursosPensum">

        </div>

      </div>
      <div id="gcambios" class="tab-pane fade">
        <h3>Registrar Pensum</h3>
        <div class="col-sm-12">
          <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
        </div>
      </div>
    </div>
