@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">ACTIVIDADES</h1>

        @include('mensajes.msg')

      </div>



      <div class="col-sm-12">



        <p>

       



          {!!link_to_route('Actividad.create', $title = ' Nueva actividad', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}

        </p>

        @if (count($actividades) == 0)

          

        @else

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>

                  <th>

                    FECHA DE INICIO

                  </th>

                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    NOTA TOTAL

                  </th>

                  <th>

                    EDITAR

                  </th>

                  <th>

                    VER ENTREGADOS

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach ($actividades as $key=>$actividad)

                  <tr>

                    <td>

                        {{ $key+1 }}

                    </td>

                    

                    <td>

                    {{ mb_strtoupper($actividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->descripcion_actividad) }}

                  </td>

                  <td>

                   {{ mb_strtoupper($actividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->nota_total) }}

                  </td>

                    <td>

                       {!!link_to_route('respuestasforos.create', $title = 'Editar Actividad', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}

                    </td>

                    <td>

                      <a class="btn btn-warning" href="misrespuestasactividades/?a={{$actividad->id_actividad}}">Ver tareas entregadas</a>
                      
                      <!--
                      {!!link_to_route('misrespuestasactividades.index', $title = 'Ver Entregas', $parameters = ['a'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}
                      -->
                    </td>

                  </tr>

                @endforeach

              </tbody>

            </table>

             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          </div>

        @endif

      </div>

    </div>

  </div>

@endsection

