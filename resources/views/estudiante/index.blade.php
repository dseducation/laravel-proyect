@extends('principal')

@section('titulo')
  <title>Mis Cursos Asignados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Mis Cursos Asignados</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CURSO
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  GRADO
                </th>
                <th>
                  CARRERA
                </th>
                <th>
                  SECCION
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  JORNADA
                </th>
                <th>
                  ACCIONES
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($cursos as $key => $curso)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_area) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_nivel) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_grado) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_carrera) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_seccion) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_plan) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($curso->nombre_jornada) }}
                  </td>
                  <td>
                  {{--   <select class="form-control" onchange="window.location.href=this.value">
                      <option value="">Seleccione...</option>
                      <option value="/docente/create?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}">Zona</option>
                      <option value="/foros?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}">Foros</option>
                      <option value="/foros?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}">Actividades</option>
                    </select>
                    --}}

                    <!-- Single button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Seleccione... <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="/docente/create?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}">Notas</a></li>
                    <li><a href="/foros?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}">Foros</a></li>
                    <li><a href="#">Tareas</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Reportes</a></li>
                  </ul>
                </div>

                    <!--{{-- {!!link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente], $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}
                    --}}-->
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-sm-12 text-center">
        {{ $cursos->setPath('docente')->links() }}
      </div>
    </div>
  </div>
@endsection
