@extends('principal')

@section('titulo')
  <title>Grados Niveles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Grados Niveles</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-gradonivel')
        {!!link_to_route('gradoniveles.create', $title = 'Nuevo Grado-Nivel', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($datos) == 0)
          <p class="text-info">
            No se han registrado grados en ningun nivel aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    SECCION
                  </th>
                  <th>
                    GRADO
                  </th>
                  <th>
                    NIVEL
                  </th>
                  <th>
                    CARRERA
                  </th>
                  @permission('editar-gradonivel')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado-gradonivel')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosGradosNiveles">
                @foreach($datos as $key => $dato)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($dato->nombre_seccion) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($dato->nombre_grado) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($dato->nombre_nivel) }}
                    </td>
                    <td>
                      {{ mb_strtoupper($dato->nombre_carrera) }}
                    </td>
                    @permission('editar-gradonivel')
                    <td>
                      {!!link_to_route('gradoniveles.edit', $title = 'Editar', $parameters = $dato->id_nivel_grado, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    @endpermission
                    @permission('estado-gradonivel')
                    <td>
                      @if($dato->estado_nivel_grado == true)
                        {!!link_to_route('gradoniveles.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGradoNivel', 'data-id'=>$dato->id_nivel_grado, 'data-estado'=>$dato->estado_nivel_grado])!!}
                      @else
                        {!!link_to_route('gradoniveles.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarGradoNivel', 'data-id'=>$dato->id_nivel_grado, 'data-estado'=>$dato->estado_nivel_grado])!!}
                      @endif
                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
            @include('mensajes.carga')
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
