@extends('principal')

@section('titulo')
  <title>Editar Grado Nivel</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Grado-Nivel</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($dato, ['route'=>['gradoniveles.update', $dato->id_nivel_grado], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'ngradonivel'])!!}
          @include('gradosniveles.form.campos')
        {!!Form::close()!!}
      </div>
      @include('mensajes.carga')
    </div>
  </div>
@endsection
