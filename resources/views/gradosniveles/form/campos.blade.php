<div class="form-group">
  {!!Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_plan', $planes, $plan, ['class'=>'form-control', 'placeholder'=>'Selecciones un plan...', 'id'=>'pngradonivel'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_jornada', $jornadas, $jornada, ['class'=>'form-control', 'placeholder'=>'Selecciones una jornada...', 'id'=>'jngradonivel'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_nivel_plan_jornada', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Selecciones un nivel...', 'id'=>'nngradonivel'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('carrera', 'Carrera*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_carrera', $carreras, null, ['class'=>'form-control', 'placeholder'=>'Selecciones una carrera...'])!!}
  </div>
</div>

<div class="form-group">
  {!!Form::label('grado', 'Grado*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_grado', $grados, null, ['class'=>'form-control', 'placeholder'=>'Selecciones un grado...'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('seccion', 'Sección*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_seccion', $secciones, null, ['class'=>'form-control', 'placeholder'=>'Selecciones una sección...'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('inscripcion', 'Inscripción*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    <div class="input-group">
      <span class="input-group-addon">
        <i>
          Q
        </i>
      </span>
      {!!Form::text('cuota_inscripcion', null, ['class'=>'form-control', 'placeholder'=>'Valor de la cuota de inscripción...'])!!}
    </div>
  </div>
</div>
<div class="form-group">
  {!!Form::label('mensualidad', 'Mensualidad*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    <div class="input-group">
      <span class="input-group-addon">
        <i>
          Q
        </i>
      </span>
      {!!Form::text('cuota_mensualidad', null, ['class'=>'form-control', 'placeholder'=>'Valor de la cuota de mensualidad...'])!!}
    </div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
  </div>
</div>
