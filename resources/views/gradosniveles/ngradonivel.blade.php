@extends('principal')

@section('titulo')
  <title>Nuevo Grado-Nivel</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Grado - Nivel</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'gradoniveles.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'ngradonivel'])!!}
          @include('gradosniveles.form.campos')
        {!!Form::close()!!}
      </div>
      @include('mensajes.carga')
    </div>
  </div>
@endsection
