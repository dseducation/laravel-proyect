@extends('principal')

@section('titulo')
  <title>Salones</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Salones</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-salon')
        {!!link_to_route('salones.create', $title = 'Nuevo Salón', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($salones) == 0)
          <p class="text-info">
            No se han registrado salones aun.
          </p>
        @else
          <table class="table table-hover">
            <thead>
              <th>
                NO
              </th>
              <th>
                NOMBRE
              </th>
              @permission('editar-salon')
              <th>
                ACTUALIZAR
              </th>
              @endpermission
              @permission('estado_salon')
              <th>
                ESTADO
              </th>
              @endpermission
            </thead>
            <tbody id="datosSalones">
              @foreach($salones as $key => $salon)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($salon->nombre_salon) }}
                  </td>
                  @permission('editar-salon')
                  <td>
                    {!!link_to_route('salones.edit', $title = 'Editar', $parameters = $salon->id_salon, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-permiso')
                  <td>
                    @if($salon->estado_salon == true)
                      {!!link_to_route('salones.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarSalon', 'data-id'=>$salon->id_salon, 'data-estado'=>$salon->estado_salon])!!}
                    @else
                      {!!link_to_route('salones.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarSalon', 'data-id'=>$salon->id_salon, 'data-estado'=>$salon->estado_salon])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>
    </div>
  </div>
@endsection
