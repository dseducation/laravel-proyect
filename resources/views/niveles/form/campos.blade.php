<div class="form-group">
  {!!Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('nombre_nivel', null, ['class'=>'form-control', 'placeholder'=>'Nombre del nivel...'])!!}
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar"  class="btn btn-success"><span class="fa fa-save"></span> Registrar Nivel</button>
  </div>
</div>
