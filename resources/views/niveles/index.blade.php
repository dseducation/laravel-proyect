@extends('principal')

@section('titulo')
  <title>Niveles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Niveles</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-nivel')
        {!!link_to_route('niveles.create', $title = 'Nuevo Nivel', $parameters =  null, $attributes =['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($niveles) == 0)
          <p class="text-info">
            No se han registrado niveles aun...
          </p>
        @else
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE NIVEL
                </th>
                @permission('editar-nivel')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-nivel')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosNiveles">
              @foreach($niveles as $key => $nivel)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($nivel->nombre_nivel) }}
                  </td>
                  @permission('editar-nivel')
                  <td>
                    {!!link_to_route('niveles.edit', $title = 'Editar', $parameters = $nivel->id_nivel, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-nivel')
                  <td>
                    @if($nivel->estado_nivel == true)
                      {!!link_to_route('niveles.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarNivel', 'data-id'=>$nivel->id_nivel, 'data-estado'=>$nivel->estado_nivel])!!}
                    @else
                      {!!link_to_route('niveles.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarNivel', 'data-id'=>$nivel->id_nivel, 'data-estado'=>$nivel->estado_nivel])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
    </div>
    @include('mensajes.carga')
  </div>
@endsection
