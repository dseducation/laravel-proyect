@extends('principal')

@section('titulo')
  <title>Nuevo Plan</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Plan</h1>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'planes.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nplan'])!!}
          @include('planes.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
