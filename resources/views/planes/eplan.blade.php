@extends('principal')

@section('titulo')
  <title>Editar Plan</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Plan</h1>
        <!-- Errores del formulario del lado del servidor -->
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($plan, ['route'=>['planes.update', $plan->id_plan], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nplan'])!!}
        <!-- Campos del formuario-->
        @include('planes.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
