@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Autenticación</div>
                <div class="panel-body">
                  <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="login">
                      {{ csrf_field() }}
                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label>-->

                        <div class="col-md-12">
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Correo institucional...'])!!}
                          <!--  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">-->
                          </div>

                              @if ($errors->has('email'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                          <div class="col-md-12">
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-key"></i></span>
                              {!!Form::password('password', ['class'=>'form-control', 'placeholder'=>'Contraseña...'])!!}
                              <!--<input id="password" type="password" class="form-control" name="password">-->
                            </div>

                              @if ($errors->has('password'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-6 col-md-offset-4">
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember"> Recordarme
                                  </label>
                              </div>
                          </div>
                      </div>


                              <button type="submit" class="btn btn-lg btn-success btn-block">
                                  <i class="fa fa-btn fa-sign-in"></i> Ingresar
                              </button>


                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
