@extends('principal')

@section('titulo')
  <title>Estudiantes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Estudiantes Inscritos</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        
        @if(count($estudiantes) == 0)
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
        @else

          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    APELLIDOS
                  </th>
                  <th>
                    NOMBRES
                  </th>
                  <th>
                    FECHA DE NACIMIENTO
                  </th>
                  <th>
                    GENERO
                  </th>
                  <th>
                    DIRECCION
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <!--
                  @permission('editar-empleado')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado-empleado')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                  @permission('ver-empleado')
                  <th>
                    MAS DATOS
                  </th>
                  @endpermission-->
                </tr>
              </thead>
              <tbody id="datosEstudiante">
                @foreach($estudiantes as $key => $persona)
                  <tr>
                    <td>
                      {{ $total=$key+1 }}
                    </td>
                    <td>
                      {{ $persona->apellidos_estudiante }}
                    </td>
                    <td>
                      {{ $persona->nombre_estudiante }}
                    </td>
                    <td>
                      {{ $persona->fecha_nacimiento_estudiante }}
                    </td>
                    <td>
                      {{ $persona->genero_estudiante }}
                    </td>
                    <td>
                      {{ $persona->direccion_estudiante }}
                    </td>
                    <td>
                      {{ $persona->telefono_casa }}
                    </td>
                    <!--@permission('editar-empleado')
                    <td>
                      {!!link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    @endpermission
                    @permission('estado-empleado')
                    <td>
                      @if($persona->estado_persona == true)
                        <input type="checkbox" name="estado" checked value="{{ $persona->id_persona }}" class="toggleEstado">
                      @else
                        <input type="checkbox" name="estado" value="{{ $persona->id_persona }}" class="toggleEstado">
                      @endif
                    </td>
                    @endpermission
                    @permission('ver-empleado')
                    <td>
                      {!!link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info'])!!}
                    </td>
                    @endpermission
                    -->
                  </tr>
                @endforeach
              </tbody>
            </table>
            <h3 class="page-header text-center"> total de estudiantes {{ $total }} </h3>
            {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
