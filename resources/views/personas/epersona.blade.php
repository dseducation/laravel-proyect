@extends('principal')

@section('titulo')
  <title>Editar Persona</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Persona</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @extends('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($persona, ['route'=>['personas.update', $persona->id_persona], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'npersona'])!!}
          @include('personas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
