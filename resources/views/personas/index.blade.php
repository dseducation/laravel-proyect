@extends('principal')

@section('titulo')
  <title>Empleados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Empleados</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-empleado')
        {!!link_to_route('personas.create', $title = 'Nuevo Empleado', $parameters = null, $attributes =['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($personas) == 0)
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    APELLIDOS
                  </th>
                  <th>
                    NOMBRES
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    DPI
                  </th>
                  @permission('editar-empleado')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado-empleado')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                  @permission('ver-empleado')
                  <th>
                    MAS DATOS
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosPersonas">
                @foreach($personas as $key => $persona)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ $persona->apellidos_persona }}
                    </td>
                    <td>
                      {{ $persona->nombres_persona }}
                    </td>
                    <td>
                      {{ $persona->telefono_persona }}
                    </td>
                    <td>
                      {{ $persona->cui_persona }}
                    </td>
                    @permission('editar-empleado')
                    <td>
                      {!!link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-primary'])!!}
                    </td>
                    @endpermission
                    @permission('estado-empleado')
                    <td>
                      @if($persona->estado_persona == true)
                        <input type="checkbox" name="estado" checked value="{{ $persona->id_persona }}" class="toggleEstado">
                      @else
                        <input type="checkbox" name="estado" value="{{ $persona->id_persona }}" class="toggleEstado">
                      @endif
                    </td>
                    @endpermission
                    @permission('ver-empleado')
                    <td>
                      {!!link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info'])!!}
                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
            <div class="text-center">
            {!! $personas->links() !!}
          </div>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
