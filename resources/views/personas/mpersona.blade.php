@extends('principal')

@section('titulo')
  <title>Datos Persona</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Datos Persona</h1>
      </div>
      <div class="col-sm-12">
          <ul>
              <li><strong>Nombre(s):</strong> {{ ucwords($persona->nombres_persona) }}</li>
              <li><strong>Apellido(s):</strong> {{ ucwords($persona->apellidos_persona) }}</li>
              <li><strong>CUI:</strong> {{ ucwords($persona->cui_persona) }}</li>
              <li><strong>Dirección:</strong> {{ ucwords($persona->direccion_persona) }}</li>
              <li><strong>Telefono:</strong> {{ $persona->telefono_persona }}</li>
              <li><strong>Telefono Emergencias:</strong> {{ $persona->telefono_auxiliar_persona }}</li>
              <li><strong>Correo personal:</strong> {{ $persona->correo_persona }}</li>
          </ul>
      </div>
    </div>
  </div>
@endsection
