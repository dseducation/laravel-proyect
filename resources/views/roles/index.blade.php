@extends('principal')

@section('titulo')
  <title> Roles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">

      <div class="row">
          <div class="col-lg-12">
              <h1 class="page-header text-center">ROLES DISPONIBLES</h1>
              @include('mensajes.msg')
          </div>
          <div class="col-sm-12">
            @permission('crear-rol')
            {!!link_to_route('roles.create', $title = "Nuevo Rol", $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
            <br>
            <br>
            @endpermission
            @if(count($roles) == 0)
              <p class="text-info">
                No se han registrado roles aun.
              </p>
            @else
                <div class="table-responsive">
                  <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>
                          NO
                        </th>
                        <th>
                          NOMBRE ROL
                        </th>
                        @permission('editar-rol')
                        <th>
                          ACTUALIZAR
                        </th>
                        @endpermission
                        @permission('estado-rol')
                        <th>
                          ESTADO
                        </th>
                        @endpermission
                      </tr>
                    </thead>
                    <tbody id="datosRoles">
                      @foreach($roles as $key => $rol)
                        <tr>
                          <td>
                            {{ $key+1 }}
                          </td>
                          <td>
                            {{ mb_strtoupper($rol->name) }}
                          </td>
                          @permission('editar-rol')
                          <td>
                            {!!link_to_route('roles.edit', $title = 'Editar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-primary'])!!}
                          </td>
                          @endpermission
                          @permission('estado-rol')
                          <td>
                            @if($rol->state_rol == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $rol->id }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $rol->id }}" class="toggleEstado">
                            @endif
                          </td>
                          @endpermission
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
                </div>
            @endif

          </div>
          <!-- /.col-lg-12 -->
      </div>

  </div>
@endsection
