@extends('principal')

@section('titulo')
  <title>Nuevo Rol</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Rol</h1>
        <p>
          Nota: Todos los campos con (*) son olbigatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'roles.store', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'nrol'])!!}
          @include('roles.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
