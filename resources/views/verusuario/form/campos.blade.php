<div class="form-group">
  {!!Form::label('persona', 'Persona*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('persona', null, ['class'=>'form-control', 'placeholder'=>'Buscar persona por nombre, apellido, telefono, correo o...', 'id'=>'busquedaPersona'])!!}
  </div>
</div>
{!!Form::hidden('id_persona', null, ['id'=>'idPersona'])!!}


