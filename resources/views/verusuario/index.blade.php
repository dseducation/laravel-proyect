@extends('principal')

@section('titulo')
  <title>Personas con rol</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Personas</h1>
        @include('mensajes.msg')
      </div>

      <div class="col-sm-12">
        {!!Form::open(['route'=>'verusuario.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'index'])!!}
          @include('verusuario.form.campos')
        {!!Form::close()!!}
        </br>
      </div>
      <div class="col-sm-12">
       
        @if(count($personas) == 0)
          <p class="text-info">
            No se han registrado personas aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                  <th>
                    APELLIDO
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    CORREO
                  </th>
                  <th>
                    PUESTO
                  </th>                  
                </tr>
              </thead>
              <tbody>
                @foreach($personas as $key => $usuario)
                  <tr>
                    <td>
                      {{ $total=$key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($usuario->nombres_persona) }}
                    </td>
                     <td>
                      {{ mb_strtoupper($usuario->apellidos_persona) }}
                    </td>
                     <td>
                      {{ mb_strtoupper($usuario->telefono_persona) }}
                    </td>
                     <td>
                      {{ mb_strtoupper($usuario->correo_persona) }}
                    </td>
                    <td>
                      {{ $usuario->nombre_puesto }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <h4 class="page-header text-center">TOTAL DE PERSONAS {{ $total }}</h4>
            
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
