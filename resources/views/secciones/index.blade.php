@extends('principal')

@section('titulo')
  <title>Secciones</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Secciones</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-seccion')
        {!!link_to_route('secciones.create', $title = 'Nueva sección', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($secciones) == 0)
          <p class="text-info">
            No se han registrado secciones aun.
          </p>
        @else
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE SECCION
                </th>
                @permission('editar-seccion')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody>
              @foreach($secciones as $key => $seccion)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($seccion->nombre_seccion) }}
                  </td>
                  @permission('editar-seccion')
                  <td>
                    {!!link_to_route('secciones.edit', $title ='Editar', $parameters = $seccion->id_seccion, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
        @endif
      </div>
    </div>
  </div>
@endsection
