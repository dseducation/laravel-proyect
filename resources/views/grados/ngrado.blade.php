@extends('principal')

@section('titulo')
  <title>Nuevo Grado</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Grado</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'grados.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'ngrado'])!!}
          @include('grados.form.campos')
        {!!Form::close()!!}
      </div>
      @include('mensajes.carga')
    </div>
  </div>
@endsection
