<?php

use Illuminate\Database\Seeder;

class permissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
          'name' => 'crear-carrera',
          'display_name' => 'crear carrera',
          'description' => 'Registra una nueva carrera en el sistema',
          'created_at' => date('Y-m-d H:i:s'),
          'id_opcion'=> 1,
      ]);
    }
}
