<?php $__env->startSection('titulo'); ?>
  <title>Unidades</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Unidades</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo link_to_route('unidades.create', $title = ' Nueva Unidad', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        <br>
        <br>
      </div>
      <div class="col-sm-12">
        <?php if(count($unidades) ==  0): ?>
          <p class="text-info">
            No se han registrado unidades aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                  <th>
                    FECHA_INICIO
                  </th>
                  <th>
                    FECHA_FINAL
                  </th>
                  <th>
                    EDITAR
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($unidades as $key => $unidad): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>

                    </td>
                    <td>
                      <?php echo e(ucwords($unidad->nombre_unidad)); ?>

                    </td>
                    <td>
                      <?php echo e($unidad->fecha_inicio); ?>

                    </td>
                    <td>
                      <?php echo e($unidad->fecha_final); ?>

                    </td>
                    <th>
                      <?php echo link_to_route('unidades.edit', $title = ' Editar', $parameters = $unidad->id_unidad, $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>

                    </th>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>