

<?php $__env->startSection('titulo'); ?>
  <title>Inscripciones Estudiantes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Inscripciones Estudiantes</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>


        <table class="table table-hover">
            <thead>
              <tr>
              <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                <th>
                  APELLIDO 
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  CORREO 
                </th>
                <?php if (\Entrust::can('editar-grado')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-grado')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody >
              <?php foreach($cursos as $key => $grado): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->apellidos_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->telefono_casa)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->correo_estudiante)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-grado')) : ?>
                  <td>
                    <?php echo link_to_route('grados.edit', $title = 'Editar', $parameters = $grado->id_grado, $attributes = ['class'=>'btn btn-primary']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-grado')) : ?>
                  <td>
                    <?php if($grado->estado_grado == true): ?>
                      <?php echo link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado]); ?>

                    <?php else: ?>
                      <?php echo link_to_route('grados.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado]); ?>

                    <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>


          </table>
           <div class="text-center">
            <?php echo $cursos->links(); ?>

          </div>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>