<div class="form-group">
  <?php echo Form::label('persona', 'Persona*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('persona', null, ['class'=>'form-control', 'placeholder'=>'Buscar persona por nombre, apellido, telefono, correo o...', 'id'=>'busquedaPersona']); ?>

  </div>
</div>
<?php echo Form::hidden('id_persona', null, ['id'=>'idPersona']); ?>



