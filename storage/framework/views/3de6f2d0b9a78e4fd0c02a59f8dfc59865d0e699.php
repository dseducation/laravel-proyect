<?php $__env->startSection('titulo'); ?>
  <title>Planes Niveles</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Planes Niveles</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-plannivel')) : ?>
        <?php echo link_to_route('asignarniveles.create', $title = 'Nuevo Plan - Nivel', $parameters = null, $attributes =['class'=>'btn btn-primary']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($datos) == 0): ?>
          <p class="text-info">
            No se han registrado asignacioónes de de niveles a planes y jornadas.
          </p>
        <?php else: ?>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  JORNADA
                </th>
                <?php if (\Entrust::can('editar-plannivel')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosAsignacion">
              <?php foreach($datos as $key => $row): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_nivel)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_plan)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_jornada)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-plannivel')) : ?>
                  <td>
                    <?php echo link_to_route('asignarniveles.edit', $title = 'Editar', $parameters = $row->id_nivel_plan_jornada, $attributes = ['class'=>'btn btn-primary']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>