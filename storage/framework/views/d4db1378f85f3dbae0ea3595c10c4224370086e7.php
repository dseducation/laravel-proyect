<?php $__env->startSection('titulo'); ?>
  <title>Salones</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Salones</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-salon')) : ?>
        <?php echo link_to_route('salones.create', $title = 'Nuevo Salón', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($salones) == 0): ?>
          <p class="text-info">
            No se han registrado salones aun.
          </p>
        <?php else: ?>
          <table class="table table-hover">
            <thead>
              <th>
                NO
              </th>
              <th>
                NOMBRE
              </th>
              <?php if (\Entrust::can('editar-salon')) : ?>
              <th>
                ACTUALIZAR
              </th>
              <?php endif; // Entrust::can ?>
              <?php if (\Entrust::can('estado_salon')) : ?>
              <th>
                ESTADO
              </th>
              <?php endif; // Entrust::can ?>
            </thead>
            <tbody id="datosSalones">
              <?php foreach($salones as $key => $salon): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($salon->nombre_salon)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-salon')) : ?>
                  <td>
                    <?php echo link_to_route('salones.edit', $title = 'Editar', $parameters = $salon->id_salon, $attributes = ['class'=>'btn btn-primary']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-permiso')) : ?>
                  <td>
                    <?php if($salon->estado_salon == true): ?>
                      <?php echo link_to_route('salones.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarSalon', 'data-id'=>$salon->id_salon, 'data-estado'=>$salon->estado_salon]); ?>

                    <?php else: ?>
                      <?php echo link_to_route('salones.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarSalon', 'data-id'=>$salon->id_salon, 'data-estado'=>$salon->estado_salon]); ?>

                    <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>