<?php $__env->startSection('titulo'); ?>
  <title>Mis Cursos</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Mis Cursos</h1>
        <h3 class="text-center"><?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h3>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CURSO
                </th>
                <th>
                  ACCIONES
                </th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($cursos as $key => $curso): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($curso->nombre_area)); ?>
                  </td>
                  <td>
                    <!-- Single button -->
                    <div class="btn-group">
                      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Seleccione... <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu">
                        <li><a href="/misactividades?a=<?php echo e($curso->id_area); ?>">Mis Tareas/actividades</a></li>
                        <li><a href="/foros?a=<?php echo e($curso->id_asignacion_area); ?>">Foros</a></li>
                        <li><a href="#">Notas</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                      </ul>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>