<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">MIS ACTIVIDADES</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <p>

          

        </p>

        <?php if(count($actividades) == 0): ?>

          <p class="text-info">

            No se han encontrado actividades para este curso...

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>

                  <th>

                    FECHA DE INICIO

                  </th>

                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    NOTA TOTAL

                  </th>

                 

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($actividades as $key=>$actividad): ?>

                  <tr>

                    <td>

                        <?php echo e($key+1); ?>


                    </td>

                    

                    <td>

                    <?php echo e(mb_strtoupper($actividad->nombre_actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->descripcion_actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_inicio)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->nota_total)); ?>


                  </td>

                    

                    <td>

                      <a class="btn btn-info" href="/respuestaactividades/create?actividad=<?php echo e($actividad->id_actividad); ?>">Engregar</a>

                      <!--<?php echo link_to_route('respuestaactividades.create', $title = 'Entregar tarea', $parameters = ['a'=>$actividad->id_actividad,'b'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info ']); ?>

-->
                    </td>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>