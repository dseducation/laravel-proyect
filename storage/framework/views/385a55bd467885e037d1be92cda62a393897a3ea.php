<?php $__env->startSection('titulo'); ?>
  <title>Nuevo Curso</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Curso</h1>
        <p>
          Nota: Todos los campos con (*) son oolbigatorios.
        </p>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo Form::open(['route'=>'areas.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'narea']); ?>

          <?php echo $__env->make('areas.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>

      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>