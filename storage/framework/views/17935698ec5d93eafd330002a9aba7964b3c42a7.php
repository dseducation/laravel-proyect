

<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>FORMULARIO DE INSCRIPCION</title>

   

  </head>

  <body>

 

    <main>

      <div id="details" class="clearfix">

        <div id="invoice">
          
            
          <center>

          <h3><img src="<?php echo e(url('/img/LOGO EDUCATIVO-19.png')); ?>">FICHA DE INSCRIPCION NO <?php echo e($data['id']); ?></h3>
        
          </center>

        </div>

      </div>
      <center>
        <div align="center">

      <table border="1" align="center" WIDTH="100%"cellspacing="0" cellpadding="0">

        


            <tr>
             <td rowspan="12" bgcolor= "#A9BCF5">
             <center>
                Información Personal</br> del alumno
             </center>
             </td>
             <td>Apellidos: </td>
              <td><?php echo e($data['apellidos']); ?></td>
            </tr>

           <tr>
            <td>Nombres: </td>
              <td><?php echo e($data['nombre']); ?></td>
              
            </tr>

            <tr>
              <td>Fecha de nacimiento:  </td>
              <td><?php echo e($data['fecha']); ?></td>
            </tr>

            <tr>
              <td>Carrera/ciclo:  </td>
              <td><?php echo e($data['nombre_carrera']); ?></td>
            </tr>

             <tr>
              <td>Grado:  </td>
              <td><?php echo e($data['nombre_grado']); ?></td>
            </tr>

            <tr>
              <td>Direccion:  </td>
               <td><?php echo e($data['direccion']); ?></td>
            </tr>

            <tr>
              <td>Colonia: </td>
              <td><?php echo e($data['colonia']); ?></td>
            </tr>

            <tr>
              <td>Zona: </td>
              <td><?php echo e($data['zona']); ?></td>
            </tr>
            
            <tr>
              <td>Municipio: </td>
              <td><?php echo e($data['zona']); ?></td>
            </tr>

            <tr>
              <td>Telefono de casa/Celular:  </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
              <td>Fecha de inscripción:  </td>
              <td><?php echo e($data['fecha_registro_estudiante']); ?></td>
            </tr>
             <tr>
              <td>Ciclo Escolar:  </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
            <td rowspan="3" bgcolor= "#A9BCF5">
             <center>
                Referencias 
             </center>
             </td>

              <td>Parentesco:   </td>
              <td><?php echo e($data['parentesco_referencia']); ?></td>
            </tr>

            <tr>
              <td>Nombre:  </td>
              <td><?php echo e($data['referencia']); ?></td>
            </tr>

            <tr>
              <td>Telefono:  </td>
              <td><?php echo e($data['telefono_referencia']); ?></td>
            </tr>
            <tr>
             <td rowspan="11" bgcolor= "#A9BCF5">
             <center>
                Información del encargado
             </center>
            
              <td>Apellidos:  </td>
              <td><?php echo e($data['apellidos_tutor']); ?></td>
            </tr>


            <tr>
              <td>Nombres:   </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
              <td>DPI:  </td>
              <td><?php echo e($data['cui_tutor']); ?></td>
            </tr>

            <tr>
              <td>Dirección:  </td>
              <td><?php echo e($data['direccion_tutor']); ?></td>
            </tr>

            <tr>
              <td>Teléfono/Celular: </td>
              <td><?php echo e($data['telefono_primario_tutor']); ?></td>
            </tr>

            <tr>
              <td>Correo electrónico:  </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
              <td>Lugar de Trabajo:   </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
              <td>Dirección de Trabajo:  </td>
              <td><?php echo e($data['direccion_tutor']); ?></td>
            </tr>

            <tr>
              <td>Linea Telefonica:  </td>
              <td><?php echo e($data['telefono_trabajo_tutor']); ?></td>
            </tr>

            <tr>
              <td>Cargo:  </td>
              <td><?php echo e($data['cargo_tutor']); ?></td>
            </tr>

            <tr>
              <td>Telefono de casa/Celular:  </td>
              <td><?php echo e($data['telefono']); ?></td>
            </tr>

            <tr>
             <td rowspan="5" bgcolor= "#A9BCF5">
             <center>
                Información Médica 
             </center>
            
              <td>Enfermedades que padece:  </td>
              <td><?php echo e($data['enfermedad_padecida_estudiante']); ?></td>
            </tr>

            <tr>
              <td>Medicamentos recomendados:  </td>
              <td><?php echo e($data['medicamento_recomendado_estudiante']); ?></td>
            </tr>
            <tr>
              <td>Es alérgico a:  </td>
              <td><?php echo e($data['alergico_estudiante']); ?></td>
            </tr>
            <tr>
              <td>Emergencia llevar a hospital:  </td>
              <td><?php echo e($data['hospital_estudiante']); ?></td>
            </tr>
            <tr>
              <td>Tipo de sangre:  </td>
              <td><?php echo e($data['tipo_sangre_estudiante']); ?></td>
            </tr>

             <tr>
             <td rowspan="5" bgcolor= "#A9BCF5">
             <center>
                Cuotras Autorizadas
             </center>
            
              <td>Inscripción:  </td>
              <td></td>
            </tr>

            <tr>
              <td>Colegiaturas:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Derecho de examen:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Graduación:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Carné: </td>
              <td></td>
            </tr>

            <tr>

              <td bgcolor= "#A9BCF5"><center>observaciones </center> </td>
              <td colspan="2" ><?php echo e($data['observaciones_estudiante']); ?> </td>
              
            </tr>


           
      </table>
    </center>
  </div>

  </body>

</html>