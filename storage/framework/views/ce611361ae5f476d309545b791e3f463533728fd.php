

<?php $__env->startSection('titulo'); ?>
  <title>Nueva Inscripción</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Inscripción</h1>
        <p>
          NOTA: Todos los campos con (*) son requeridos.
        </p>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo Form::open(['route'=>'inscripcionestudiantes.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'inscripcionestudiante']); ?>


        <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Grado <i class="fa"></i></a></li>
        <li><a data-toggle="tab" href="#menu1">Estudiante <i class="fa"></i></a></li>
        <li><a data-toggle="tab" href="#menu2">Tutores <i class="fa"></i></a></li>
        <li><a data-toggle="tab" href="#menu3">Referencias <i class="fa"></i></a></li>
        <li><a data-toggle="tab" href="#menu4">Registrar <i class="fa"></i></a></li>
        </ul>

        <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <h3>Grado Inscripción</h3>
          <?php echo $__env->make('inscripciones.campos.grado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div id="menu1" class="tab-pane fade">
          <h3>Datos Estudiante</h3>
          <?php echo $__env->make('inscripciones.campos.estudiantes', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div id="menu2" class="tab-pane fade">
          <h3>Datos Tutores</h3>
          <?php echo $__env->make('inscripciones.campos.tutores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
        </div>
        <div id="menu3" class="tab-pane fade">
          <h3>Datos Referencias</h3>
          <?php echo $__env->make('inscripciones.campos.referencias', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <div id="menu4" class="tab-pane fade">
          <h3>Registrar Estudiante</h3>
          <button type="submit" name="registrar" class="btn btn-success"><i class="fa fa-save"></i> Registrar</button>
        </div>
        </div>


        <?php echo Form::close(); ?>

      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>