<?php
  $msg = Session::get('mensaje');
  if ($msg != null) {
    $palabra = 'No';
    $buscar = strpos($msg, $palabra);
  }
?>
<?php if($msg != null): ?>

  <?php if($buscar === false): ?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
       <?php echo e($msg); ?>

    </div>
  <?php else: ?>
    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo e($msg); ?>

    </div>
  <?php endif; ?>

<?php endif; ?>
