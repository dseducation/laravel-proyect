<?php $__env->startSection('titulo'); ?>
  <title>Niveles</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Niveles</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-nivel')) : ?>
        <?php echo link_to_route('niveles.create', $title = 'Nuevo Nivel', $parameters =  null, $attributes =['class'=>'btn btn-primary']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($niveles) == 0): ?>
          <p class="text-info">
            No se han registrado niveles aun...
          </p>
        <?php else: ?>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE NIVEL
                </th>
                <?php if (\Entrust::can('editar-nivel')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-nivel')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosNiveles">
              <?php foreach($niveles as $key => $nivel): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($nivel->nombre_nivel)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-nivel')) : ?>
                  <td>
                    <?php echo link_to_route('niveles.edit', $title = 'Editar', $parameters = $nivel->id_nivel, $attributes = ['class'=>'btn btn-primary']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-nivel')) : ?>
                  <td>
                    <?php if($nivel->estado_nivel == true): ?>
                      <?php echo link_to_route('niveles.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarNivel', 'data-id'=>$nivel->id_nivel, 'data-estado'=>$nivel->estado_nivel]); ?>

                    <?php else: ?>
                      <?php echo link_to_route('niveles.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarNivel', 'data-id'=>$nivel->id_nivel, 'data-estado'=>$nivel->estado_nivel]); ?>

                    <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
        <?php endif; ?>
      </div>
    </div>
    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>