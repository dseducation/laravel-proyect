<?php $__env->startSection('titulo'); ?>
  <title>Foros</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Foros</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <p>
          <?php echo link_to_route('foros.create', $title = ' Nuevo Foro', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        </p>
        <?php if(count($foros) == 0): ?>
          <p class="text-info">
            No se han creado foros en este curso...
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    TITULO
                  </th>
                  <th>
                    EDITAR
                  </th>
                  <th>
                    RESPONDER
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($foros as $key=>$foro): ?>
                  <tr>
                    <td>
                        <?php echo e($key+1); ?>

                    </td>
                    <td>
                      <?php echo link_to_route('respuestasforos.show', $title = $foro->titulo_foro, $parameters = $foro->id_foro, $attributes= null); ?>

                    </td>
                    <td>
                      <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('owner', $foro)): ?>
                      <?php echo link_to_route('foros.edit', $title = ' Editar', $parameters = $foro->id_foro, $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>

                      <?php endif; ?>
                    </td>
                    <td>
                      <?php echo link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments']); ?>

                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>