<?php $__env->startSection('title'); ?>
  <title>Nueva Sección</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-sm-12">
      <h1 class="page-header text-center">Nueva Sección</h1>
      <p>
        Nota: Todos los campos con (*) son obligatorios.
      </p>
      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <div class="col-sm-12">
      <?php echo Form::open(['route'=>'secciones.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nseccion']); ?>

        <?php echo $__env->make('secciones.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo Form::close(); ?>

    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>