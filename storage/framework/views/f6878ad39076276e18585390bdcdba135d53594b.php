<div class="form-group">
  <?php echo Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Nombre del nuevo rol...']); ?>

  </div>
</div>

<div class="form-group">
  <?php echo Form::label('descripcion', 'Descripción*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('description', null, ['class'=>'form-control', 'placeholder'=>'Breve descriptción sobre el rol..']); ?>

  </div>
</div>

<div class="form-group">
  <?php echo Form::label('opciones', 'Opciones*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::select('opciones', $opciones, null, ['placeholder'=>'Seleccione una opción...', 'class'=>'form-control', 'id'=>'opcionesSistema']); ?>

  </div>
</div>

<div class="form-group">
  <?php echo Form::label('permisos', 'Lista Permisos*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <div class="table-responsive">

    <table class="table table-hover">
      <thead>
        <tr>
          <th>
            NO
          </th>
          <th>
            NOMBRE PERMISO
          </th>
          <th>
            OPCION
          </th>
        </tr>
      </thead>
      <tbody id="subOpciones">

      </tbody>
    </table>
  </div>
</div>
</div>

<div class="form-group">
  <?php echo Form::label('permiso', 'Permisos Rol*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <div id="permisosRoles">

    </div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar Rol</button>
  </div>
</div>
