<!-- Bootstrap Core CSS -->
<link href="<?php echo e(url('/')); ?>/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?php echo e(url('/')); ?>/css/metisMenu.min.css" rel="stylesheet">

<!-- Timeline CSS -->
<link href="<?php echo e(url('/')); ?>/css/timeline.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo e(url('/')); ?>/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<?php echo e(url('/')); ?>/css/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo e(url('/')); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Bootstrap validator CSS -->
<link href="<?php echo e(url('/')); ?>/css/formValidation.min.css" rel="stylesheet" type="text/css">

<!-- Mis Estilos CSS-->
<link href="<?php echo e(url('/')); ?>/css/estilos.css" rel="stylesheet" type="text/css">

<!-- Estilos para el datepicker bootstrap -->
<link rel="stylesheet" href="<?php echo e(url('/')); ?>/css/bootstrap-datetimepicker.min.css" />

<!-- Estilos de bootstrap toggle -->
<link rel="stylesheet" href="<?php echo e(url('/')); ?>/css/bootstrap-toggle.min.css" />

<!-- Estilos de alert jquery -->
<link rel="stylesheet" href="<?php echo e(url('/')); ?>/css/jquery-confirm.min.css" />

<link rel="shortcut icon" href="<?php echo e(url('/')); ?>/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo e(url('/')); ?>/favicon.ico" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
