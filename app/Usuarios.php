<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Usuarios extends Model
{
    protected $table = 'USUARIOS';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setUsuario($usuario, $pass, $fecha, $correo, $rol)
    {
      return DB::select('CALL nuevo_usuario(?, ?, ?, ?, ?)', array($usuario, $pass, $fecha, $correo, $rol));
    }
}
