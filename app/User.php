<?php

namespace education;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use DB;
class User extends Authenticatable
{
    use EntrustUserTrait; //hacemos uso del trait en la clase User para hacer uso de sus métodos
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //establecemos las relaciones con el modelo Role, ya que un usuario puede tener varios roles
    //y un rol lo pueden tener varios usuarios
    public function roles(){
        return $this->belongsToMany('education\Role');
    }

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setUsuario($usuario, $pass, $fecha, $correo, $rol)
    {
      return DB::select('CALL nuevo_usuario(?, ?, ?, ?, ?)', array($usuario, $pass, $fecha, $correo, $rol));
    }

    /*
      Metodo para agregar un nuevo rol a un usuario
    */
    public static function setRolUsuario($usuario, $rol)
    {
      return DB::insert('CALL nuevo_rol_usuario(?, ?)', array($usuario, $rol));
    }
    /*
      Metodo para buscar los permisos asignados a un usuario por medio de su rol
    */
    public static function getPermisosRol($id)
    {
        return DB::table('OPCIONES as o')
                 ->join('permissions as p', 'o.id_opcion', '=', 'p.id_opcion')
                 ->join('permission_role as pr', 'p.id', '=', 'pr.permission_id')
                 ->join('roles as r', 'pr.role_id', '=', 'r.id')
                 ->join('role_user as ru', 'r.id', '=', 'ru.role_id')
                 ->where('ru.user_id', $id)
                 ->select('o.nombre_opcion', 'p.display_name', 'o.contenedor_principal', 'o.ruta_opcion')
                 ->get();
    }

    //funcion agregada por cj para agregar tareas
    public function actividad()
    {
        return $this->belongsToMany('education\actividad','idUsuario','id');
    }


}
