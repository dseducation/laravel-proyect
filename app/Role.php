<?php

namespace education;

//use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;
use DB;
class Role extends EntrustRole
{
  protected $fillable = [
      'name',
      'display_name',
      'description'
  ];

  /*
    Agrega un nuevo registro a la tabla roles
  */
  public static function setRol($nombre, $descripcion)
  {
    return DB::select('CALL nuevo_rol(?, ?, ?)', array($nombre, $descripcion, date('Y-m-d H:i:s')));
  }

  /*
    Metodo para buscar los datos de un rol por su id
  */
  public static function findRol($id)
  {
    return Role::where('id', $id)->first();
  }

  /*
    Metodo para cambiar el estado de un rol por medio de su id
  */
  public static function stateRol($id, $estado)
  {
    return DB::select('CALL estado_rol(?, ?)', array($id, $estado));
  }

 //establecemos las relacion de muchos a muchos con el modelo User, ya que un rol
 //lo pueden tener varios usuarios y un usuario puede tener varios roles
 public function users(){
      return $this->belongsToMany('education\User');
  }
}
