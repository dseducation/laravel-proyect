<?php



namespace education;



use Illuminate\Database\Eloquent\Model;

use DB;

class Permisos_roles extends Model

{

    protected $table = 'PERMISOS_ROLES';



    /*

      Agrega un nuevo registro a al tabla permisos roles

    */

    public static function setPermisoRol($rol, $permiso)

    {

      return DB::select('CALL nuevo_permiso_rol(?, ?)', array($rol, $permiso));

    }



    /* Metodo para buscar los permisos asignados a un rol por el id del rol

    */

    public static function findPermisos($rol)

    {

      return PERMISOS_ROLES::join('SUB_OPCIONES AS so', 'PERMISOS_ROLES.id_sub_opcion', '=', 'so.id_sub_opcion')

                           ->where('id_rol', $rol)

                           ->select('so.nombre_sub_opcion', 'PERMISOS_ROLES.id_permiso_rol', 'PERMISOS_ROLES.estado_permiso_rol')

                           ->get();

    }



    /*

      Metodo para cambiar el estado de un permiso asignado a un rol

    */

    public static function statePermisoRol($id, $estado)

    {

      return DB::select('CALL estado_permiso_rol(?, ?)', array($id, $estado));

    }

}

