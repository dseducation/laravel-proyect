<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Jornadas extends Model
{
    protected $table ='JORNADAS';

    /*
      Metodo para agregar un nuevo registro a la tabla JORNADAS
    */
    public static function setJornadas($nombre)
    {
      return DB::select('CALL nueva_jornada(?)', array($nombre));
    }

    /*
    metodo para buscar los datos de una jornada por su id
    */
    public static function findJornada($id)
    {
      return Jornadas::where('id_jornada', $id)->first();
    }

    /*
    metodo para editar los datos de una jornada
    */
    public static function updateJornada($id, $nombre)
    {
      return DB::select('CALL actualizar_jornada(?, ?);', array($id, $nombre));
    }

    /*
    Metodo para cambiar el estado de una Jornada
    */
    public static function stateJornada($id, $estado)
    {
      return DB::select('CALL estado_jornada(?, ?);', array($id, $estado));
    }

    /*
      Metodo para obtener todas las jornadas disponibles
    */
    public static function getJornadas()
    {
      $jornadas = array();
      foreach (JORNADAS::where('estado_jornada', TRUE)->get() as $key => $j) {
        $jornadas[$j->id_jornada] = mb_strtoupper($j->nombre_jornada);
      }
      return $jornadas;
    }

}
