<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{
    protected $table = 'UNIDADES';
    public $timestamps = false;
}
