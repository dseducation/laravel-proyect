<?php

namespace education\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use education\User;
use education\Foros;

class ForosPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        //
    }*/

    public function owner(User $user, Foros $foros)
    {
      return $user->id === $foros->id_usuario;
    }
}
