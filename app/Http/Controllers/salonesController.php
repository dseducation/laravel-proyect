<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use \Validator;
use Session;
use education\Salones;
use education\User;
use Auth;
class salonesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = User::where('id', Auth::user()->id)->first();
      if ($user->can(['crear-salon', 'editar-salon', 'estado-salon'])) {
        $salones = Salones::All();
        return view('salones.index', ['salones'=>$salones, 'items'=>$this->menu]);
      }
      return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-salon')) {
          return view('salones.nsalon', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_salon' => 'required|min:2|max:20|unique:SALONES,nombre_salon',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_salon'];
      $result = Salones::setSalon($nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/salones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-salon')) {
          $salon = Salones::findSalon($id);
          return view('salones.esalon', ['salon'=>$salon, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_salon' => 'required|min:2|max:20|unique:SALONES,nombre_salon,'.$id.',id_salon',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_salon'];
      $result = Salones::updateSalon($id, $nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/salones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Salones::stateSalon($id, FALSE);
        } else {
          Salones::stateSalon($id, TRUE);
        }
        $salones = Salones::All();
        return response()->json($salones);
    }
}
