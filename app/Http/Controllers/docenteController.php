<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Repositories\docentesRepository;
use education\Repositories\estudiantesRepository;
use education\Repositories\unidadesRepository;

use Auth;
use Session;
class docenteController extends Controller
{
  protected $menu;
  protected $docentes;
  protected $estudiantes;
  protected $unidades;
  public function __construct(docentesRepository $docente, estudiantesRepository $estudiante, unidadesRepository $unidad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->docentes = $docente;
    $this->estudiantes = $estudiante;
    $this->unidades = $unidad;
  }

  /**
     * Create a length aware custom paginator instance.
     *
     * @param  Collection  $items
     * @param  int  $perPage
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    protected function paginate($items, $perPage = 12)
    {
        //Get current page form url e.g. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($items);

        //Slice the collection to get the items to display in current page
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);

        //Create our paginator and pass it to the view
        return new LengthAwarePaginator($currentPageItems, count($collection), $perPage);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->docentes->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada
        $aux = $this->docentes->getCursos();//obtenemos los cursos asignados al docente
        $cursos = $this->paginate($aux, 5);//paginar el resultado
        return view('docentes.index', ['items'=>$this->menu, 'cursos'=>$cursos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      if (isset($request['a']) && isset($request['b'])) {
        $this->docentes->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada
        //$c=$this->docentes->id_usuario = Auth::user()->id;
        $cursos = $this->docentes->getCursos();//obtenemos los cursos asignados al docente
        $b = false;
        $datos = array();
        foreach ($cursos as $key => $row) {
          if ($request['a'] == $row->id_asignacion_area) {
            $b = true;
            $datos['curso'] = 'Nombre del Curso: '.ucwords($row->nombre_area);
            $datos['grado'] = 'Grado: '.ucwords($row->nombre_grado);
            $datos['nivel'] = 'Nivel: '.ucwords($row->nombre_nivel);
            $datos['carrera'] = 'Carrera: '.ucwords($row->nombre_carrera);
            $datos['seccion'] = 'Seccion: '.ucwords($row->nombre_seccion);
          }
        }
        if ($b) {
            $this->estudiantes->curso = $request['a'];
            $nomina = $this->estudiantes->getEstudiantesCurso();
            $this->unidades->fecha_inicio = date('Y-m-d');
            $this->unidades->fecha_final = date('Y-m-d');
            $unidad = $this->unidades->getUnidadActual();
        } 

        else {
          Session::flash('mensaje', 'No se pudo procesar la solicitud, porque no tienes asignado el curso o el curso no existe!!!');
          return redirect('/docente');
        }

      } else {
        Session::flash('mensaje', 'No se pudo procesar la solicitud, porque los datos de la petición no son validos!!');
        return redirect('/docente');
      }
      return view('docentes.zonas', ['items'=>$this->menu, 'datos'=>$datos, 'nomina'=>$nomina, 'unidad'=>$unidad]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
