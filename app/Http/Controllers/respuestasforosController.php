<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Requests\respuestasforosRequest;
use education\Http\Controllers\menuController;
use education\Repositories\forosRepository;
use education\Repositories\respuestasforosRepository;
use Auth;
use Session;

class respuestasforosController extends Controller
{
  protected $menu;
  protected $foro;
  protected $respuestasforos;
  public function __construct(menuController $m, forosRepository $foro, respuestasforosRepository $respuesta)
  {
    $this->menu = $m->index();
    $this->foros = $foro;
    $this->respuestasforos = $respuesta;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return View('');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->foros->id_foro = $request['foro'];
        $foro = $this->foros->getForoById();
        if (count($foro) == 0) {
          abort(404);
        }
        return view('respuestasforos.nrespuesta', ['items'=>$this->menu, 'foro'=>$foro]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(respuestasforosRequest $request)
    {
        try {
        $this->respuestasforos->id_usuario = Auth::user()->id;
        $this->respuestasforos->mensaje_respuesta = $request['mensaje_respuesta'];
        $this->respuestasforos->id_foro = $request['foro'];
        $this->respuestasforos->setRespuestaForo();
        } catch (Exception $e) {
          abort(500);
        }
        Session::flash('mensaje', 'Se regitraso la nueva respuesta para el foro seleccionado!!!');
        return redirect('/foros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->foros->id_foro = $id;
        $foro = $this->foros->getForoById();
        $this->respuestasforos->id_foro = $id;
        $respuestasE = $this->respuestasforos->getRespuestasForoE();
        $respuestasP = $this->respuestasforos->getRespuestasForoP();
        $respuestas = array();
        foreach ($respuestasE as $key => $row) {
          $aux = array('nombre'=>$row->nombre_estudiante, 'apellido'=>$row->apellidos_estudiante, 'respuesta'=>$row->mensaje_respuesta, 'fecha'=>$row->fecha_respuesta);
          array_push($respuestas, $aux);
        }
        foreach ($respuestasP as $key => $row) {
          $aux = array('nombre'=>$row->nombres_persona, 'apellido'=>$row->apellidos_persona, 'respuesta'=>$row->mensaje_respuesta, 'fecha'=>$row->fecha_respuesta);
          array_push($respuestas, $aux);
        }
        return view('respuestasforos.index', ['items'=>$this->menu, 'foro'=>$foro, 'respuestas'=>$respuestas]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
