<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use \Validator;
use Session;
use education\Niveles;
use education\User;
use Auth;
class nivelesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-nivel', 'editar-nivel', 'estado-nivel'])) {
          $niveles = Niveles::All();
          return view('niveles.index', ['niveles'=>$niveles, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-nivel')) {
          return view('niveles.nnivel', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_nivel' => 'required|min:4|max:30|unique:NIVELES,nombre_nivel',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_nivel'];
      $result = Niveles::setNivel($nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/niveles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-nivel')) {
          $nivel = Niveles::findNivel($id);
          return view('niveles.enivel', ['nivel'=>$nivel, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_nivel' => 'required|min:4|max:30|unique:NIVELES,nombre_nivel,'.$id.',id_nivel',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_nivel'];
      $result = Niveles::updateNivel($id, $nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/niveles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Niveles::stateNivel($id, FALSE);
        } else {
          Niveles::stateNivel($id, TRUE);
        }
        $niveles = Niveles::All();
        return response()->json($niveles);
    }

    /**
    * Listado de todos los niveles disponibles que se hacen por medio de una peticon ajax
    */
    public function listado(Request $request)
    {
      $plan = $request['plan'];
      $jornada = $request['jornada'];
      $niveles = Niveles::All();
      return response()->json($niveles);
    }
}
