<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Requests\unidadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\unidadesRepository;
use Session;
class unidadesController extends Controller
{

    protected $menu;
    protected $unidades;
    public function __construct(unidadesRepository $unidad)
    {
      $m = new menuController;
      $this->menu = $m->index();
      $this->unidades = $unidad;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $unidades = $this->unidades->getUnidades();
        return view('unidades.index', ['items'=>$this->menu, 'unidades'=>$unidades]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('unidades.nunidad', ['items'=>$this->menu]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(unidadesRequest $request)
    {
        //dd('Valido');
        $this->unidades->nombre = $request['nombre_unidad'];
        $this->unidades->fecha_inicio =  $request['fecha_inicio'];
        $this->unidades->fecha_final = $request['fecha_final'];

        $existe = $this->unidades->existUnidad();
        if ($existe == 0) {
          $this->unidades->setUnidad();
          $msg = 'Se ha registrado la nueva unidad exitosamente!!!';
        } else {
          $msg = 'No se pudo registrar la unidad, porque el nombre de unidad, ya esta registrado!!!';
        }
        Session::flash('mensaje',$msg);
        return redirect('/unidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->unidades->id = $id;
        $unidad = $this->unidades->getUnidadById();
        return view('unidades.eunidad', ['items'=>$this->menu, 'unidad'=>$unidad]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(unidadesRequest $request, $id)
    {
        //dd('valido');
        $this->unidades->nombre = $request['nombre_unidad'];
        $this->unidades->fecha_inicio = $request['fecha_inicio'];
        $this->unidades->fecha_final = $request['fecha_final'];
        $this->unidades->id = $id;
        $this->unidades->updateUnidad();
        Session::flash('mensaje', 'Se ha actualizado los datos de la unidad exitosamente!!!');
        return redirect('/unidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
