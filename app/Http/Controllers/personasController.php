<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Personas;
use education\Puestos;
use \Validator;
use Session;
use education\User;
use Auth;
use DB;
class personasController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-empleado', 'editar-empleado', 'estado-empleado', 'ver-empleado'])) {
          $personas = Personas::paginate(5);;

           /*$personas = DB::table('PERSONAS as p')
                 ->join('puestos as pu', 'p.id_puesto', '=', 'pu.id_puesto')

            ->get();*/

          return view('personas.index', ['personas'=>$personas, 'items'=>$this->menu]);

          //$personas_roles=Personas::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto');
          //return view('personas.index', ['personas_roles'=>$personas_roles, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-empleado')) {
          $puestos = Puestos::All();
          foreach ($puestos as $key => $row) {
            $p[$row->id_puesto] = mb_strtoupper($row->nombre_puesto);
          }
          return view('personas.npersona', ['puestos'=>$p, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          'date_format' => 'Hey! El valor de campo :attribute tiene que ser una fecha valida con el formato año-mes-día',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombres_persona' => 'required|min:5|max:50',
                'apellidos_persona' => 'required|min:5|max:50',
                'cui_persona' => 'required|min:13|max:13|unique:PERSONAS,cui_persona',
                'direccion_persona' => 'required|min:5|max:60',
                'fecha_nacimiento_persona' => 'required|min:10|max:10|date_format:"Y-m-d"',
                'telefono_persona' => 'required|min:8|max:10',
                'telefono_auxilia_persona' => 'min:8|max:10',
                'id_puesto' => 'required',
                'correo_persona'=>'required'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $result = Personas::setPersona(
        $request['nombres_persona'], $request['apellidos_persona'], $request['cui_persona'],
        $request['direccion_persona'], $request['fecha_nacimiento_persona'], $request['telefono_persona'],
        $request['telefono_auxilia_persona'], $request['id_puesto'], $request['correo_persona']
        );

        Session::flash('mensaje', $result[0]->msg);
        return redirect('/personas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('ver-empleado')) {
          $persona = Personas::findPersona($id);
          return view('personas.mpersona', ['persona'=>$persona, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-empleado')) {
          $persona = Personas::findPersona($id);
          $puestos = Puestos::All();
          foreach ($puestos as $key => $row) {
            $p[$row->id_puesto] = mb_strtoupper($row->nombre_puesto);
          }
          return view('personas.epersona', ['persona'=>$persona, 'puestos'=>$p, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          'date_format' => 'Hey! El valor de campo :attribute tiene que ser una fecha valida con el formato año-mes-día',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombres_persona' => 'required|min:5|max:50',
                'apellidos_persona' => 'required|min:5|max:50',
                'cui_persona' => 'required|min:13|max:13|unique:PERSONAS,cui_persona,'.$id.',id_persona',
                'direccion_persona' => 'required|min:5|max:60',
                'fecha_nacimiento_persona' => 'required|min:10|max:10|date_format:"Y-m-d"',
                'telefono_persona' => 'required|min:8|max:10',
                'telefono_auxilia_persona' => 'min:8|max:10',
                'id_puesto' => 'required',
                'correo_persona'=>'required'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $result = Personas::updatePersona(
        $id,
        $request['nombres_persona'], $request['apellidos_persona'], $request['cui_persona'],
        $request['direccion_persona'], $request['fecha_nacimiento_persona'], $request['telefono_persona'],
        $request['telefono_auxilia_persona'], $request['id_puesto'], $request['correo_persona']
        );

        Session::flash('mensaje', $result[0]->msg);
        return redirect('/personas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Personas::statePersona($id, FALSE);
        } else {
          Personas::statePersona($id, TRUE);
        }
        return response()->json('Listo!!!');
    }

    /*
      Metodo para buscar los nombres completos de las personas para el
      Autocompletado
    */
    public function bscautomplete(Request $request)
    {
      $coincidencia = $request['query'];
     $p = array();
    $datos = Personas::getPersonasLike($coincidencia);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona.' - -  Puesto: '.$row->puesto), 'data'=>$row->id_persona);
        array_push($p, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$p));//devolviendo el json para la respuesta
    }

    /*Busca a una persona por el parametro de busqueda y que tenga el puesto docente*/
    public function buscadocente(Request $request)
    {
      $q = $request['query'];
      $d = array();
      $datos = Personas::getPersonasLikeDocente($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona), 'data'=>$row->id_persona);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }
}