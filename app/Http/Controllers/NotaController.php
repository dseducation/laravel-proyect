<?php



namespace education\Http\Controllers;
use Illuminate\Http\Request;

use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\RespondeActividad;
use education\actividad;
use education\User;
use education\Estudiantes;
use education\Repositories\estudiantesRepository;
use Session;
use Auth;
use DB;


class NotaController extends Controller
{
    protected $menu;
    protected $actividades;
  	protected $ractividades;
  	protected $unidades;
  	protected $estudiantes;
  	public function __construct(estudiantesRepository $estudiante, RespondeActividadesRepository $ractividad, unidadesRepository $unidad, actividadesRepository $actividad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->ractividades = $ractividad; 
    $this->unidades = $unidad;
    $this->actividades = $actividad;
    $this->estudiantes = $estudiante;
  }

   public function index(Request $request)
    {

      /*
      if (isset($request['a']) && isset($request['b'])) {
        $this->docentes->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada
        //$c=$this->docentes->id_usuario = Auth::user()->id;
        $cursos = $this->docentes->getCursos();//obtenemos los cursos asignados al docente
        $b = false;
        $datos = array();
        foreach ($cursos as $key => $row) {
          if ($request['a'] == $row->id_asignacion_area) {
            $b = true;
            $datos['curso'] = 'Nombre del Curso: '.ucwords($row->nombre_area);
            $datos['grado'] = 'Grado: '.ucwords($row->nombre_grado);
            $datos['nivel'] = 'Nivel: '.ucwords($row->nombre_nivel);
            $datos['carrera'] = 'Carrera: '.ucwords($row->nombre_carrera);
            $datos['seccion'] = 'Seccion: '.ucwords($row->nombre_seccion);
          }
        }
        if ($b) {
            $this->estudiantes->curso = $request['a'];
            $this->unidades->fecha_inicio = date('Y-m-d');
            $this->unidades->fecha_final = date('Y-m-d');
            $unidad = $this->unidades->getUnidadActual();
            $nomina = $this->estudiantes->getEstudiantesCurso();
        } 
        else {
          Session::flash('mensaje', 'No se pudo procesar la solicitud, porque no tienes asignado el curso o el curso no existe!!!');
          return redirect('/docente');
        }
      } else {
        Session::flash('mensaje', 'No se pudo procesar la solicitud, porque los datos de la petición no son validos!!');
        return redirect('/docente');
      }

      return view('docentes.zonas', ['items'=>$this->menu, 'datos'=>$datos, 'nomina'=>$nomina, 'unidad'=>$unidad]);
*/

// ------ FUNCION SERVIDOR LOCAL  --- SI FUNCIONA 
       if (isset($request['a']))
       {
          $this->estudiantes->curso = $request['a'];
          $this->estudiantes->area = $request['b'];

          $this->unidades->fecha_inicio = date('Y-m-d');
          $this->unidades->fecha_final = date('Y-m-d');

          $areas=$this->ractividades->getIdRespuestaActividad();
          $unidades = $this->unidades->getUnidadActual();//traigo la unidad actual
          $this->ractividades->unidad=$unidades['id_unidad'];
          $this->estudiantes->unidad=$unidades['id_unidad'];
          
          $items = $this->estudiantes->getDetalleNota2();
          $items2 = $this->estudiantes->gettotalNota();


        return view('Nota.index', [ 'items'=>$this->menu, 'notas'=>$items, 'total'=>$items2, 'curso'=>$request['a'], 'curso'=>$request['b']]);



       }

       else {
        Session::flash('mensaje', 'No se pudo procesar la solicitud, porque los datos de la petición no son validos!!');
        return redirect('/docente');
      }

 

}
}