<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\Controller;
use \Validator;
use Session;
use education\User;
use Auth;
use education\Images;

class ImageController extends Controller
{
	public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-grado', 'editar-grado', 'estado-grado'])) {
         
          return view('images.index');
        }
        return abort(403);
    }

      public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-plannivel')) {
          $niveles = Niveles::All();
          $jornadas = Jornadas::All();
          $planes = Planes::All();
          foreach ($niveles as $key => $row) {
            $n[$row->id_nivel] = mb_strtoupper($row->nombre_nivel);
          }
          foreach ($jornadas as $key => $row) {
            $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);
          }
          foreach ($planes as $key => $row) {
            $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);
          }
          return view('nivelesplanesjornadas.nnivelplanjornada', ['niveles'=>$n, 'jornadas'=>$j, 'planes'=>$p, 'items'=>$this->menu]);
        }
        return abort(403);
    }
    //
}
