<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Niveles_grados;
use education\Grados;
use education\Secciones;
use education\Jornadas;
use education\Planes;
use education\Carreras;
use \Validator;
use Session;
use education\User;
use Auth;
class gradosnivelesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-gradonivel', 'editar-gradonivel', 'estado-gradonivel'])) {
          $datos = Niveles_grados::getNivelesGrados();
         
          return view('gradosniveles.index', ['datos'=>$datos, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-gradonivel')) {
          $grados = Grados::All();
          $secciones = Secciones::All();
          $planes = Planes::All();
          $jornadas = Jornadas::All();
          $carreras = Carreras::All();
          $n = array();
          foreach ($grados as $key => $row) {
            $g[$row->id_grado] = mb_strtoupper($row->nombre_grado);
          }
          foreach ($secciones as $key => $row) {
            $s[$row->id_seccion] = mb_strtoupper($row->nombre_seccion);
          }
          foreach ($planes as $key => $row) {
            $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);
          }
          foreach ($jornadas as $key => $row) {
            $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);
          }
          foreach ($carreras as $key => $row) {
            $c[$row->id_carrera] = mb_strtoupper($row->nombre_carrera);
          }
          return view('gradosniveles.ngradonivel', ['grados'=>$g, 'secciones'=>$s, 'planes'=>$p, 'jornadas'=>$j, 'carreras'=>$c, 'niveles'=>$n, 'plan'=>null, 'jornada'=>null, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'id_nivel_plan_jornada' => 'required',
                'id_grado' => 'required',
                'id_carrera' => 'required',
                'id_seccion' => 'required',
                'cuota_inscripcion' => 'required|regex:/^[0-9]+(\.{1}[0-9]{2})$/',
                'cuota_mensualidad' => 'required|regex:/^[0-9]+(\.{1}[0-9]{2})$/'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $npj = $request['id_nivel_plan_jornada'];
      $grado = $request['id_grado'];
      $carrera = $request['id_carrera'];
      $seccion = $request['id_seccion'];
      $inscripcion = $request['cuota_inscripcion'];
      $mensualidad = $request['cuota_mensualidad'];
      $result = Niveles_grados::setGradoNivel($npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/gradoniveles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-gradonivel')) {
          $grados = Grados::All();
          $secciones = Secciones::All();
          $planes = Planes::All();
          $jornadas = Jornadas::All();
          $carreras = Carreras::All();
          foreach ($grados as $key => $row) {
            $g[$row->id_grado] = mb_strtoupper($row->nombre_grado);
          }
          foreach ($secciones as $key => $row) {
            $s[$row->id_seccion] = mb_strtoupper($row->nombre_seccion);
          }
          foreach ($planes as $key => $row) {
            $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);
          }
          foreach ($jornadas as $key => $row) {
            $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);
          }
          foreach ($carreras as $key => $row) {
            $c[$row->id_carrera] = mb_strtoupper($row->nombre_carrera);
          }
          $dato = Niveles_grados::findGradoNivel($id);
          $npj = Niveles_grados::findNivelPJ($dato->id_nivel_plan_jornada);
          $n = array($npj[0]->id_nivel_plan_jornada => mb_strtoupper($npj[0]->nombre_nivel));
          $plan = $npj[0]->id_plan;
          $jornada = $npj[0]->id_jornada;
          return view('gradosniveles.egradonivel', ['grados'=>$g, 'secciones'=>$s, 'planes'=>$p, 'jornadas'=>$j, 'carreras'=>$c, 'dato'=>$dato, 'niveles'=>$n, 'plan'=>$plan, 'jornada'=>$jornada, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ////crear el arreglo de los mensajes de validacion
            $mensajes = array(
            'required' => 'Hey! EL campo :attribute es requerido!!!.',
            'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
            'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
            'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
            );

            $v = Validator::make(
            $request->all(),
             [
                  'id_nivel_plan_jornada' => 'required',
                  'id_grado' => 'required',
                  'id_carrera' => 'required',
                  'id_seccion' => 'required',
                  'cuota_inscripcion' => 'required|regex:/^[0-9]+(\.{1}[0-9]{2})$/',
                  'cuota_mensualidad' => 'required|regex:/^[0-9]+(\.{1}[0-9]{2})$/'
              ],
              $mensajes);

          if ($v->fails())
          {
              return redirect()->back()->withInput()->withErrors($v->errors());
          }
        $npj = $request['id_nivel_plan_jornada'];
        $grado = $request['id_grado'];
        $carrera = $request['id_carrera'];
        $seccion = $request['id_seccion'];
        $inscripcion = $request['cuota_inscripcion'];
        $mensualidad = $request['cuota_mensualidad'];
        $result = Niveles_grados::updateGradoNivel($id, $npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad);
        Session::flash('mensaje', $result[0]->msg);
        return redirect('/gradoniveles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Niveles_grados::stateGradoNivel($id, FALSE);
        } else {
          Niveles_grados::stateGradoNivel($id, TRUE);
        }
        $datos = Niveles_grados::getNivelesGrados();
        return response()->json($datos);
    }
}
