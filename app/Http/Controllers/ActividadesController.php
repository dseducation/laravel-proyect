<?php



namespace education\Http\Controllers;



use Illuminate\Http\Request;



use education\Http\Requests;

use education\Http\Requests\ActividadesRequest;

use education\Http\Controllers\menuController;

use education\Repositories\actividadesRepository;

use education\Repositories\unidadesRepository;

use education\Repositories\userRepository;

use education\actividad;

use education\tipo_actividad;

use education\User;

use Session;

use Auth;

//use Carbon\Carbon;



class ActividadesController extends Controller

{

  protected $menu;

  protected $actividades;

  protected $unidades;

  protected $users;

  public function __construct(actividadesRepository $actividad, unidadesRepository $unidad, userRepository $User)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->actividades = $actividad;

    $this->unidades = $unidad;

    $this->users = $User;

  }

   public function index(Request $request)

    {

       

        $this->actividades->id_area = $request['a'];
        

        $items = $this->actividades->getActividadCurso();

        return view('Actividad.index', ['items'=>$this->menu, 'actividades'=>$items, 'curso'=>$request['a']]);

    }



      public function create(Request $request)

    {

     

        $items = $this->actividades->getActividadCurso();
        $tipo_actividad = tipo_actividad::All();

          foreach ($tipo_actividad as $key => $row) {

            $ta[$row->id_tipo_actividad] = mb_strtoupper($row->nombre_actividad);

          }

        return view('Actividad.nactividad', ['tipo_actividad'=>$ta,'items'=>$this->menu, 'curso'=>$request['a']]);



            

    }





    

     public function store(ActividadesRequest $request)

    {

      try {

      	//$date = Carbon::now();



           $this->unidades->fecha_inicio = date('Y-m-d');

            $this->unidades->fecha_final = date('Y-m-d');

            $unidad = $this->unidades->getUnidadActual();

        

        $this->actividades->id_usuario = Auth::user()->id;

        $this->actividades->id_asignacion_area = $request['curso'];

        $this->actividades->id_unidad = $unidad['id_unidad'];

        $this->actividades->id_area = $request['curso'];

        $this->actividades->id_tipo_actividad = $request['id_tipo_actividad'];

        $this->actividades->nombre_actividad = $request['nombre_actividad'];

        $this->actividades->descripcion_actividad = $request['descripcion_actividad'];

        $this->actividades->nota_total = $request['nota_total'];

        $this->actividades->fecha_entrega = $request['fecha_entrega'];

        

        $this->actividades->setActividad();

      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'la actividad ha sido registrada exitosamente!!!');

        return redirect('/Actividad');

    }

    



/*

     public function store(Request $request)

    {

      //crear el arreglo de los mensajes de validacion

          $mensajes = array(

          'required' => 'Hey! EL campo :attribute es requerido!!!.',

          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',

          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',

          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',

          'date_format' => 'Hey! El valor de campo :attribute tiene que ser una fecha valida con el formato año-mes-día',

          );



          $v = Validator::make(

          $request->all(),

           [

            'id_actividad' => 'required',

            'nombre_actividad' => 'required|max:60',

            'descripcion_actividad' => 'required|max:60',

            'fecha_entrega' => 'required|date_format:"Y-m-d"',

            'nota_total' => 'required'



            ],

            $mensajes);



        if ($v->fails())

        {

            return redirect()->back()->withInput()->withErrors($v->errors());

        }

        $result = Personas::setPersona(

        $request['id_actividad'], $request['apellidos_persona'], $request['cui_persona'],

        $request['direccion_persona'], $request['fecha_nacimiento_persona'], $request['telefono_persona'],

        $request['telefono_auxilia_persona'], $request['id_puesto'], $request['correo_persona']

        );



        Session::flash('mensaje', $result[0]->msg);

        return redirect('/personas');



    }

    */

}



