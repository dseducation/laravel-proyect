<?php

namespace education\Http\Controllers;
use Illuminate\Http\Request;
use education\Http\Requests\inscripcionRequest;
use education\Http\Controllers\menuController;
use education\Repositories\estudiantesRepository;
use education\Repositories\tutorestudianteRepository;
use education\Repositories\referenciasRepository;
use education\Repositories\userRepository;
use education\Repositories\roleuserRepository;
use education\Repositories\rolesRepository;
use education\Departamento;
use education\Planes;
use education\Estudiantes;
use education\Jornadas;
use \Validator;
use Session;
use education\User;
use education\Niveles;
use DB;
use Auth;

class PdfController extends Controller
{

      protected $menu;
      protected $estudiantes;
      protected $tutores;
      protected $referencias;
      protected $users;
      protected $roleusers;
      protected $roles;
      public function __construct(estudiantesRepository $estudiante, tutorestudianteRepository $tutor, referenciasRepository $referencia, userRepository $user, roleuserRepository $roleuser, rolesRepository $rol)
      {
        $m = new menuController();
        $this->menu = $m->index();
        $this->estudiantes = $estudiante;
        $this->tutores = $tutor;
        $this->referencias = $referencia;
        $this->users = $user;
        $this->roleusers = $roleuser;
        $this->roles = $rol;
      }




	public function invoice() 
     {

        
       
        $data = $this->getData();
        $date = date('Y-m-d H:i:s');
        $invoice = "2222";
        $view =  \View::make('report.invoice', compact('data', 'date', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        return $pdf->stream('invoice');
    }
 
    public function getData() 
    {


        $estudiantedata = $this->estudiantes->getUltimoEstudianteInscrito();
        /*$datos = array();
        foreach ($estudiantedata as $key => $row) 
        {
          
            $datos['nombre'] = ($row->nombre_estudiante);
            return $datos;
           
          
        }
        */

        $data =[
          'id'=>$estudiantedata->id,
          'nombre' => $estudiantedata->nombre,
          'apellidos' => $estudiantedata->apellido,
          'fecha' => $estudiantedata->fecha,
          'direccion' => $estudiantedata->direccion,
          'colonia' => $estudiantedata->colonia,
          'zona' => $estudiantedata->zona,
          'telefono' => $estudiantedata->telefono,
          'referencia' => $estudiantedata->nombre_ref,
          'correo_estudiante' => $estudiantedata->correo_estudiante,
          'fecha_registro_estudiante' => $estudiantedata->fecha_registro_estudiante,
          'enfermedad_padecida_estudiante' => $estudiantedata->enfermedad_padecida_estudiante,
          'medicamento_recomendado_estudiante' => $estudiantedata->medicamento_recomendado_estudiante,
          'alergico_estudiante' => $estudiantedata->alergico_estudiante,
          'hospital_estudiante' => $estudiantedata->hospital_estudiante,
          'tipo_sangre_estudiante' => $estudiantedata->tipo_sangre_estudiante,
          'observaciones_estudiante' => $estudiantedata->observaciones_estudiante,
          'empresa_telefonica' => $estudiantedata->empresa_telefonica,
          'nombre_carrera' => $estudiantedata->nombre_carrera,
          'nombre_grado' => $estudiantedata->nombre_grado,
          'nombre_nivel' => $estudiantedata->nombre_nivel,
          'parentesco_referencia' => $estudiantedata->parentesco_referencia,
          'telefono_referencia' => $estudiantedata->telefono_referencia,
          'apellidos_tutor' => $estudiantedata->apellidos_tutor,
          'cui_tutor' => $estudiantedata->cui_tutor,
          'direccion_tutor' => $estudiantedata->direccion_tutor,
          'telefono_primario_tutor' => $estudiantedata->telefono_primario_tutor,
          'telefono_trabajo_tutor' => $estudiantedata->telefono_trabajo_tutor,
          'direccion_tutor' => $estudiantedata->direccion_tutor,
          'telefono_trabajo_tutor' => $estudiantedata->telefono_trabajo_tutor,
          'cargo_tutor' => $estudiantedata->cargo_tutor,

        ];

        return $data;

/*
        $estudiante = $this->estudiantes->getUltimoEstudianteInscrito();

        $data =  [
            'quantity'      =>  $estudiante->apellidos_estudiante,
            'description'   => 'some ramdom text',
            'price'   => '500',
            'total'     => '500'
        ];
        */
        }
    
}
