<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Carreras;
use \Validator;
use Session;
use education\User;
use Auth;
class carrerasController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = User::where('id', '=', Auth::user()->id)->first();
      if ($user->can(['crear-carrera', 'editar-carrera', 'estado-carrera'])) {
        $carreras = Carreras::All();
        return view('carreras.index', ['carreras'=>$carreras, 'items'=>$this->menu]);
      }
      return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', '=', Auth::user()->id)->first();
        if ($user->can('crear-carrera')) {
          return view('carreras.ncarrera', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_carrera' => 'required|min:2|max:70|unique:CARRERAS,nombre_carrera',
                'nombre_corto_carrera' => 'required|min:2|max:15',
                'descripcion_carrera' => 'required|min:2|max:500'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_carrera'];
      $nombreCorto = $request['nombre_corto_carrera'];
      $descripcion = $request['descripcion_carrera'];
      $result = Carreras::setCarrera($nombre, $nombreCorto, $descripcion);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/carreras');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', '=', Auth::user()->id)->first();
        if ($user->can('editar-carrera')) {
          $carrera = Carreras::findCarrera($id);
          return view('carreras.ecarrera', ['carrera'=>$carrera, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_carrera' => 'required|min:2|max:70|unique:CARRERAS,nombre_carrera,'.$id.',id_carrera',
                'nombre_corto_carrera' => 'required|min:2|max:15',
                'descripcion_carrera' => 'required|min:2|max:500'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_carrera'];
      $nombreCorto = $request['nombre_corto_carrera'];
      $descripcion = $request['descripcion_carrera'];
      $result = Carreras::updateCarrera($id, $nombre, $nombreCorto, $descripcion);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/carreras');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Carreras::stateCarrera($id, FALSE);
        } else {
          Carreras::stateCarrera($id, TRUE);
        }
        $carreras = Carreras::All();
        return response()->json($carreras);
    }
}
