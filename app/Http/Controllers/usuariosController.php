<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\User;
use education\Roles;
use education\Personas;
use education\Usuario_persona;
use \Validator;
use Session;
use Mail;
use PDF;
use DB;
use Hash;
use education\Role;
use Auth;
class usuariosController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-usuario')) {
          $usuarios = User::paginate(5);
          /*$usuarios =  DB::table('users as u')
                 ->join('role_user as ru', 'u.id', '=', 'ru.user_id')
                 ->join('roles as r', 'ru.role_id', '=', 'r.id')
                 ->select('u.name as nombre', 'u.email as correo', 'r.name as rol')

            ->get();*/
          return view('usuarios.index', ['usuarios'=>$usuarios, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-usuario')) {
          $roles = Role::All();
          foreach ($roles as $key => $row) {
            $r[$row->id] = mb_strtoupper($row->name);
          }
          return view('usuarios.nusuario', ['roles'=>$r, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $messages = array(
          'id_persona.required' => 'Hey! EL campo persona es requerido!!!.',
          'id_rol.required' => 'Hey! EL campo rol es requerido!!!.',
          );

          $v = Validator::make(
          $request->all(),
           [
                'id_persona' => 'required',
                'id_rol' => 'required',
            ],
            $messages);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $id = $request['id_persona'];
      $rol = $request['id_rol'];
      $persona = Personas::findPersona($id);
      // Separamos los Apellidos
      $aux = $persona->fecha_nacimiento_persona;
      $fecha = '';
      for ($i=0; $i < strlen($aux); $i++) {//quitamos los guiones de la fecha de nacimiento
        if ($aux[$i] != '-') {
          $fecha .= $aux[$i];
        }
      }

      list($pri_apellido) = explode(" ",$persona->apellidos_persona);
      list($pri_nombre) = explode(" ",$persona->nombres_persona);
      $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);
      //$pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
      $pass = 'admin123';
      $correo = mb_strtolower($persona->nombres_persona[0].$pri_apellido.$fecha).'@webusiness.co';

       $password = Hash::make($pass);//password cifrado
      DB::beginTransaction();
      try {
        $result = User::setUsuario($usuario, $password, $fecha, $correo, $rol);
        //return $result[0];
        //var_dump($result);
        //return $result['id'];
        //echo $result[0]->id;
        $msg = 'No se pudo registrar al usuario, porque ya existe en el base de datos!!!';
        if ($result[0]->id != 0) {
          //echo $result[0]->id;
          Usuario_persona::setUsuarioPersona($result[0]->id, $id);
          User::setRolUsuario($result[0]->id, $rol);
          $msg = 'Se a registrado al nuevo usuario exitosamente!!!';
        }
        $html = '<strong>usuario: </strong>'.$usuario.'<br />';
        $html .= '<strong>contraseña:</strong> '.$pass.'<br />';
        $html .= '<strong>Correo Institucional:</strong> '.$correo;


        //PDF::SetTitle('Registro Usuario');
        //PDF::AddPage();
        //PDF::Write(0, $html);
        // output the HTML content
        //PDF::writeHTML($html, true, false, true, false, '');
        //PDF::Output('registro.pdf', 'D');
        DB::commit();
      } catch (Exception $e) {
        DB::rollBack();
      }
      Session::flash('mensaje', $msg);
      return redirect('/usuarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
