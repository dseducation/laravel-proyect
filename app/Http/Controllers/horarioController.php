<?php


namespace education\Http\Controllers;



use Illuminate\Http\Request;
use education\Http\Requests\horariosRequest ;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Asignacion_areas;
use education\Repositories\HorarioRepository;
use education\horario;
use education\Areas;
use \Validator;
use Session;
use education\User;
use Auth;
use DB;



class horarioController extends Controller
{

	protected $menu;

  protected $horarios;

  public function __construct(HorarioRepository $horario)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->horarios = $horario;

  }

   public function index(Request $request)

    {

        

        //$items = $this->horarios->getAllCalendarios();



            $query=trim($request->get('searchText'));

          $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','1')
         
          ->paginate(50);
          /*
          QUERY QUE MUESTRA TODAS LAS ACTIVIDADES DEL CALENDARIO POR DIA
          select distinct * from calendario as c join DIAS as d on c.dia=d.id_dia
          join niveles_grados as ng on ng.id_nivel_grado=c.id_nivel_grado
          join grados as g on ng.id_grado=g.id_grado
          join carreras as ca on ng.id_carrera=ca.id_carrera
          join niveles_planes_jornadas as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
          join niveles as n on npj.id_nivel=n.id_nivel
          join jornadas as j on npj.id_jornada=j.id_jornada
          join planes as pl on npj.id_plan=pl.id_plan
          join areas as ar on ar.id_area=c.id_area

          where c.dia=1
          */

           $martes=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','2')
         
          ->paginate(50);

          //MIERCOLES

          $miercoles=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','3')
         
          ->paginate(50);

          //jueves

           $jueves=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','4')
         
          ->paginate(50);

          //viernes

           $viernes=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','5')
         
          ->paginate(50);

          //sabado

           $sabado=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','6')
         
          ->paginate(50);
         
          

          //domingo

           $domingo=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.dia','LIKE', '%'.$query.'%')
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=','7')
         
          ->paginate(50);
          
          /*

				select
				     c.dia, c.hora_inicia, c.hora_fin,c.asignacion_area, aa.id_salon, aa.id_area, a.nombre_area, d.dia 
				   
				  from calendario as c
				  join asignacion_areas as aa on c.asignacion_area=aa.id_asignacion_area
				  join areas as a on aa.id_area=a.id_area
				  left join DIAS as d on c.dia=d.id_dia
				  
				 
				    
				    order by c.dia
    		*/

        return view('horarios.index', ['items'=>$this->menu, 'horarios'=>$items, 'martes'=>$martes, 'miercoles'=>$miercoles, 'jueves'=>$jueves, 'viernes'=>$viernes, 'sabado'=>$sabado, 'domingo'=>$domingo,  'searchText'=>$query]);

    }



      public function create(Request $request)

    {

      //traigo todos los grados, niveles y jornadas disponibles
      /*
        select DISTINCT ng.id_nivel_grado, g.nombre_grado, ca.nombre_carrera, n.nombre_nivel, j.nombre_jornada, pl.nombre_plan from grados as g join niveles_grados as ng on g.id_grado=ng.id_grado
        join carreras as ca on ca.id_carrera=ng.id_carrera
        join niveles_planes_jornadas as npj on npj.id_nivel_plan_jornada=ng.id_nivel_plan_jornada
        join niveles as n on n.id_nivel=npj.id_nivel
        join jornadas as j on j.id_jornada=npj.id_jornada
        join planes as pl on pl.id_plan=npj.id_plan
      */

        $user = User::where('id', Auth::user()->id)->first();


          if ($user->can('crear-horario')) {


        $carreras =  DB::table('GRADOS as g')
          ->join('NIVELES_GRADOS as ng', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ca.id_carrera', '=', 'ng.id_carrera') 
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'n.id_nivel', '=', 'npj.id_nivel')
          ->join('JORNADAS as j', 'j.id_jornada', '=', 'npj.id_jornada')
          ->join('PLANES as pl', 'pl.id_plan', '=', 'npj.id_plan')   
         ->select(DB::raw('DISTINCT(g.nombre_grado)'), 'ca.nombre_carrera', 'ca.id_carrera', 'ng.id_nivel_grado', 'ca.nombre_corto_carrera','n.nombre_nivel','j.nombre_jornada','pl.nombre_plan')
          ->where('g.estado_grado','=','1')
          ->paginate(50);

          foreach ($carreras as $key => $row) {
            $tc[$row->id_nivel_grado] = mb_strtoupper($row->nombre_grado). '  '. mb_strtoupper($row->nombre_carrera). '   '. mb_strtoupper($row->nombre_nivel). ' - JORNADA: '. mb_strtoupper($row->nombre_jornada);
          }

          //traigo todos los DIAS disponibles

           $DIAS =  DB::table('DIAS as d')
         ->select('d.id_dia', 'd.dia')
          ->paginate(50);

          foreach ($DIAS as $key => $row) {
            $td[$row->id_dia] = mb_strtoupper($row->dia);
          }

          $areas = Areas::All();

          foreach ($areas as $key => $row) {

            $a[$row->id_area] = mb_strtoupper($row->nombre_area);

          }



          $n = array();//almaceno los valores de la busqueda por aa



        return view('horarios.nhorario', ['carreras'=>$tc, 'DIAS'=>$td, 'areas'=>$a,'items'=>$this->menu]);

        }

        return abort(403);



            

    }




     public function store(horariosRequest $request)

    {

      try {

        //$date = Carbon::now();



          

        

        $this->horarios->user = Auth::user()->id;

        $this->horarios->id_nivel_grado = $request['id_nivel_grado'];

        $this->horarios->id_area = $request['id_area'];

        $this->horarios->dia = $request['id_dia'];

        $this->horarios->hora_inicia = $request['hora_inicia'];

        $this->horarios->hora_fin = $request['hora_fin'];

        $this->horarios->estado_actividad_calendario = 1;
        
        $this->horarios->setCalendario();

      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'la actividad en el calendario ha sido registrada exitosamente!!!');

        return redirect('/horarios');

    }

    //funcion que busca las areas asignadas a una carrera 


     public function bscajx(Request $request)


    {


      $nivel = $request['nivel'];


      $areas = Asignacion_areas::getAreaWhere($nivel);


      return response()->json($areas);


    }



    public function edit($id)

    {

       
        $this->horarios->idcalendario = $id;

        $carreras =  DB::table('GRADOS as g')
          ->join('NIVELES_GRADOS as ng', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ca.id_carrera', '=', 'ng.id_carrera') 
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'n.id_nivel', '=', 'npj.id_nivel')
          ->join('JORNADAS as j', 'j.id_jornada', '=', 'npj.id_jornada')
          ->join('PLANES as pl', 'pl.id_plan', '=', 'npj.id_plan')   
         ->select(DB::raw('DISTINCT(g.nombre_grado)'), 'ca.nombre_carrera', 'ca.id_carrera', 'ng.id_nivel_grado', 'ca.nombre_corto_carrera','n.nombre_nivel','j.nombre_jornada','pl.nombre_plan')
          ->where('g.estado_grado','=','1')
          ->paginate(50);

          foreach ($carreras as $key => $row) {
            $tc[$row->id_nivel_grado] = mb_strtoupper($row->nombre_grado). '  '. mb_strtoupper($row->nombre_carrera). '   '. mb_strtoupper($row->nombre_nivel). ' - JORNADA: '. mb_strtoupper($row->nombre_jornada);
          }

          //traigo todos los DIAS disponibles

           $DIAS =  DB::table('DIAS as d')
         ->select('d.id_dia', 'd.dia')
          ->paginate(50);

          foreach ($DIAS as $key => $row) {
            $td[$row->id_dia] = mb_strtoupper($row->dia);
          }

          $areas = Areas::All();

          foreach ($areas as $key => $row) {

            $a[$row->id_area] = mb_strtoupper($row->nombre_area);

          }



          $n = array();//almaceno los valores de la busqueda por aa


          $dato = $this->horarios->getCalendarioById();

             //$dato = horario::findGradoNivel($id);

        return view('horarios.ehorario', ['carreras'=>$tc, 'DIAS'=>$td, 'areas'=>$a,'items'=>$this->menu, 'dato'=>$dato]);

        

    }


     public function update(Request $request, $id)

    {

        ////crear el arreglo de los mensajes de validacion

          

        $this->horarios->idcalendario = $id;

        $this->horarios->id_nivel_grado = $request['id_nivel_grado'];

        $this->horarios->id_area = $request['id_area'];

        $this->horarios->dia = $request['id_dia'];

        $this->horarios->hora_inicia = $request['hora_inicia'];

        $this->horarios->hora_fin = $request['hora_fin'];

        $this->horarios->estado_actividad_calendario = 1;
        
        $this->horarios->updateCalendario();

        Session::flash('mensaje', 'Se han actualizado los datos del foro exitosamente!!!');

        return redirect('/horarios');

    }



    
}
