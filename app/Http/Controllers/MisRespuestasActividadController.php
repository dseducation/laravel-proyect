<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;


use education\Http\Requests;
use education\Http\Requests\MisRespuestasActividadRequest;
use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\RespondeActividad;
use education\actividad;
use education\User;
use Session;
use Auth;
use DB;

class MisRespuestasActividadController extends Controller
{
   	protected $menu;
    protected $actividades;
  	protected $ractividades;
  	protected $unidades;
  	public function __construct(RespondeActividadesRepository $ractividad, unidadesRepository $unidad, actividadesRepository $actividad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->ractividades = $ractividad; 
    $this->unidades = $unidad;
    $this->actividades = $actividad;
  }

   public function index(Request $request)
    {
        
        $this->ractividades->id_actividad = $request['a'];
       
        

        $items = $this->ractividades->getUsuariosRespuestActividades();
        $items1 = $this->ractividades->valorMaximoNota();
         
        return view('misrespuestasactividades.index', ['items'=>$this->menu, 'ractividades'=>$items, 'notas'=>$items1, 'curso'=>$request['a']]);
        /*
        $this->ractividades->id_actividad = $request['a'];
        $items = $this->ractividades->valorMaximoNota();
        return view('misrespuestasactividades.index', ['items'=>$this->menu, 'ractividades'=>$items, 'curso'=>$request['a']]);
        */

    }

    
      public function create(Request $request)
    {
        return view('misrespuestasactividades.nractividad', ['items'=>$this->menu, 'curso'=>$request['a']]);

            
    }



      public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$this->authorize('owner', $foros);
        $this->ractividades->id_respuesta_actividad = $id;


        
        


        $items1 = $this->ractividades->valorMaximoNota();
        $ractividad = $this->ractividades->getRespuestaActividadesById1();
        //$valor_maximo_nota = $this->ractividades->valorMaximoNota();
        return view('misrespuestasactividades.eractividad', ['items'=>$this->menu, 'ractividad'=>$ractividad,'notas'=>$items1]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MisRespuestasActividadRequest $request, $id)


    {


        
        
          //return back();

               
            

              $this->ractividades->id_respuesta_actividad = $id;
              $id_actividad=$id;
              $this->ractividades->estado_entrega_actividad = $request['estado_entrega_actividad'];
              $nota=$this->ractividades->calificacion = $request['calificacion'];
              $this->ractividades->observaciones_maestro = $request['observaciones_maestro'];
              $nota_max = $this->ractividades->valorMaximoNota2($id_actividad);//traigo el valor maximo de la nota
              $val_max_nota=intval($nota_max['nota_total']);

              $nota_total = $this->actividades->nota_total;//traigo todos los datos de actividades
              



                 $this->ractividades->updateRespuestaActividades();
                 Session::flash('mensaje', 'La actividad ha sido CALIFICADA exitosamente!!!');
          
                return redirect('/docente');

                
          
                        

                  

                

            }



             
            

       // }
     //}

        

        

     

    



         


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
