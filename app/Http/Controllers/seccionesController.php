<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Secciones;
use \Validator;
use Session;
use education\User;
use Auth;
class seccionesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-seccion', 'editar-seccion'])) {
          $secciones = Secciones::All();
          return view('secciones.index', ['secciones'=>$secciones, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-seccion')) {
          return view('secciones.nseccion', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_seccion' => 'required|min:1|max:10|unique:SECCIONES,nombre_seccion',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_seccion'];
      $result = Secciones::setSeccion($nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/secciones');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-seccion')) {
          $seccion = Secciones::findSeccion($id);
          return view('secciones.eseccion', ['seccion'=>$seccion, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_seccion' => 'required|min:1|max:10|unique:SECCIONES,nombre_seccion,'.$id.',id_seccion',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_seccion'];
      $result = Secciones::updateSeccion($id, $nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/secciones');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
