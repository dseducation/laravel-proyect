<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Roles;
use education\Opciones;
use education\Permisos_roles;
use \Validator;
use Session;
use education\Role;
use DB;
use education\Permission_role;
use education\User;
use Auth;
class rolesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-rol', 'editar-rol', 'estado-rol'])) {
          $roles = Role::All();
          return view('roles.index', ['roles'=>$roles, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-rol')) {
          $opciones = Opciones::All();
          $o = array();
          foreach ($opciones as $key => $row) {
            $o[$row->id_opcion] = mb_strtoupper($row->nombre_opcion);
          }
          return view('roles.nrol', ['opciones'=>$o, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'name' => 'required|min:5|max:40|unique:roles,name',
                'description' => 'required|min:5|max:100',
                'permiso' => 'required'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['name'];
      $descripcion = $request['description'];
      $permiso = $request['permiso'];
      try {
        DB::beginTransaction();
        $id = Role::setRol($nombre, $descripcion);
        foreach ($permiso as $key => $row) {
          if ($id[0]->id != 0) {
            Permission_role::setPermisoRol($id[0]->id, $row);
          }
        }
        DB::commit();
      } catch (Exception $e) {
        DB::rollBack();
        $msg = 'Lo sentimos a ocurrido un error al realizar el registro del rol!!!';
      }
      $msg = 'El nuevo rol '.$nombre.', fue registrado exitosamente!!!';
      Session::flash('mensaje', $msg);
      return redirect('/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $opciones = Opciones::All();
        $o = array();
        foreach ($opciones as $key => $row) {
          $o[$row->id_opcion] = mb_strtoupper($row->nombre_opcion);
        }
        $rol = Role::findRol($id);
        $permisos = Permission_role::findPermisos($id);
        return view('roles.erol', ['rol'=>$rol, 'permisos'=>$permisos, 'opciones'=>$o, 'items'=>$this->menu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //$datos = count($request['permisos']);
      $permisos = json_decode($request['permisos']);
      /*$datos = array();
      foreach ($permisos as $key => $permiso) {
        $aux = $permiso->permiso;
        array_push($datos, $aux);
      }*/
      try {
        DB::beginTransaction();
        foreach ($permisos as $key => $row) {
            Permission_role::setPermisoRol($id, $row->permiso);
        }
        $msg = 'Se han registrado los permisos exitosamente!!!';
        DB::commit();
      } catch (Exception $e) {
        DB::rollBack();
        $msg = 'Lo sentimos a ocurrido un error al realizar el registro del rol!!!';
      }
      $permisos = Permission_role::findPermisos($id);
      return response()->json(array(
                              'mensaje'=>$msg,
                              'permisos'=>$permisos
                                    ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Role::stateRol($id, FALSE);
        } else {
          Role::stateRol($id, TRUE);
        }
        //$roles = Role::All();
        return response()->json('Listo');
    }
}
