<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\Controller;
use education\Roles;
use education\Http\Controllers\menuController;
use \Validator;
use Session;
use education\User;
use education\Niveles;
use DB;
use Auth;

class UsuariosRolesController extends Controller
{

	 protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
	  public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['ver-usuario'])) {
          //$personas = Personas::All();
		 $personas = DB::table('PERSONAS as p')
                 ->join('puestos as pu', 'p.id_puesto', '=', 'pu.id_puesto')

            ->get();
            
          return view('verusuario.index', ['personas'=>$personas, 'items'=>$this->menu]);

          //$personas_roles=Personas::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto');
          //return view('personas.index', ['personas_roles'=>$personas_roles, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'bspersona.required' => 'Hey! EL campo :attribute es requerido!!!.',
          
          );

          $v = Validator::make(
          $request->all(),
           [
                'bspersona' => 'required|min:5|max:50',
               
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
       $id = $request['bspersona'];

      $persona = Personas::findPersona($id);
      

        Session::flash('mensaje', $result[0]->msg);
        return redirect('/verusuario');
    }


    //funcion de autocompletado de empleados
public function bscautomplete1(Request $request)
    {
     $f = $request['query'];
     $p = array();
     $datos = getPersonasLike1($f);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona), 'data'=>$row->id_persona);
        array_push($p, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$p));//devolviendo el json para la respuesta
    }
}
