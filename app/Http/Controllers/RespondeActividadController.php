<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Requests\RespondeActividadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\RespondeActividad;
use education\User;
use Session;
use Auth;

class RespondeActividadController extends Controller
{
  	protected $menu;
    protected $actividades;
  	protected $ractividades;
  	protected $unidades;
  	public function __construct(RespondeActividadesRepository $ractividad, unidadesRepository $unidad, actividadesRepository $actividad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->ractividades = $ractividad; 
    $this->unidades = $unidad;
    $this->actividades = $actividad;
  }

  public function index(Request $request)
    {
        
        $this->ractividades->id_actividad = $request['a'];
        $items = $this->actividades->getActividadCurso();
        return view('respuestaactividades.index', ['items'=>$this->menu, 'actividades'=>$items, 'actividad'=>$request['a'],'curso'=>$request['b']]);
    }

      public function create(Request $request)
    {
         
        return view('respuestaactividades.nractividad', ['items'=>$this->menu, 'curso'=>$request['actividad']]);

            
    }
    
     public function store(RespondeActividadesRequest $request)
    {


        $this->unidades->fecha_inicio = date('Y-m-d');
        $this->unidades->fecha_final = date('Y-m-d');
        
         $unidad = $this->unidades->getUnidadActual();//traigo la unidad actual


       
        $id_actividad=$this->ractividades->id_actividad=$request['curso'];
        $id_usuario=$this->ractividades->id_usuario = Auth::user()->id;
        $this->ractividades->comentarios = $request['comentarios'];
        $this->ractividades->ruta = $request['ruta'];
        $unidades=$this->ractividades->unidad=$unidad['id_unidad'];

        $areas=$this->ractividades->getIdActividad();


        $area=$this->ractividades->areas=$areas['id_area'];
        
        $nota_max = $this->ractividades->existRespuestaActividades($id_actividad,$id_usuario,$unidades,$area);//traigo todos los datos de las notas


         if(count($nota_max)==0)//si no se subio la tarea
         {

        
          $this->ractividades->setRespondeActividad($nota_max);//se ingresa la tarea
          Session::flash('mensaje', 'Bien hecho, la actividad ha sido registrada exitosamente!!!');
          
          return redirect('/respuestaactividades');

         }

         else //de lo contrario ya se entrego la tarea y no se puede subir 2 veces la misma
         {
           Session::flash('mensaje', 'No puedes entregar dos veces la misma tarea preguntale a tu profesor!!!');
          
          return redirect('/respuestaactividades');
         }

      

       


        
      } 
        
    



    
}
