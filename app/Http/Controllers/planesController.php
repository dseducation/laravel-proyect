<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Planes;
use \Validator;
use Session;
use education\User;
use Auth;
class planesController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-plan', 'editar-plan', 'estado-plan'])) {
          $planes = Planes::All();
          return view('planes.index', ['planes'=>$planes, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-plan')) {
          return view('planes.nplan', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_plan' => 'required|min:4|max:25|unique:PLANES,nombre_plan',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_plan'];
      //$client->create($request->all());
      //$clients = Client::all();
      //return \View::make('list', compact('clients'));
      $result = Planes::setPlanes($nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/planes');
      //return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-plan')) {
          $plan = Planes::findPlan($id);
          return view('planes.eplan', ['plan'=>$plan, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_plan' => 'required|min:4|max:25|unique:PLANES,nombre_plan,'.$id.',id_plan',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_plan'];
      $result = Planes::updatePlan($id, $nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/planes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
      $estado = $request['estado'];
      if ($estado == 1) {
        Planes::statePlan($id, false);//se cambia el estado a desactivado
      } else {
        Planes::statePlan($id, true);//se camia el estado a activado
      }
      $planes = Planes::All();
      return response()->json($planes);
    }
}
