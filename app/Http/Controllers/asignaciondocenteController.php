<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\User;
use education\Planes;
use education\Jornadas;
use education\Grados;
use education\Niveles;
use education\Niveles_planes_jornadas;
use education\Niveles_grados;
use education\Asignacion_docente;
use education\Personas;
use \Validator;
use Auth;
use DB;
use Session;
class asignaciondocenteController extends Controller
{
      protected $menu;
      public function __construct()
      {
        $m = new menuController();
        $this->menu = $m->index();
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-asignacion', 'editar-asignacion'])) {
          $docentes = Asignacion_docente::getNombreDocente();
          return view('asignaciondocente.index', ['items'=>$this->menu, 'docentes'=>$docentes]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-asignacion')) {
          $jornadas = Jornadas::All();
          $planes = Planes::All();
          $grados = Grados::All();
          $niveles = Niveles::All();
          $j = array();
          $p = array();
          $g = array();
          $n = array();
          foreach ($jornadas as $key => $row) {
            $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);
          }
          foreach ($planes as $key => $row) {
            $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);
          }
          foreach ($grados as $key => $row) {
            $g[$row->id_grado] = mb_strtoupper($row->nombre_grado);
          }
          foreach ($niveles as $key => $row) {
            $n[$row->id_nivel] = mb_strtoupper($row->nombre_nivel);
          }
          return view('asignaciondocente.nasignacion', ['items'=>$this->menu, 'planes'=>$p, 'jornadas'=>$j, 'grados'=>$g, 'niveles'=>$n]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $v = Validator::make(
          $request->all(),
           [
                'id_persona' => 'required',
                'curso' => 'required'
            ]);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $persona = $request['id_persona'];
      $cursos = $request['curso'];
      DB::beginTransaction();
      try {
        foreach ($cursos as $key => $curso) {
          Asignacion_docente::setAsignacionDocente($curso, $persona);
        }
        DB::commit();
        $msg = 'Se han registrado la asignación de cursos al docente!!!';
      } catch (Exception $e) {
        DB::rollBack();
        $msg = 'No se pudo registrar la asignación de cursos al docente, debido a un error en la transacción!!!';
      }
      Session::flash('mensaje', $msg);
      return redirect('/asignaciondocente');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asignaciones = Asignacion_docente::getCursosAsignados($id);
        $docente = Personas::findPersona($id);
        return view('asignaciondocente.masignacion', ['items'=>$this->menu, 'asignaciones'=>$asignaciones, 'docente'=>$docente]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $jornadas = Jornadas::All();
      $planes = Planes::All();
      $grados = Grados::All();
      $niveles = Niveles::All();
      $j = array();
      $p = array();
      $g = array();
      $n = array();
      foreach ($jornadas as $key => $row) {
        $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);
      }
      foreach ($planes as $key => $row) {
        $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);
      }
      foreach ($grados as $key => $row) {
        $g[$row->id_grado] = mb_strtoupper($row->nombre_grado);
      }
      foreach ($niveles as $key => $row) {
        $n[$row->id_nivel] = mb_strtoupper($row->nombre_nivel);
      }
      $asignaciones = Asignacion_docente::getCursosAsignados($id);
      $docente = Personas::findPersona($id);
      return view('asignaciondocente.easignacion', ['items'=>$this->menu, 'asignaciones'=>$asignaciones, 'docente'=>$docente, 'planes'=>$p, 'jornadas'=>$j, 'grados'=>$g, 'niveles'=>$n]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $cursos = json_decode($request['cursos']);
      DB::beginTransaction();
      try {
        foreach ($cursos as $key => $curso) {
          Asignacion_docente::setAsignacionDocente($curso->curso, $id);
        }
        DB::commit();
      } catch (Exception $e) {
        DB::rollBack();
      }
        $asignaciones = Asignacion_docente::getCursosAsignados($id);
        return response()->json($asignaciones);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      $estado = $request['estado'];
      if ($estado == 1) {
        Asignacion_docente::stateAsignacionDocente(FALSE, $id);
      } else {
        Asignacion_docente::stateAsignacionDocente(TRUE, $id);
      }
      return response()->json('Se ha cambiado el estado de la asignación!!!');
    }

    /*
      Obtiene los grados de acuerdo a nu nivel, plan y jornada
    */
    public static function getGrados(Request $request)
    {
      $n = $request['nivel'];
      $p = $request['plan'];
      $j = $request['jornada'];
      $id = Niveles_planes_jornadas::getIdNivelPlanJornada($p, $j, $n);
      if (isset($id->id_nivel_plan_jornada)) {
        $grados = Niveles_grados::getGrados($id->id_nivel_plan_jornada);
        return response()->json($grados);
      }

      return response()->json(array());
    }
}
