<?php

namespace education\Http\Requests;

use education\Http\Requests\Request;

class unidadesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $id = $this->route('unidades');
      if ($this->method() == 'PUT') {
        $nombre_rules = 'required|unique:UNIDADES,nombre_unidad,'.$id.',id_unidad';
      }else {
        $nombre_rules = 'required|unique:UNIDADES,nombre_unidad';
      }
        return [
            'nombre_unidad'=>'required|max:20',
            'fecha_inicio'=>'required|date_format:"Y-m-d"',
            'fecha_final'=>'required|date_format:"Y-m-d"'
        ];
    }
}
