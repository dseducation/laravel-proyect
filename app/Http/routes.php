<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('logueo', 'autenticacionController@index');
Route::group(['middleware' => 'auth'], function () {

Route::get('/', 'principalController@index');
Route::resource('roles', 'rolesController');
Route::get('niveles/ajxniveles', 'nivelesController@listado');
Route::resource('niveles', 'nivelesController');
Route::resource('planes', 'planesController');
Route::resource('jornadas', 'jornadasController');
Route::resource('puestos', 'puestosController');
Route::resource('secciones', 'seccionesController');
Route::resource('grados', 'gradosController');
Route::get('asignarniveles/obtenerajx', 'planesnivelesController@bscajx');
Route::resource('asignarniveles', 'planesnivelesController');
Route::resource('salones', 'salonesController');
Route::resource('areas/autocomplete', 'areasController@areas');
Route::resource('areas', 'areasController');
Route::get('personas/autocomplete', 'personasController@bscautomplete');
Route::get('verusuario/autocomplete', 'UsuariosRolesController@bscautomplete1');//control de busqueda de empleado por puesto cj
Route::get('personas/autocomplete/docente/', 'personasController@buscadocente');
Route::resource('personas', 'personasController');
Route::resource('carreras', 'carrerasController');
Route::resource('gradoniveles', 'gradosnivelesController');
Route::resource('pensum/pensumgrado', 'asignacionareasController@pensumGrado');
Route::resource('pensum/grados', 'asignacionareasController@grados');
Route::resource('pensum', 'asignacionareasController');
Route::resource('subopciones', 'subopcionesController');
Route::resource('permisosroles', 'permisosrolesController');
Route::resource('usuarios', 'usuariosController');
Route::resource('prueba', 'pruebaController');
Route::resource('asignaciondocente/grados', 'asignaciondocenteController@getGrados');
Route::resource('asignaciondocente', 'asignaciondocenteController');
Route::get('inscripcionestudiantes/constancia', 'inscripcionController@constancia');
Route::resource('inscripcionestudiantes', 'inscripcionController');
Route::resource('municipios', 'municipioController');
Route::resource('unidades', 'unidadesController');
Route::resource('docente', 'docenteController');
Route::resource('foros', 'forosController');
Route::resource('estudiantes', 'miscursosController');
Route::resource('respuestasforos', 'respuestasforosController');

//cambios hechos por jose florian
Route::resource('verestudiantes', 'EstudianteController');//cj
Route::resource('verusuario', 'UsuariosRolesController');//cj
Route::resource('verusuario', 'UsuariosRolesController');//cj

//Route::resource('actividades', 'ActividadesController');
Route::resource('Actividad', 'ActividadesController');//cj
Route::resource('misactividades',  'misActividadesController');//cj
Route::resource('respuestaactividades', 'RespondeActividadController');//cj
Route::resource('misrespuestasactividades', 'MisRespuestasActividadController');//cj


Route::get('report', 'PdfController@invoice');

Route::get('reportinscripcion', 'inscripcionController@reports');




//Route::get('constancia', 'reportesController@constancia_inscripcion');
//Route::get('pagos', 'reportesController@constancia_inscripcion');
});//fin del rout group
/*Route::get('/', function () {
    return view('welcome');
});*/

Route::auth();

//Route::get('/home', 'HomeController@index');
