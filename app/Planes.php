<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Planes extends Model
{
    protected $table = 'PLANES';

    /*
    Metodo para registrar un nuevo plan en la base de datos
    */
    public static function setPlanes($nombre)
    {
      return DB::select('CALL nuevo_plan(?)', array($nombre));
    }

    /*
    metodo para buscar los datos de un plan por su id
    */
    public static function findPlan($id)
    {
      return Planes::where('id_plan', $id)->first();
    }

    /*
    Metodo para actualizar los datos de un Plan
    */
    public static function updatePlan($id, $nombre)
    {
      return DB::select('CALL actualizar_plan(?, ?)', array($id, $nombre));
    }

    /*
    Metodo para cambiar el estado de un plan
    */
    public static function statePlan($id, $estado)
    {
      return DB::select('CALL estado_plan(?, ?)', array($id, $estado));
    }

    /*
      Metodo para obtener un array de los planes disponibles
    */
    public static function getPlanes()
    {
      $planes = array();
      foreach (PLANES::where('estado_plan', true)->get() as $key => $p) {
        $planes[$p->id_plan] = mb_strtoupper($p->nombre_plan);
      }
      return $planes;
    }
    
}
