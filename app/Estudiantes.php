<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Estudiantes extends Model
{
    protected $table = 'ESTUDIANTES';

     /*
      Metodo para obtener los datos de un alumno por su id
    */
    public static function findEstudiante($id)
    {
      return ESTUDIANTES::where('id_estudiante', $id)->first();
    }

}
