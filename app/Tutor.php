<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
    protected $table = 'TUTOR';
}
