<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'MUNICIPIO';
    /*
      Metodo para obtner todos los municipios de un departamento
    */
    public static function getMunicipios($depto)
    {
      $municipios = array();
      foreach (MUNICIPIO::All() as $key => $m) {
        $municipios[$m->id_municipio] = mb_strtoupper($m->nombre_municipio);
      }
      return $municipios;
    }
}
