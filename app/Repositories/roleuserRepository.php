<?php
namespace education\Repositories;

use education\Role_user;
use DB;
/**
 * Clase repositorio para tratar los datos de la tabla role_user
 */
class roleuserRepository
{
  public $role_id;
  public $user_id;

  //metodo para asignar un rol a un usuario
  public function setRoleUser()
  {
    DB::insert('INSERT INTO role_user(role_id, user_id) VALUES(?, ?)', array($this->role_id, $this->user_id));
  }

}
