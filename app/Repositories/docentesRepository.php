<?php
namespace education\Repositories;

use DB;
/**
 * Clase repositorio para el tratamiento de datos para el docente
 */
class docentesRepository
{
    public $id_persona;
    public $id_usuario;
    //metodo para obtener todo los cursos que tiene asignado un docente
    public function getCursos()
    {
        return DB::table('PERSONAS as p')
                 ->join('ASIGNACION_DOCENTE as ad', 'p.id_persona', '=', 'ad.id_persona')
                 ->join('ASIGNACION_AREAS as aa', 'ad.id_asignacion_area', '=', 'aa.id_asignacion_area')
                 ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
                 ->join('NIVELES_GRADOS as ng', 'aa.id_nivel_grado', '=', 'ng.id_nivel_grado')
                 ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
                 ->join('SECCIONES as s', 'ng.id_seccion', '=', 's.id_seccion')
                 ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')
                 ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                 ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
                 ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
                 ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
                 ->join('USUARIO_PERSONA as up', 'p.id_persona', '=', 'up.id_persona')
                 ->where('ad.estado_asignacion_docente', TRUE)
                 ->where('up.user_id',$this->id_usuario)
                 ->select('a.nombre_area', 'aa.id_asignacion_area', 'ad.id_asignacion_docente', 'g.nombre_grado', 's.nombre_seccion', 'c.nombre_carrera', 'n.nombre_nivel', 'pl.nombre_plan', 'j.nombre_jornada', 'up.user_id', 'a.id_area')
                 ->get();
    }

    /*

    QUERY PARA SELECCIONAR TODOS LOS CURSOS ASIGNADOS AL PROFESOR

    select a.nombre_area, aa.id_asignacion_area, ad.id_asignacion_docente, g.nombre_grado, s.nombre_seccion, c.nombre_carrera, n.nombre_nivel, pl.nombre_plan, j.nombre_jornada, up.user_id, a.id_area from PERSONAS as p join ASIGNACION_DOCENTE as ad on p.id_persona=ad.id_persona
                 join ASIGNACION_AREAS as aa on ad.id_asignacion_area=aa.id_asignacion_area
                 join AREAS as a on aa.id_area=a.id_area
                 join NIVELES_GRADOS as ng on aa.id_nivel_grado=ng.id_nivel_grado
                 join GRADOS as g on ng.id_grado=g.id_grado
                 join SECCIONES as s on ng.id_seccion=s.id_seccion
                 join CARRERAS as c on ng.id_carrera=c.id_carrera
                 join NIVELES_PLANES_JORNADAS as npj on  ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
                 join NIVELES as n on npj.id_nivel=n.id_nivel
                 join PLANES as pl on npj.id_plan=pl.id_plan
                 join JORNADAS as j on npj.id_jornada=j.id_jornada
                 join USUARIO_PERSONA as up on p.id_persona=up.id_persona
                 where ad.estado_asignacion_docente=TRUE and
                 up.user_id=3
               */

  /*select a.nombre_area, aa.id_asignacion_area, ad.id_asignacion_docente, g.nombre_grado, s.nombre_seccion, c.nombre_carrera, n.nombre_nivel, pl.nombre_plan, j.nombre_jornada
from PERSONAS p inner join ASIGNACION_DOCENTE ad
on p.id_persona = ad.id_persona inner join ASIGNACION_AREAS aa
on ad.id_asignacion_area = aa.id_asignacion_area inner join AREAS a
on aa.id_area = a.id_area inner join NIVELES_GRADOS ng
on aa.id_nivel_grado = ng.id_nivel_grado inner join GRADOS g
on ng.id_grado = g.id_grado inner join SECCIONES s
on ng.id_seccion = s.id_seccion inner join CARRERAS c
on ng.id_carrera = c.id_carrera inner join NIVELES_PLANES_JORNADAS npj
on ng.id_nivel_plan_jornada = npj.id_nivel_plan_jornada inner join NIVELES n
on npj.id_nivel = n.id_nivel inner join PLANES pl
on npj.id_plan = pl.id_plan inner join JORNADAS j
on npj.id_jornada = j.id_jornada inner join USUARIO_PERSONA up
on p.id_persona = up.id_persona
where ad.estado_asignacion_docente = true
order by n.id_nivel*/

}
