<?php

namespace education\Repositories;



use education\horario;

use DB;

/**

 * Clase repositorio para el tratamiento de datos para el docente

 */

class HorarioRepository

{

    public $idcalendario;

    public $dia;

    public $hora_inicia;

    public $hora_fin;

    public $id_nivel_grado;

    public $id_area;

    public $fecha_creacion;

    public $fecha_modificacion;

    public $user;

    public $estado_actividad_calendario;

    //metodo para obtener todo los cursos que tiene asignado un docente

    public function setCalendario()

    {

      DB::table('CALENDARIO')->insert([

                                  'user'=>$this->user,

                                  'dia'=>$this->dia,

                                  'hora_inicia'=>$this->hora_inicia,

                                  'hora_fin'=>$this->hora_fin,

                                  'id_nivel_grado'=>$this->id_nivel_grado,

                                  'id_area'=>$this->id_area,


                                  'estado_actividad_calendario'=>$this->estado_actividad_calendario
                                  

                                ]);

    }

      public function getCalendarioById()

  {

     return horario::where('idcalendario', $this->idcalendario)->first();

  }



     public function updateCalendario()

  {

    DB::table('CALENDARIO')->where('idcalendario', $this->idcalendario)

                      ->update([

                                  'dia'=>$this->dia,

                                  'hora_inicia'=>$this->hora_inicia,

                                  'hora_fin'=>$this->hora_fin,

                                  'id_nivel_grado'=>$this->id_nivel_grado,

                                  'id_area'=>$this->id_area,


                              ]);

  }







    /*



    QUERY PARA SELECCIONAR TODOS LOS horarios en el calendario


select
     c.dia, c.hora_inicia, c.hora_fin,c.asignacion_area, aa.id_salon, aa.id_area, a.nombre_area, d.dia 
   
  from calendario as c
  join asignacion_areas as aa on c.asignacion_area=aa.id_asignacion_area
  join areas as a on aa.id_area=a.id_area
  left join dias as d on c.dia=d.id_dia
  
 
    
    order by c.dia
    */


}

