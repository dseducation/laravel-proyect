<?php
namespace education\Repositories;

use education\Respuestas_foros;
use DB;
/**
 * Clase repositorio para tratar los datos de la tabla repsuetas_foros
 */
class respuestasforosRepository
{
  public $id_respuesta_foro;
  public $id_foro;
  public $mensaje_respuesta;
  public $fecha_respuesta;
  public $id_usuario;

  //metodo para registrar una nueva respuesta a un foro
  public function setRespuestaForo()
  {
    DB::table('RESPUESTAS_FOROS')->insert([
                                          'id_foro'=>$this->id_foro,
                                          'mensaje_respuesta'=>$this->mensaje_respuesta,
                                          'fecha_respuesta'=>$this->fecha_respuesta=date('Y-m-d'),
                                          'id_usuario'=>$this->id_usuario
                                          ]);
  }

  //metodo para buscar todas las repsuestas a un foro por medio de los estudiantes
  public function getRespuestasForoE()
  {
    return Respuestas_foros::join('users as u', 'RESPUESTAS_FOROS.id_usuario', '=', 'u.id')
                           ->join('ESTUDIANTES as e', 'u.id', '=', 'e.id')
                           ->where('id_foro',$this->id_foro)
                           ->select('e.nombre_estudiante', 'e.apellidos_estudiante', 'RESPUESTAS_FOROS.mensaje_respuesta', 'RESPUESTAS_FOROS.fecha_respuesta')
                           ->get();
  }
  //metodo para buscar todas las respuestas a un foro por medio de los docentes
  public function getRespuestasForoP()
  {
    return Respuestas_foros::join('users as u', 'RESPUESTAS_FOROS.id_usuario', '=', 'u.id')
                           ->join('USUARIO_PERSONA as up', 'u.id', '=', 'up.user_id')
                           ->join('PERSONAS as p', 'up.id_persona', '=', 'p.id_persona')
                           ->where('id_foro',$this->id_foro)
                           ->select('p.nombres_persona', 'p.apellidos_persona', 'RESPUESTAS_FOROS.mensaje_respuesta', 'RESPUESTAS_FOROS.fecha_respuesta')
                           ->get();
  }

}
