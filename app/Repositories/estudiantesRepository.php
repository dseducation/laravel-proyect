<?php



namespace education\Repositories;



use education\Estudiantes;
use education\Tutor;
use education\Referencias;

use DB;

//use DB;

/**

 * Clase repositorio para los estudiantes

 */

class estudiantesRepository

{

  public $id_estudiante;

  public $id_usuario;

  public $id_nivel_grado;

  public $id_municipio;

  public $nombre;

  public $apellido;

  public $codigo_personal;

  public $fecha_nacimiento;

  public $genero;

  public $direccion;

  public $colonia;

  public $zona;

  public $telefono_casa;

  public $empresa_telefonica;

  public $correo;

  public $enfermedad_padecida;

  public $medicamento_recomendado;

  public $alergico;

  public $hospital;

  public $tipo_sangre;

  public $observaciones;

  public $curso;

  public $nombre_tutor;



  /*

      Metodo para obtener todos los estudiantes

  */

  public function getEstudiantes()

  {

    return Estudiantes::select('*')->get();

  }



  /*

      Metodo para buscar a un estudiante por su id

  */

  public function findEstudiante($id)

  {

    return Estudiantes::where('id_estudiante', $id)->first();

  }



  /*

      Metodo para buscar a aun estudiante por sus nombres y apellidos

  */

  public function findEstudianteWhere()

  {

    return Estudiantes::where('nombre_estudiante', $this->nombre)

                      ->where('apellidos_estudiante', $this->apellido)

                      ->get();

  }



  public function setEstudiante()

  {

    return DB::table('ESTUDIANTES')->insertGetId(

                      ['id'=>$this->id_usuario,

                       'id_nivel_grado'=>$this->id_nivel_grado,

                       'id_municipio'=>$this->id_municipio,

                       'nombre_estudiante'=>$this->nombre,

                       'apellidos_estudiante'=>$this->apellido,

                       'codigo_personal_estudiante'=>$this->codigo_personal,

                       'fecha_nacimiento_estudiante'=>$this->fecha_nacimiento,

                       'genero_estudiante'=>$this->genero,

                       'direccion_estudiante'=>$this->direccion,

                       'colonia_estudiante'=>$this->colonia,

                       'zona_estudiante'=>$this->zona,

                       'telefono_casa'=>$this->telefono_casa,

                       'empresa_telefonica'=>$this->empresa_telefonica,

                       'correo_estudiante'=>$this->correo,

                       'enfermedad_padecida_estudiante'=>$this->enfermedad_padecida,

                       'medicamento_recomendado_estudiante'=>$this->medicamento_recomendado,

                       'alergico_estudiante'=>$this->alergico,

                       'hospital_estudiante'=>$this->hospital,

                       'tipo_sangre_estudiante'=>$this->tipo_sangre,

                       'observaciones_estudiante'=>$this->observaciones,

                       'fecha_registro_estudiante'=>date('Y-m-d H:i:s'),

                       'estado_estudiante'=>TRUE], 'id_estudiante');

  }



//metodo para buscar a un estudiante por su id para generar el reporte de inscripción

public function getEstudianteInscripcion()

{

  return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')

                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')

                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')

                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')

                    ->where('ESTUDIANTES.id_estudiante', $this->id_estudiante)

                    ->select('ESTUDIANTES.nombre_estudiante', 'ESTUDIANTES.apellidos_estudiante', 'ESTUDIANTES.fecha_nacimiento_estudiante', 'ESTUDIANTES.direccion_estudiante', 'ESTUDIANTES.colonia_estudiante', 'ESTUDIANTES.zona_estudiante', 'ESTUDIANTES.telefono_casa', 'ESTUDIANTES.correo_estudiante', 'ESTUDIANTES.fecha_registro_estudiante', 'ESTUDIANTES.enfermedad_padecida_estudiante', 'ESTUDIANTES.medicamento_recomendado_estudiante', 'ESTUDIANTES.alergico_estudiante', 'ESTUDIANTES.hospital_estudiante', 'ESTUDIANTES.tipo_sangre_estudiante', 'ESTUDIANTES.observaciones_estudiante', 'ESTUDIANTES.empresa_telefonica', 'm.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel')

                    ->first();

}

/*

-- select de todos los estudiantes inscritos
Select ESTUDIANTES.nombre_estudiante, ESTUDIANTES.apellidos_estudiante, ESTUDIANTES.fecha_nacimiento_estudiante, ESTUDIANTES.direccion_estudiante, ESTUDIANTES.colonia_estudiante, ESTUDIANTES.zona_estudiante, ESTUDIANTES.telefono_casa, ESTUDIANTES.correo_estudiante, ESTUDIANTES.fecha_registro_estudiante, ESTUDIANTES.enfermedad_padecida_estudiante, ESTUDIANTES.medicamento_recomendado_estudiante, ESTUDIANTES.alergico_estudiante, ESTUDIANTES.hospital_estudiante, ESTUDIANTES.tipo_sangre_estudiante, ESTUDIANTES.observaciones_estudiante, ESTUDIANTES.empresa_telefonica, m.nombre_municipio, c.nombre_carrera, g.nombre_grado, n.nombre_nivel
from Estudiantes join NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado

                    join GRADOS as g on ng.id_grado=g.id_grado
                    join CARRERAS as c on ng.id_carrera=c.id_carrera
                    join MUNICIPIO as m on ESTUDIANTES.id_municipio=m.id_municipio
                    join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
                    join NIVELES as n on npj.id_nivel=n.id_nivel
                    */



//metodo para buscar la nomina de estudiantes de acuerdo a un curs




public function getEstudiantesCurso()

{

  return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->where('aa.id_asignacion_area', $this->curso)

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('ESTUDIANTES.id_estudiante', 'ESTUDIANTES.nombre_estudiante', 'ESTUDIANTES.apellidos_estudiante', 'aa.id_asignacion_area')

                    ->get();

}





//metodo para obtener todos los cursos asignados a un estudiante

/*
Select Estudiantes.id_estudiante, Estudiantes.nombre_estudiante, Estudiantes.apellidos_estudiante, aa.id_asignacion_area from Estudiantes join NIVELES_GRADOS as ng on Estudiantes.id_nivel_grado=ng.id_nivel_grado 

                    join ASIGNACION_AREAS as aa on ng.id_nivel_grado=aa.id_nivel_grado

                    where aa.id_asignacion_area=13

                    order by Estudiantes.apellidos_estudiante asc

                    */

public function getCursosEstudiante()

{

    return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')

                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->select('aa.id_asignacion_area', 'a.nombre_area', 'a.id_area')

                    ->get();
}

//funcion para mostrar el ultimo estudiante inscrito cjfn

public function getUltimoEstudianteInscrito()
{

    return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')

                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')

                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')

                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')

                    ->leftjoin('REFERENCIAS as r', 'r.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')

                    ->leftjoin('TUTOR_ESTUDIANTE as te', 'te.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')

                    ->leftjoin('TUTOR as t', 'te.id_tutor', '=', 't.id_tutor')

                    ->join('USUARIO_ESTUDIANTE as UE', 'ESTUDIANTES.id_estudiante', '=', 'UE.id_alumno')

                    ->join('users as u', 'UE.id_usuario', '=', 'u.id')

                   

                    ->orderBy('ESTUDIANTES.id_estudiante', 'DESC')

                    ->select('ESTUDIANTES.id_estudiante as id','ESTUDIANTES.nombre_estudiante as nombre', 'ESTUDIANTES.apellidos_estudiante as apellido', 'ESTUDIANTES.fecha_nacimiento_estudiante as fecha', 'ESTUDIANTES.direccion_estudiante as direccion', 'ESTUDIANTES.colonia_estudiante as colonia', 'ESTUDIANTES.zona_estudiante as zona', 'ESTUDIANTES.telefono_casa as telefono', 'ESTUDIANTES.correo_estudiante', 'ESTUDIANTES.fecha_registro_estudiante', 'ESTUDIANTES.enfermedad_padecida_estudiante', 'ESTUDIANTES.medicamento_recomendado_estudiante', 'ESTUDIANTES.alergico_estudiante', 'ESTUDIANTES.hospital_estudiante', 'ESTUDIANTES.tipo_sangre_estudiante', 'ESTUDIANTES.observaciones_estudiante', 'ESTUDIANTES.empresa_telefonica', 'm.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel','r.nombre_referencia as nombre_ref','r.parentesco_referencia','r.parentesco_referencia','r.telefono_referencia','r.telefono_referencia','t.nombre_tutor','t.apellidos_tutor','t.cui_tutor','t.direccion_tutor','t.telefono_primario_tutor','t.telefono_trabajo_tutor','t.direccion_tutor','t.telefono_trabajo_tutor','t.cargo_tutor','u.email', 'u.password')



                    ->first();
  /*

  --- query que muestra todos los datos del ultimo  estudiante inscrito
 Select ESTUDIANTES.nombre_estudiante, ESTUDIANTES.apellidos_estudiante, ESTUDIANTES.fecha_nacimiento_estudiante, ESTUDIANTES.direccion_estudiante, ESTUDIANTES.colonia_estudiante, ESTUDIANTES.zona_estudiante, ESTUDIANTES.telefono_casa, ESTUDIANTES.correo_estudiante, ESTUDIANTES.fecha_registro_estudiante, ESTUDIANTES.enfermedad_padecida_estudiante, ESTUDIANTES.medicamento_recomendado_estudiante, ESTUDIANTES.alergico_estudiante, ESTUDIANTES.hospital_estudiante, ESTUDIANTES.tipo_sangre_estudiante, ESTUDIANTES.observaciones_estudiante, ESTUDIANTES.empresa_telefonica, m.nombre_municipio, c.nombre_carrera, g.nombre_grado, n.nombre_nivel, r.nombre_referencia, r.parentesco_referencia, r.telefono_referencia, t.nombre_tutor, t.apellidos_tutor, t.cui_tutor, t.direccion_tutor, t.telefono_primario_tutor, t.telefono_trabajo_tutor, t.correo_electronico_tutor, t.lugar_trabajo_tutor, t.direccion_tutor, t.telefono_trabajo_tutor, t.cargo_tutor, u.email, u.password
from ESTUDIANTES 
          left join REFERENCIAS AS r on r.id_estudiante=ESTUDIANTES.id_estudiante
                    left join TUTOR_ESTUDIANTE as te on ESTUDIANTES.id_estudiante=te.id_estudiante
                    left join TUTOR as t on t.id_tutor=te.id_tutor
                    join NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado
                    join GRADOS as g on ng.id_grado=g.id_grado
                    join CARRERAS as c on ng.id_carrera=c.id_carrera
                    join MUNICIPIO as m on ESTUDIANTES.id_municipio=m.id_municipio
                    join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
                    join NIVELES as n on npj.id_nivel=n.id_nivel
          
          join USUARIO_ESTUDIANTE UE on ESTUDIANTES.id_estudiante=UE.id_alumno
          join users u on UE.id_usuario=u.id




          

                    
                    order by  ESTUDIANTES.id_estudiante desc limit 1
                    */


}



}

