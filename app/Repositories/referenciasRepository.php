<?php



namespace education\Repositories;



use education\Referencias;

use DB;

/**

 * Clase repositorio para las referencias de los estudiantes

 */

class referenciasRepository

{

    //propiedades de las referencias

    public $id_referencia;

    public $nombre;

    public $parentesco;

    public $telefono;

    public $id_estudiante;

    //fin de la declaracion de las propiedades de las referencias



    //metodo para registrar para una nueva referencia

    public function setReferencia()

    {

      DB::table('REFERENCIAS')->insert([

                          'nombre_referencia'=>$this->nombre,

                          'parentesco_referencia'=>$this->parentesco,

                          'telefono_referencia'=>$this->telefono,

                          'id_estudiante'=>$this->id_estudiante

                          ]);

    }



    //metodo para buscar una referncia por medio de su nombre completo

    public function findReferenciaWhere()

    {

      return Referencias::where('nombre_referencia', $this->nombre)->get();

    }



    //metodo para buscar las referencias que tiene un estudiante

    public function getReferenciaEstudiante()

    {

      return Referencias::where('id_estudiante', $this->id_estudiante)->get();

    }

    



}

