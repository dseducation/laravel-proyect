<?php

namespace education\Repositories;

use education\actividad;
use DB;
/**
 * Clase repositorio para el manejo de datos de la tabla Foros
 */
class actividadesRepository
{
  public $id_actividad;
  public $id_usuario;
  public $id_asignacion_area;
  public $id_unidad;
  public $id_area;
  public $id_tipo_actividad;
  public $nombre_actividad;
  public $descripcion_actividad;  
  public $fecha_entrega;
  public $nota_total;
  

    //metodo para buscar todos los foros disponibles en la plataforma
  public function getActividad($value='')
  {
    # code...
  }

  //metodo para buscar los foros de acuerdo a un curso
  public function getActividadCurso()
  {
    return DB::table('ACTIVIDADES')
    ->where('id_area',$this->id_area)
    ->get();
  }

  public function getMiActividadCurso()
  {
            return DB::table('ACTIVIDADES as a')
                 ->join('ASIGNACION_AREAS as aa', 'a.id_asignacion_area', '=', 'aa.id_asignacion_area')
                 ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')
                 ->where('a.id_area',  $this->id_area)

                 ->get();

  }



  /*
      Metodo para registrar un nuevo foro en el sistema
  */
  public function setActividad()
  {
    DB::table('ACTIVIDADES')->insert([
                                  'id_usuario'=>$this->id_usuario,
                                  'id_asignacion_area'=>$this->id_asignacion_area,
                                  'id_unidad'=>$this->id_unidad,
                                  'id_area'=>$this->id_area,
                                  'id_tipo_actividad'=>$this->id_tipo_actividad,
                                  'nombre_actividad'=>$this->nombre_actividad,
                                  'descripcion_actividad'=>$this->descripcion_actividad,
                                  'nota_total'=>$this->nota_total,
                                  'fecha_entrega'=>$this->fecha_entrega = date('Y-m-d H:i:s')
                                  
                                ]);
  }

  /*
      Metodo para obtener los datos de un foro por medio de su id
  */
  public function getActividadById()
  {
    return Foros::where('id_actividad', $this->id_actividad)->first();
  }

  //metodo para actualizar los datos de un foro
  public function updateActividad()
  {
    DB::table('FOROS')->where('id_actividad', $this->id_actividad)
                      ->update([

                                  'id_tipo_actividad'=>$this->id_tipo_actividad,
                                  'nombre_actividad'=>$this->nombre_actividad,
                                  'descripcion_actividad'=>$this->descripcion_actividad,
                                  'nota_total'=>$this->nota_total,
                                  'nombre_recurso'=>$this->nombre_recurso,
                                  'url_recursos'=>$this->url_recursos,
                                  'fecha_entrega'=>$this->fecha = date('Y-m-d H:i:s'),
                              ]);
  }
  

}
