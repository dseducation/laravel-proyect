<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Repositories\unidadesRepository;
use education\Repositories\estudiantesRepository;
use education\Repositories\userRepository;
use education\User;
use Auth;

class miscursosController extends Controller
{
  protected $menu;
  protected $unidades;
  protected $estudiantes;
  protected $usuarios;
  public function __construct(menuController $m, unidadesRepository $unidad, estudiantesRepository $estudiante, userRepository $usuario)
  {
    $this->menu = $m->index();
    $this->unidades = $unidad;
    $this->estudiantes = $estudiante;
    $this->usuarios=$usuario;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad
        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad
        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha
        
        $this->estudiantes->id_usuario = Auth::user()->id;
        
        
        $cursos = $this->estudiantes->getCursosEstudiante();


        return view('estudiantes.index', ['items'=>$this->menu, 'unidad'=>$unidad,  'cursos'=>$cursos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
