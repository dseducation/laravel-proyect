<?php

namespace education\Repositories;

use education\Tutor;
use education\Tutor_estudiante;
use DB;
/**
 * Clase repositorio para la tabla tutor
 */
class tutorestudianteRepository
{

  public $id_usuario;
  public $nombre;
  public $apellido;
  public $direccion;
  public $telefono_primario;
  public $empresa_telefono;
  public $correo_electronico;
  public $cui;
  public $lugar_trabajo;
  public $direccion_trabajo;
  public $telefono_trabajo;
  public $empresa_telefono_trabajo;
  public $cargo;
  public $parentesco;
  public $estado;
  public $id_estudiante;

  /**
  * Metodo para poder registrar a un nuevo tutor en el sistema
  */
  public function setTutor()
  {
    return DB::table('TUTOR')->insertGetId([
                                            'nombre_tutor'=>$this->nombre,
                                            'apellidos_tutor'=>$this->apellido,
                                            'direccion_tutor'=>$this->direccion,
                                            'telefono_primario_tutor'=>$this->telefono_primario,
                                            'empresa_telefono_tutor'=>$this->empresa_telefono,
                                            'correo_electronico_tutor'=>$this->correo_electronico,
                                            'cui_tutor'=>$this->cui,
                                            'lugar_trabajo_tutor'=>$this->lugar_trabajo,
                                            'direccion_trabajo_tutor'=>$this->direccion_trabajo,
                                            'telefono_trabajo_tutor'=>$this->telefono_trabajo,
                                            'empresa_telefono_trabajo'=>$this->empresa_telefono_trabajo,
                                            'cargo_tutor'=>$this->cargo,
                                            'parentesco_tutor'=>$this->parentesco,
                                            'estado_tutor'=>$this->estado = true,
                                            'id'=>$this->id_usuario
                                            ],
                                            'id_tutor');
  }

  /**
  * Metodo para buscar a un tutor por su dpi
  */
  public function findTutorByDPI()
  {
    return Tutor::where('cui_tutor', $this->cui)->first();
  }

  /**
  * Metodo para editar los datos de un tutor
  */
  public function updateTutor()
  {

  }

  /**
  * Metodo para asignar un tutor a un estudiante
  */
  public function setTutorEstudiante($tutor, $estudiante)
  {
    DB::table('TUTOR_ESTUDIANTE')->insert([
                              'id_estudiante'=>$estudiante,
                              'id_tutor'=>$tutor
                            ]);
  }

  //metodo para buscar los datos de los tutores asignados a un estudiante
  public function getTutoresEstudiante()
  {
    return Tutor_estudiante::join('TUTOR as t', 'TUTOR_ESTUDIANTE.id_tutor', '=', 't.id_tutor')
                           ->where('TUTOR_ESTUDIANTE.id_estudiante', $this->id_estudiante)
                           ->get();
  }
  /*
  select* from TUTOR_ESTUDIANTE te inner join TUTOR t
on te.id_tutor = t.id_tutor
where te.id_estudiante = 6
  */

}
