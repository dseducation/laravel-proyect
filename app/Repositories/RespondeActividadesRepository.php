<?php

namespace education\Repositories;



use education\RespondeActividad;

use education\actividad;





use DB;

/**

 * Clase repositorio para tratar los datos de la tabla respuetas de tareas

 */

class RespondeActividadesRepository

{

  

  public $id_respuesta_actividad;

  public $id_actividad;

  public $id_usuario;

  public $estado_entrega_actividad;

  public $calificacion;

  public $fecha_entrega;

  public $comentarios;

  public $ruta_actividad;

  public $area;

  public $unidad;

  public $valor_max;



  /*

      Metodo para registrar una nueva unidad

  */

  public function setRespondeActividad($val_max)

  {

    DB::table('RESPUESTA_ACTIVIDADES')->insert([

                                 

                                   'id_actividad'=>$this->id_actividad,

                                  'id_usuario'=>$this->id_usuario,

                                  'comentarios'=>$this->comentarios,

                                  'ruta_actividad'=>$this->ruta_actividad,

                                  'unidad'=>$this->unidad,

                                  'area'=>$this->area,

                                  'valor_max'=>$val_max,




                                    ]);

  }



   



  /*

      Metodo para ver si exite una respuesta de actividad con el nombre especificado

  */

  public function existRespuestaActividades($id_actividad,$id_usuario,$unidades,$areas)

  {


    return RespondeActividad::where('id_actividad',$id_actividad)
              ->where('id_usuario',$id_usuario)
              ->where('unidad', $unidades)
              ->where('area',$areas)
              ->first();

      

              //SELECT count(*) FROM `respuesta_actividades` WHERE id_actividad=1 and id_usuario=17 and unidad=5 and area=3

  }

  public function valorMaximoNota()
  {
/*
      return DB::table('actividades')

              ->where('id_actividad',$id)
              ->select('nota_total')


              ->get();*/

              return actividad::where('id_actividad', $this->id_actividad)->select('nota_total')->get();

    //SELECT * FROM `actividades` where id_actividad=3
  }

  public function valorMaximoNota2($id_actividad)
  {
/*
      return DB::table('actividades')

              ->where('id_actividad',$id)
              ->select('nota_total')


              ->get();*/

              return actividad::where('id_actividad', $id_actividad)->select('nota_total')->first();;

    //SELECT * FROM `actividades` where id_actividad=3
  }


  /*

      Metodo para buscar todas las respuesta de actividad  regitradas

  */

  public function getRespuestaActividades()

  {

    return RESPUESTA_ACTIVIDADES::All();

  }



  public function getUsuariosRespuestActividades()

  {

    return RespondeActividad::join('users as u', 'u.id', '=', 'RESPUESTA_ACTIVIDADES.id_usuario')

             ->where('RESPUESTA_ACTIVIDADES.id_actividad', $this->id_actividad)

             ->select('RESPUESTA_ACTIVIDADES.id_respuesta_actividad', 'RESPUESTA_ACTIVIDADES.estado_entrega_actividad','RESPUESTA_ACTIVIDADES.calificacion','RESPUESTA_ACTIVIDADES.fecha_entrega','RESPUESTA_ACTIVIDADES.comentarios','u.name')

             ->get();

  }



  /*

      Metodo para obtener los datos de una respuesta de actividad por su id de actividad

  */

  public function getRespuestaActividadesById()

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $this->id_actividad)

    ->get();

  }

   public function getNotaMaxRespuestaActividadesById($id_act)

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $id_act)

    ->first();

  }




  /*



 public function getRespuestaActividadesById()

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $this->id_actividad)

    ->get();

  }

*/



  //funcion para traer todas las respuetas para modificar

   public function getRespuestaActividadesById1()

  {

   return RespondeActividad::where('id_respuesta_actividad', $this->id_respuesta_actividad)->first();

   }

  /*

      Metodo para actualizar los datos de una unidad

  */

  public function updateRespuestaActividades()

  {

   DB::table('RESPUESTA_ACTIVIDADES')->where('id_respuesta_actividad', $this->id_respuesta_actividad)

            ->update([

                   'estado_entrega_actividad'=>$this->estado_entrega_actividad,

                   //'fecha_calificacion'=>$this->fecha_calificacion,

                  'calificacion'=>$this->calificacion,

                  'observaciones_maestro'=>$this->observaciones_maestro,

            ]);

  }

  public function getIdActividad()
  {

 
             
             return actividad::where('id_actividad', '=', $this->id_actividad)

             

                   ->first();




  }



  /*

  update respuesta_actividades set estado_entrega_actividad="CALIFICADO", fecha_calificacion="20-3-2016", calificacion=10, observaciones_maestro="EXCELENTE TRABAJO" where id_respuesta_actividad=1

  */



 

  

}

