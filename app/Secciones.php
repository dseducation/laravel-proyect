<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Secciones extends Model
{
    protected $table = 'SECCIONES';

    /*
      Metodo para agrega un nuevo registro a la base de datos
    */
    public static function setSeccion($nombre)
    {
      return DB::select('CALL nueva_seccion(?)', array($nombre));
    }
    /*
      Metodo para buscar los datos de una seccion por su id
    */
    public static function findSeccion($id)
    {
      return Secciones::where('id_seccion', $id)->first();
    }

    /*
      Metodo par actualizar los datos de una seccion
    */
    public static function updateSeccion($id, $nombre)
    {
      return DB::select('CALL actualizar_seccion(?, ?)', array($id, $nombre));
    }
}
