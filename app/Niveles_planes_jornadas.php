<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Niveles_planes_jornadas extends Model
{
    protected $table = 'NIVELES_PLANES_JORNADAS';

    /*
      Metodo para obtener todos los registros de la tabla
    */
    public static function getPlanesNiveles()
    {
      return NIVELES_PLANES_JORNADAS::join('NIVELES', 'NIVELES_PLANES_JORNADAS.id_nivel', '=', 'NIVELES.id_nivel')
                                    ->join('PLANES', 'NIVELES_PLANES_JORNADAS.id_plan', '=', 'PLANES.id_plan')
                                    ->join('JORNADAS', 'NIVELES_PLANES_JORNADAS.id_jornada', '=', 'JORNADAS.id_jornada')
                                    ->select('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada', 'NIVELES.nombre_nivel', 'PLANES.nombre_plan', 'JORNADAS.nombre_jornada')
                                    ->get();
    }

    /*
      Metodo para agregar un nuevo registro a la tabla
    */
    public static function setPlanNivel($nivel, $plan, $jornada)
    {
      return DB::select('CALL nuevo_plan_nivel(?, ?, ?)', array($nivel, $plan, $jornada));
    }

    /*
      Metodo para buscar un registro por su id
    */
    public static function findPlanNivel($id)
    {
      return NIVELES_PLANES_JORNADAS::where('id_nivel_plan_jornada', $id)->first();
    }

    /*
      Metodo para actualizar los datos de un registro
    */
    public static function updatePlanNivel($id, $nivel, $plan, $jornada)
    {
      return DB::select('CALL actualizar_plan_nivel(?, ?, ?, ?)', array($id, $nivel, $plan, $jornada));
    }

    /*
      Metodo para buscar el nombre de un nivel que se encuantra en la tabla NIVELES_PLANES_JORNADAS por medio de
      la jornada y plan indicado como paramtro
    */
    public static function getNivelWhere($plan, $jornada)
    {
      return NIVELES_PLANES_JORNADAS::join('NIVELES as n', 'NIVELES_PLANES_JORNADAS.id_nivel', '=', 'n.id_nivel')
                                    ->where([['NIVELES_PLANES_JORNADAS.id_plan',$plan], ['NIVELES_PLANES_JORNADAS.id_jornada', $jornada]])
                                    ->select('n.nombre_nivel', 'NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada')
                                    ->get();
    }

    /*
      Metodo para buscar el id_nivel_plan_jornada deacuerdo a los parametros enviados
    */
    public static function getIdNivelPlanJornada($plan, $jornada, $nivel)
    {
      return NIVELES_PLANES_JORNADAS::join('NIVELES as n', 'NIVELES_PLANES_JORNADAS.id_nivel', '=', 'n.id_nivel')
                                    ->join('PLANES as p', 'NIVELES_PLANES_JORNADAS.id_plan', '=', 'p.id_plan')
                                    ->join('JORNADAS as j', 'NIVELES_PLANES_JORNADAS.id_jornada', '=', 'j.id_jornada')
                                    ->where('NIVELES_PLANES_JORNADAS.id_plan', $plan)
                                    ->where('NIVELES_PLANES_JORNADAS.id_nivel', $nivel)
                                    ->where('NIVELES_PLANES_JORNADAS.id_jornada', $jornada)
                                    ->select('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada')
                                    ->first();
    }
    /*
    select npj.id_nivel_plan_jornada
from NIVELES_PLANES_JORNADAS npj inner join NIVELES n
on npj.id_nivel = n.id_nivel inner join PLANES p
on npj.id_plan = p.id_plan inner join JORNADAS j
on npj.id_jornada = j.id_jornada
where npj.id_plan = 1 and npj.id_nivel = 3 and npj.id_jornada = 1
    */

}
