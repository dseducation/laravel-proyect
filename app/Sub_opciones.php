<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Sub_opciones extends Model
{
    protected $table = 'SUB_OPCIONES';

    /*
      Metodo para buscar las sub opciones diponibles de acuerdo al id de opcion
    */
    public static function findSubOpciones($id)
    {
      return SUB_OPCIONES::where([['estado_sub_opcion', TRUE], ['id_opcion', $id]])
                          ->select('nombre_sub_opcion', 'id_sub_opcion')
                          ->get();
    }
}
