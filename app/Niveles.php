<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Niveles extends Model
{
    protected $table =  'NIVELES';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setNivel($nombre)
    {
      return DB::select('CALL nuevo_nivel(?, ?)', array($nombre, date('Y-m-d')));
    }

    /*
      Metodo para buscar los datos de un nivel por su id
    */
    public static function findNivel($id)
    {
      return Niveles::where('id_nivel', $id)->first();
    }

    /*
      Metodo para actulizar los datos de un nivel
    */
    public static function updateNivel($id, $nombre)
    {
      return DB::select('CALL actualizar_nivel(?, ?)', array($id, $nombre));
    }

    /*
      Metodo para cambiar de estado a un nivel
    */
    public static function stateNivel($id, $estado)
    {
      return DB::select('CALL estado_nivel(?, ?)', array($id, $estado));
    }

    /*
      Metodo para devolver un array de los niveles
    */
    public static function getNiveles()
    {
      $niveles = array();
      foreach (NIVELES::where('estado_nivel', TRUE)->get() as $key => $n) {
        $niveles[$n->id_nivel] = mb_strtoupper($n->nombre_nivel);
      }
      return $niveles;
    }

}
