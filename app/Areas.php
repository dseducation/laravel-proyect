<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Areas extends Model
{
    protected $table = 'AREAS';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setArea($nombre)
    {
      return DB::select('CALL nueva_area(?, ?)', array($nombre, date('Y-m-d')));
    }

    /*
      Metodo para buscar los datos de un área por su id
    */
    public static function findArea($id)
    {
      return Areas::where('id_area', $id)->first();
    }

    /*
      Metodo para actualizar el nombre de un area
    */
    public static function updateArea($id, $nombre)
    {
      return DB::select('CALL actualizar_area(?, ?)', array($id, $nombre));
    }

    /*
      Metodo para cambiar el estado del area
    */
    public static function stateArea($id, $estado)
    {
      return DB::select('CALL estado_area(?, ?)', array($id, $estado));
    }

    /*
      Metodo para buscar las areas donde coincida el parametro
    */
    public static function getAreasLike($c)
    {
      return Areas::where('nombre_area', 'like', '%'.$c.'%')
                  ->select('nombre_area', 'id_area')
                  ->get();
    }
}
