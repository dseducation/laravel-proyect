<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Roles extends Model
{
    protected $table = 'ROLES';

    /*
      Metodo para agregar un nuevo registro a la tabla roles
    */
    public static function setRol($nombre)
    {
      return DB::select('CALL nuevo_rol(?)', array($nombre));
    }

    /*
      Metodo para buscar los datos de un rol por su id
    */
    public static function findRol($id)
    {
      return ROLES::where('id_rol', $id)->first();
    }

    /*
      Metodo para cambiar el estado de un rol por medio de su id
    */
    public static function stateRol($id, $estado)
    {
      return DB::select('CALL estado_rol(?, ?)', array($id, $estado));
    }

    
}
