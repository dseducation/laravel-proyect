<?php

namespace education;

//use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
  protected $table = 'permissions';
  protected $fillable = [
      'name',
      'display_name',
      'description'
  ];

  /*
    Metodo para buscar los permisos disponibles de una opción
  */
  public static function findPermisosOpcion($opcion)
  {
    return Permission::where('id_opcion', $opcion)
                        ->select('display_name', 'id', 'description')
                        ->get();
  }
 //establecemos las relacion de muchos a muchos con el modelo Role, ya que un permiso
 //lo pueden tener varios roles y un rol puede tener varios permisos
 public function roles(){
      return $this->belongsToMany('education\Role');
  }

}
