<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'DEPARTAMENTO';

    public static function getDepartamentos()
    {
      $deptos = array();
      foreach (DEPARTAMENTO::All() as $key => $row) {
        $deptos[$row->id_departamento] = mb_strtoupper($row->nombre_departamento);
      }
      return $deptos;
    }
}
