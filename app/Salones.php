<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Salones extends Model
{
    protected $table = 'SALONES';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setSalon($nombre)
    {
      return DB::select('CALL nuevo_salon(?)', array($nombre));
    }

    /*
      Metodo para buscar los datos de un salo por su id
    */
    public static function findSalon($id)
    {
      return Salones::where('id_salon', $id)->first();
    }

    /*
      Metodo para actualizar los datos de un salon
    */
    public static function updateSalon($id, $nombre)
    {
      return DB::select('CALL actualizar_salon(?, ?)', array($id, $nombre));
    }

    /*
      Metodo para cambiar el estado de un salón
    */
    public static function stateSalon($id, $estado)
    {
      return DB::select('CALL estado_salon(?, ?)', array($id, $estado));
    }
}
