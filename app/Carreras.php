<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Carreras extends Model
{
    protected $table = 'CARRERAS';

    /*
      metodo para agregar un nuevo registro a la base de datos
    */
    public static function setCarrera($nombre, $nombreCorto, $descripcion)
    {
      return DB::select('CALL nueva_carrera(?, ?, ?, ?)', array($nombre, $nombreCorto, $descripcion, date('Y-m-d H:i:s')));
    }

    /*
      Metodo para buscar los datos de una carrera por medio de si id
    */
    public static function findCarrera($id)
    {
      return Carreras::where('id_carrera', $id)->first();
    }

    /*
      Metodo para actualizar los datos de una carrera
    */
    public static function updateCarrera($id, $nombre, $nombreCorto, $descripcion)
    {
      return DB::select('CALL actualizar_carrera(?, ?, ?, ?)', array($id, $nombre, $nombreCorto, $descripcion));
    }

    /*
      Metodo para cambiar el estado de una carrera
    */
    public static function stateCarrera($id, $estado)
    {
      return DB::select('CALL estado_carrera(?, ?)', array($id, $estado));
    }
}
