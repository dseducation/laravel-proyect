<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Personas extends Model
{
    protected $table = 'PERSONAS';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setPersona($nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo)
    {
      return DB::select('CALL nueva_persona(?, ?, ?, ?, ?, ?, ?, ?, ?)', array($nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo));
    }

    /*
      Metodo para obtener los datos de una persona por su id
    */
    public static function findPersona($id)
    {
      return PERSONAS::join('puestos AS p', 'personas.id_puesto', '=', 'p.id_puesto')
      ->where('id_persona', $id)->first();
    }

    /*
      Metodo para actualizar los datos de una persona
    */
    public static function updatePersona($id, $nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo)
    {
      return DB::select('CALL actualizar_persona(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', array($id, $nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo));
    }

    /*
      Metodo para camibar el estado de un registro de la tabla PERSONAS
    */
    public static function statePersona($id, $estado)
    {
      return DB::select('CALL estado_persona(?, ?)', array($id, $estado));
    }

    /*
      Metodo para buscar a las personas que tengan una coincidencia en el nombre o apellido
    */
    public static function getPersonasLike($coincidencia)
    {
      return PERSONAS::join('puestos AS p', 'personas.id_puesto', '=', 'p.id_puesto')
                     ->where('apellidos_persona', 'like', '%'.$coincidencia.'%')
                     ->orWhere('nombres_persona', 'like', '%'.$coincidencia.'%')
                     ->orWhere('correo_persona', 'like', '%'.$coincidencia.'%')
                     ->orWhere('nombre_puesto', 'like', '%'.$coincidencia.'%')
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona','p.nombre_puesto as puesto')
                     ->get();

                     /*return permission_role::join('permissions AS so', 'permission_role.permission_id', '=', 'so.id')
                         ->where('role_id', $rol)
                         ->select('so.name', 'permission_role.id_permission_role', 'permission_role.state_permission_role')
                         ->get();*/
    }
 

//funcion que Buscar persona por nombre, apellido, telefono, correo o puesto cj
   



    /*Metodo para buscar a las personas que coincidan con el paramtreo de busqueda y que tenga el puesto de docente*/
    public static function getPersonasLikeDocente($q)
    {
      /*return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     //->orWhere('nombre_persona', 'like', '%'.$q.'%')
                     //->where([['p.nombre_puesto', '=', 'docente'], ['apellidos_persona', 'like', '%'.$q.'%'], ['nombre_persona', 'like', '%'.$q.'%']])
                    //->where([['apellidos_persona', 'like', '%'.$q.'%'], 'OR',['nombres_persona', 'like', '%'.$q.'%']])
                     //->orWhere('nombres_persona', 'like', '%'.$q.'%')
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();*/
      return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     ->where(function ($query) use ($q) {
                         $query->where('apellidos_persona', 'like', '%'.$q.'%')
                               ->orWhere('nombres_persona', 'like', '%'.$q.'%');
                     })
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();
    }

    /*Model::where(function ($query) {
    $query->where('a', '=', 1)
          ->orWhere('b', '=', 1);
})->where(function ($query) {
    $query->where('c', '=', 1)
          ->orWhere('d', '=', 1);
});*/
}
