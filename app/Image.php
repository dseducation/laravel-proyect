<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'image';
}
