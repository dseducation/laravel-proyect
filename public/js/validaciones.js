$(document).ready(function() {

$('#nrol').formValidation({
    message: 'This value is not valid',
    icon: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      name: {
          message: 'El nombre del rol no es valido!!!',
          validators: {
              notEmpty: {
                  message: 'El nombre del rol es requerido, no puede estar vacío!!!'
              },
              stringLength: {
                  min: 6,
                  max: 30,
                  message: 'El nombre del rol debe tener por lo menos 6 y menos 30 caracteres de longitud!!!'
              },
              regexp: {
                  regexp: /^[a-zA-Z0-9_\.]+$/,
                  message: 'El nombre del rol tiene que contener letras del alfabeto, numeros, sin espacios y ningún otro caracter!!!!'
              }
          }
      },
      description: {
          message: 'El nombre del rol no es valido!!!',
          validators: {
              notEmpty: {
                  message: 'La descripción del rol es requerido, no puede estar vacío!!!'
              },
              stringLength: {
                  min: 6,
                  max: 100,
                  message: 'El nombre del rol debe tener por lo menos 6 y menos 1000 caracteres de longitud!!!'
              },
              regexp: {
                  regexp: /^[a-zA-Z0-9\.\s]+$/,
                  message: 'El nombre del rol tiene que contener letras del alfabeto, numeros, sin espacios y ningún otro caracter!!!!'
              }
          }
      },
    }
  });//fin de la validación del formulario

  $('#nnivel').formValidation({
      message: 'This value is not valid',
      icon: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        nombre_nivel: {
            message: 'El nombre del nivel no es valido!!!',
            validators: {
                notEmpty: {
                    message: 'El nombre del nivel es requerido, no puede estar vacío!!!'
                },
                stringLength: {
                    min: 4,
                    max: 30,
                    message: 'El nombre del nivel debe tener por lo menos 4 y menos 30 caracteres de longitud!!!'
                },
                regexp: {
                    regexp: /^[a-zA-Z\-áéíóúÁÉÍÓÚ\s\.]+$/,
                    message: 'El nombre del nivel tiene que contener unicamente letras del alfabeto y espacios, ningún otro caracter!!!!'
                }
            }
        },
  }
    });//fin de la validación del formulario

    $('#nplan').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nombre_plan: {
              message: 'El nombre del plan no es valido!!!',
              validators: {
                  notEmpty: {
                      message: 'El nombre del plan es requerido, no puede estar vacío!!!'
                  },
                  stringLength: {
                      min: 4,
                      max: 25,
                      message: 'El nombre del plan debe tener por lo menos 4 y menos 25 caracteres de longitud!!!'
                  },
                  regexp: {
                      regexp: /^[a-zA-Z\s\.]+$/,
                      message: 'El nombre del plan tiene que contener unicamente letras del alfabeto, sin ningún otro caracter!!!!'
                  }
              }
          }
        }
      });//fin de la validación del formulario

      $('#njornada').formValidation({
          message: 'This value is not valid',
          icon: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          fields: {
            nombre_jornada: {
                message: 'El nombre de la jornada no es valido!!!',
                validators: {
                    notEmpty: {
                        message: 'El nombre de la jornada es requerido, no puede estar vacío!!!'
                    },
                    stringLength: {
                        min: 4,
                        max: 25,
                        message: 'El nombre de la jornada debe tener por lo menos 4 y menos 25 caracteres de longitud!!!'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z\s\.]+$/,
                        message: 'El nombre del plan tiene que contener unicamente letras del alfabeto, sin ningún otro caracter!!!!'
                    }
                }
            }
          }
        });//fin de la validación del formulario

        $('#npuesto').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              nombre_puesto: {
                  message: 'El nombre del puesto no es valido!!!',
                  validators: {
                      notEmpty: {
                          message: 'El nombre del puesto es requerido, no puede estar vacío!!!'
                      },
                      stringLength: {
                          min: 4,
                          max: 30,
                          message: 'El nombre del puesto debe tener por lo menos 4 y menos 30 caracteres de longitud!!!'
                      },
                      regexp: {
                          regexp: /^[a-zA-Z\s\.]+$/,
                          message: 'El nombre del puesto tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro caracter!!!!'
                      }
                  }
              }
            }
          });//fin de la validación del formulario

          $('#nseccion').formValidation({
              message: 'This value is not valid',
              icon: {
                  valid: 'glyphicon glyphicon-ok',
                  invalid: 'glyphicon glyphicon-remove',
                  validating: 'glyphicon glyphicon-refresh'
              },
              fields: {
                nombre_seccion: {
                    message: 'El nombre de la sección no es valido!!!',
                    validators: {
                        notEmpty: {
                            message: 'El nombre de la sección es requerido, no puede estar vacío!!!'
                        },
                        stringLength: {
                            min: 1,
                            max: 10,
                            message: 'El nombre de la sección debe tener por lo menos 1 y menos 10 caracteres de longitud!!!'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z\s\.]+$/,
                            message: 'El nombre de la sección tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro caracter!!!!'
                        }
                    }
                }
              }
            });//fin de la validación del formulario

            $('#ngrado').formValidation({
                message: 'This value is not valid',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                  nombre_grado: {
                      message: 'El nombre del grado no es valido!!!',
                      validators: {
                          notEmpty: {
                              message: 'El nombre del grado es requerido, no puede estar vacío!!!'
                          },
                          stringLength: {
                              min: 4,
                              max: 40,
                              message: 'El nombre del grado debe tener por lo menos 4 y menos 40 caracteres de longitud!!!'
                          },
                          regexp: {
                              regexp: /^[a-zA-Z\-áéíóúÁÉÍÓÚ\s\.]+$/,
                              message: 'El nombre del grado tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro caracter!!!!'
                          }
                      }
                  },
                }
              });//fin de la validación del formulario

              $('#nasignarnivel').formValidation({
                  message: 'This value is not valid',
                  icon: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                    id_nivel: {
                        message: 'El nombre del nivel no es valido!!!',
                        validators: {
                            notEmpty: {
                                message: 'El nombre del nivel es requerido, no puede estar vacío!!!'
                            },
                        }
                    },
                    id_plan: {
                        message: 'El nombre del plan no es valido!!!',
                        validators: {
                            notEmpty: {
                                message: 'El nombre del plan es requerido, no puede estar vacío!!!'
                            },
                        }
                    },
                    id_jornada: {
                        message: 'El nombre de la jornada no es valido!!!',
                        validators: {
                            notEmpty: {
                                message: 'El nombre de la jornada es requerido, no puede estar vacío!!!'
                            },
                        }
                    },
                  }
                });//fin de la validación del formulario

                $('#nsalon').formValidation({
                    message: 'This value is not valid',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                      nombre_salon: {
                          message: 'El nombre del salón no es valido!!!',
                          validators: {
                              notEmpty: {
                                  message: 'El nombre del salón es requerido, no puede estar vacío!!!'
                              },
                              stringLength: {
                                  min: 2,
                                  max: 20,
                                  message: 'El nombre del salón debe tener por lo menos 2 y menos 20 caracteres de longitud!!!'
                              },
                              regexp: {
                                  regexp: /^[a-zA-Záéíóú0-9\s\-\.]+$/,
                                  message: 'El nombre del salón tiene que contener unicamente letras del alfabeto, espacios o números, sin ningún otro caracter!!!!'
                              }
                          }
                      },
                    }
                  });//fin de la validación del formulario

                  $('#narea').formValidation({
                      message: 'This value is not valid',
                      icon: {
                          valid: 'glyphicon glyphicon-ok',
                          invalid: 'glyphicon glyphicon-remove',
                          validating: 'glyphicon glyphicon-refresh'
                      },
                      fields: {
                        nombre_area: {
                            message: 'El nombre del curso no es valido!!!',
                            validators: {
                                notEmpty: {
                                    message: 'El nombre del curso es requerido, no puede estar vacío!!!'
                                },
                                stringLength: {
                                    min: 2,
                                    max: 60,
                                    message: 'El nombre del curso debe tener por lo menos 2 y menos 60 caracteres de longitud!!!'
                                },
                                regexp: {
                                    regexp: /^[a-zA-Záéíóúñ0-9\s\-\/\.]+$/,
                                    message: 'El nombre del curso tiene que contener unicamente letras del alfabeto, espacios, números o guiones, sin ningún otro caracter!!!!'
                                }
                            }
                        },
                      }
                    });//fin de la validación del formulario

                    $('#npersona').formValidation({
                        message: 'This value is not valid',
                        icon: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        fields: {
                          nombre_persona: {
                              message: 'El nombre del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'El nombre del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 2,
                                      max: 50,
                                      message: 'El nombre del empleado debe tener por lo menos 2 y menos 50 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[a-zA-Záéíóúñ\s\.]+$/,
                                      message: 'El nombre del empleado tiene que contener unicamente letras del alfabeto o espacios, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          apellidos_persona: {
                              message: 'El apellido del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'El apellido del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 2,
                                      max: 50,
                                      message: 'El apellido del empleado debe tener por lo menos 2 y menos 50 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[a-zA-Záéíóúñ\s\.]+$/,
                                      message: 'El apellido del empleado tiene que contener unicamente letras del alfabeto o espacios, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          cui_persona: {
                              message: 'El dpi del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'El dpi del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 13,
                                      max: 13,
                                      message: 'El dpi del empleado debe tener 13 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[0-9]{13}$/,
                                      message: 'El dpi del empleado tiene que contener unicamente digitos, sin ningún otro caracter o espacio!!!'
                                  }
                              }
                          },
                          direccion_persona: {
                              message: 'La dirección del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'La dirección del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 2,
                                      max: 50,
                                      message: 'La dirección del empleado debe tener por lo menos 2 y menos 60 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[a-zA-Záéíóúñ0-9\-\.\s]+$/,
                                      message: 'La dirección del empleado tiene que contener unicamente letras del alfabeto, números, espacios o guiones, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          correo_persona: {
                              message: 'La dirección de correo del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'La dirección de correo del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 2,
                                      max: 50,
                                      message: 'La dirección del correo empleado debe tener por lo menos 2 y menos 50 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/,
                                      message: 'La dirección de correo del empleado tiene que tener un formato valido!!!!'
                                  }
                              }
                          },
                          fecha_nacimiento_persona: {
                              message: 'La fecha de nacimiento del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'La fecha de nacimiento del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 10,
                                      max: 10,
                                      message: 'La fecha de nacimiento del empleado debe tener exactamente 10 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/,
                                      message: 'La fecha de nacimiento del empleado tiene que contener unicamente digitos y diagonales, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          telefono_persona: {
                              message: 'El número de telefono del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'El número de telefono del empleado es requerido, no puede estar vacío!!!'
                                  },
                                  stringLength: {
                                      min: 8,
                                      max: 10,
                                      message: 'El número de telefono del empleado debe tener exactamente 9 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[0-9]{4}\-*\s*[0-9]{4}$/,
                                      message: 'El número de telefono del empleado tiene que contener unicamente digitos, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          telefono_auxiliar_persona: {
                              message: 'El número de telefono del empleado no es valido!!!',
                              validators: {
                                  stringLength: {
                                      min: 8,
                                      max: 9,
                                      message: 'El número de telefono del empleado debe tener exactamente 9 caracteres de longitud!!!'
                                  },
                                  regexp: {
                                      regexp: /^[0-9]{4}\-*\s*[0-9]{4}$/,
                                      message: 'El número de telefono del empleado tiene que contener unicamente digitos, sin ningún otro caracter!!!!'
                                  }
                              }
                          },
                          id_puesto: {
                              message: 'El puesto del empleado no es valido!!!',
                              validators: {
                                  notEmpty: {
                                      message: 'El puesto del empleado es requerido, no puede estar vacío!!!'
                                  },
                              }
                          },
                        }
                      });//fin de la validación del formulario

                      $('#ncarrera').formValidation({
                          message: 'This value is not valid',
                          icon: {
                              valid: 'glyphicon glyphicon-ok',
                              invalid: 'glyphicon glyphicon-remove',
                              validating: 'glyphicon glyphicon-refresh'
                          },
                          fields: {
                            nombre_carrera: {
                                message: 'El nombre de la carrera no es valido!!!',
                                validators: {
                                    notEmpty: {
                                        message: 'El nombre de la carrera es requerido, no puede estar vacío!!!'
                                    },
                                    stringLength: {
                                        min: 2,
                                        max: 70,
                                        message: 'El nombre de la carrera debe tener por lo menos 2 y menos 70 caracteres de longitud!!!'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚ\s]+$/,
                                        message: 'El nombre de la carrera tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro caracter!!!!'
                                    }
                                }
                            },
                            nombre_corto_carrera: {
                                message: 'El nombre corto de la carrera no es valido!!!',
                                validators: {
                                    notEmpty: {
                                        message: 'El nombre corto de la carrera es requerido, no puede estar vacío!!!'
                                    },
                                    stringLength: {
                                        min: 2,
                                        max: 15,
                                        message: 'El nombre corto de la carrera debe tener por lo menos 2 y menos 15 caracteres de longitud!!!'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-ZáéíóúñÁÉÍÓÚ]+$/,
                                        message: 'El nombre de la carrera tiene que contener unicamente letras del alfabeto, sin ningún otro tipo de caracter!!!!'
                                    }
                                }
                            },
                            descripcion_carrera: {
                                message: 'La descripción de la carrera no es valido!!!',
                                validators: {
                                    notEmpty: {
                                        message: 'La descripción de la carrera es requerido, no puede estar vacío!!!'
                                    },
                                    stringLength: {
                                        min: 2,
                                        max: 500,
                                        message: 'La descripción de la carrera debe tener por lo menos 2 y menos 500 caracteres de longitud!!!'
                                    },
                                    regexp: {
                                        regexp: /^[a-z0-9A-ZáéíóúñÁÉÍÓÚ\s\,\.]+$/,
                                        message: 'El nombre de la carrera tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro tipo de caracter!!!!'
                                    }
                                }
                            },
                          }
                        });//fin de la validación del formulario

                        $('#ngradonivel').formValidation({
                            message: 'This value is not valid',
                            icon: {
                                valid: 'glyphicon glyphicon-ok',
                                invalid: 'glyphicon glyphicon-remove',
                                validating: 'glyphicon glyphicon-refresh'
                            },
                            fields: {
                              id_nivel: {
                                  message: 'El nombre del nivel no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El nombre del nivel es requerido, no puede estar vacío!!!'
                                      }
                                  }
                              },
                              id_carrera: {
                                  message: 'El nombre de la carrera no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El nombre de la carrera es requerido, no puede estar vacío!!!'
                                      }
                                  }
                              },
                              id_grado: {
                                  message: 'El nombre del grado no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El nombre del grado es requerido, no puede estar vacío!!!'
                                      }
                                  }
                              },
                              id_seccion: {
                                  message: 'El nombre de la sección no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El nombre de la sección es requerido, no puede estar vacío!!!'
                                      }
                                  }
                              },
                              cuota_inscripcion: {
                                  message: 'El valor de la inscripción no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El valor de la inscripción es requerido, no puede estar vacío!!!'
                                      },
                                      regexp: {
                                          regexp: /^[0-9]+(\.{1}[0-9]{2})$/,
                                          message: 'El nombre de la carrera tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro tipo de caracter!!!!'
                                      },
                                  }
                              },
                              cuota_mensualidad: {
                                  message: 'El valor de la mensualidad no es valido!!!',
                                  validators: {
                                      notEmpty: {
                                          message: 'El valor de la mensualidad es requerido, no puede estar vacío!!!'
                                      },
                                      regexp: {
                                          regexp: /^[0-9]+(\.{1}[0-9]{2})$/,
                                          message: 'El nombre de la carrera tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro tipo de caracter!!!!'
                                      },
                                  }
                              },
                            }
                          });//fin de la validación del formulario

                          $('#pensum').formValidation({
                              message: 'This value is not valid',
                              icon: {
                                  valid: 'glyphicon glyphicon-ok',
                                  invalid: 'glyphicon glyphicon-remove',
                                  validating: 'glyphicon glyphicon-refresh'
                              },
                              fields: {
                                id_plan: {
                                    message: 'El nombre del plan no es valido!!!',
                                    validators: {
                                        notEmpty: {
                                            message: 'El nombre del plan es requerido, no puede estar vacío!!!'
                                        },
                                    }
                                },
                                id_jornada: {
                                    message: 'El nombre de jornada no es valido!!!',
                                    validators: {
                                        notEmpty: {
                                            message: 'El nombre de jornada es requerido, no puede estar vacío!!!'
                                        },
                                    }
                                },
                              }
                            });//fin de la validación del formulario

                              $('#nusuario').formValidation({
                                  message: 'This value is not valid',
                                  icon: {
                                      valid: 'glyphicon glyphicon-ok',
                                      invalid: 'glyphicon glyphicon-remove',
                                      validating: 'glyphicon glyphicon-refresh'
                                  },
                                  fields: {
                                    id_persona: {
                                        message: 'El nombre de la persona no es valido!!!',
                                        validators: {
                                            notEmpty: {
                                                message: 'El nombre de la persona es requerido, no puede estar vacío!!!'
                                            },
                                        }
                                    },
                                    persona: {
                                        message: 'El nombre de la persona no es valido!!!',
                                        validators: {
                                            notEmpty: {
                                                message: 'El nombre de la persona es requerido, no puede estar vacío!!!'
                                            },
                                        }
                                    },
                                    id_rol: {
                                        message: 'El nombre del rol no es valido!!!',
                                        validators: {
                                            notEmpty: {
                                                message: 'El  rol es requerido, no puede estar vacío!!!'
                                            },
                                        }
                                    }
                                  }
                                });//fin de la validación del formulario

                                  var referenciaNombre = {   // The title is placed inside a <div class="col-xs-4"> element
                                  validators: {
                                    notEmpty: {
                                      message: 'El nombre de la referencia es requerido,no puede estar vacío',
                                    },
                                    stringLength: {
                                      max: 150,
                                      message: 'El nombre dela referencia nopuede tener más de 150 caracteres de longitud'
                                    },
                                    regexp: {
                                      regexp: /^[a-zA-ZáéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                      message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                              },
                              referenciaParentesco = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El parentesco de la referencia,es requerido, no puede estar vacío'
                                    },
                                    stringLength: {
                                      max: 50,
                                      message: 'El parenteco no puede tener más de 50 caracteres de longitud'
                                    },
                                    regexp: {
                                      regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                      message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                              },
                              referenciaTelefono = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El número de telefon de la referencia es requerido'
                                    },
                                    digits:{
                                      message: 'El numero de telefono solo puede tner digitos'
                                    },
                                    stringLength: {
                                      max: 10,
                                      message: 'El número de telefono no puede tener más de 10 digitos'
                                    }
                                  }
                              },
                              tutorNombre = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El nombre del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 60,
                                      message: 'El nombre del tutor no puede tener más de 60 caracteres de longitud'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s]+$/,
                                        message: 'El valor no es un nombre valido.'
                                    }
                                  }
                                },
                                tutorApellido = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El apellido del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 60,
                                      message: 'El apellido del tutor no puede tener más de 60 caracteres de longitud'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s]+$/,
                                        message: 'El valor no es un apellido valido.'
                                    }
                                  }
                                },
                                tutorDireccion = {
                                  validators: {
                                    notEmpty: {
                                      message: 'La direción del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 70,
                                      message: 'El apellido del tutor no puede tener más de 70 caracteres de longitud'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\.\-\s\,]+$/,
                                        message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                                },
                                tutorTelefonoPrimario = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El número de telefono es requerido'
                                    },
                                    digits: {
                                      message: 'El número de telefono soo puede contener dígitos'
                                    },
                                    stringLength: {
                                      max: 10,
                                      message: 'El número de telefono no puede tener más de 10 digítos de longitud'
                                    }
                                  }
                                },
                                tutorEmpresaTelefono = {
                                  validators: {
                                    notEmpty: {
                                      message: 'La empresa telefonica esrequerida'
                                    }
                                  }
                                },
                                tutorCorreoElectronico = {
                                  validators: {
                                    notEmpty: {
                                      message: 'La dirección de correo electronico del tutor es requerido'
                                    },
                                    regexp: {
                                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                        message: 'El valor no es una dirección valida de correo.'
                                    }
                                  }
                                },
                                tutorCui = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El número de DPI del tutor es requerido'
                                    },
                                    digits: {
                                      message: 'El número de DPI solo puede contener digítos'
                                    },
                                    stringLength: {
                                      max:13,
                                      min:13,
                                      message: 'El número de DPI tiene que tener 13 dígitos de longitud'
                                    }
                                  }
                                },
                                tutorLugarTrabajo = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El lugar de trabajo del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 100,
                                      message: 'La longitud no puede susperar los 100 caracteres'
                                    },
                                      regexp: {
                                          regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                          message: 'El valor caracteres invalidos.'
                                      }
                                  }
                                },
                                tutorDireccionTrabajo = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El lugar de trabajo del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 100,
                                      message: 'La dirección no puede tener más de 100 caracteres de longitud'
                                    },
                                    regexp: {
                                        regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                        message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                                },
                                tutorTelefonoTrabajo = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El numero de trabajo del tutor es requerido'
                                    },
                                    stringLength: {
                                      max: 10,
                                      message: 'El número de telefono no puede tener más de 10 digitos'
                                    },
                                    digits: {
                                      message: 'El número de telefono solo puede tener digitos'
                                    }
                                  }
                                },
                                tutorEmpresaTelefono = {
                                  validators:{
                                    notEmpty: {
                                      message: 'La empresa telefonica es requerida'
                                    }
                                  }
                                },
                                tutorCargo = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El cargo que ocupa en el tutor en su lugar de trabajo es requerido'
                                    },
                                    stringLength: {
                                      max:50,
                                      message: 'Este campo no puede tener más de 50 caracteres de longitud'
                                    },
                                    regexp: {
                                      regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                      message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                                },
                                tutorParentesco = {
                                  validators: {
                                    notEmpty: {
                                      message: 'El parentesco del tutor es requerido',
                                    },
                                    stringLength: {
                                      max: 50,
                                      message: 'El parentesco no puede tener más de 50 caracteres de longitud'
                                    },
                                    regexp: {
                                      regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s\.\:\-\,\;]+$/,
                                      message: 'El valor contiene caracteres invalidos.'
                                    }
                                  }
                                },

                                referenciaIndex, tutorIndex = 1;
                                $('#inscripcionestudiante').formValidation({
                                    message: 'This value is not valid',
                                    excluded: [':disabled'],
                                    icon: {
                                        valid: 'glyphicon glyphicon-ok',
                                        invalid: 'glyphicon glyphicon-remove',
                                        validating: 'glyphicon glyphicon-refresh'
                                    },
                                    fields: {
                                      id_plan: {
                                          message: 'El nombre del plan no es valido!!!',
                                          validators: {
                                              notEmpty: {
                                                  message: 'El nombre del plan es requerido, no puede estar vacío!!!'
                                              },
                                          }
                                      },
                                      id_jornada: {
                                          message: 'El nombre de la jornada no es valido!!!',
                                          validators: {
                                              notEmpty: {
                                                  message: 'El nombre de la jornada es requerido, no puede estar vacío!!!'
                                              },
                                          }
                                      },
                                      id_nivel: {
                                          message: 'El nombre del nivel no es valido!!!',
                                          validators: {
                                              notEmpty: {
                                                  message: 'El nombre del nivel es requerido, no puede estar vacío!!!'
                                              },
                                          }
                                      },
                                      id_grado: {
                                          message: 'El nombre del grado no es valido!!!',
                                          validators: {
                                              notEmpty: {
                                                  message: 'El nombre del grado es requerido, no puede estar vacío!!!'
                                              },
                                          }
                                      },
                                      nombre_estudiante: {
                                        message: 'El nombre del estudiante no es valido!!!',
                                        validators: {
                                            notEmpty: {
                                                message: 'El nombre del estudiante es requerido, no puede estar vacío!!!'
                                            },
                                            stringLength: {
                                                max: 60,
                                                message: 'El nombre del estudiante debe tener como maximo 60 caracteres de longitud!!!'
                                            },
                                            regexp: {
                                                regexp: /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/,
                                                message: 'El nombre del estudiante tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro tipo de caracter!!!'
                                            },
                                        }
                                      },
                                      apellidos_estudiante: {
                                        message: 'El apellido del estudiante no es valido!!!',
                                        validators: {
                                            notEmpty: {
                                                message: 'El apellido del estudiante es requerido, no puede estar vacío!!!'
                                            },
                                            stringLength: {
                                                max: 60,
                                                message: 'El apellido del estudiante debe tener como maximo 60 caracteres de longitud!!!'
                                            },
                                            regexp: {
                                                regexp: /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/,
                                                message: 'El apellido del estudiante tiene que contener unicamente letras del alfabeto y espacios, sin ningún otro tipo de caracter!!!'
                                            },
                                        }
                                      },
                                      codigo_personal_estudiante: {
                                        message: 'El código del estudiante no es valido!!!',
                                        validators: {
                                            stringLength: {
                                                max: 15,
                                                message: 'El código del estudiante debe tener como maximo 15 caracteres de longitud!!!'
                                            },
                                            regexp: {
                                                regexp: /^[a-z0-9A-ZáéíóúÁÉÍÓÚñÑ\s]+$/,
                                                message: 'El apellido del estudiante tiene que contener unicamente letras del alfabeto, numeros y espacios, sin ningún otro tipo de caracter!!!'
                                            },
                                        }
                                      },
                                      fecha_nacimiento_estudiante: {
                                        validators: {
                                          notEmpty: {
                                              message: 'La fecha de nacimiento del estudiante es requerido, no puede estar vacío!!!'
                                          },
                                          date: {
                                              format: 'YYYY-MM-DD',
                                              message: 'El valor no es una fecha valida'
                                          }
                                        }
                                      },
                                      genero_estudiante: {
                                          validators: {
                                            notEmpty: {
                                              message: 'El genero del estudiante es requerido'
                                            }
                                          }
                                      },
                                      id_departamento: {
                                          validators: {
                                            notEmpty: {
                                              message: 'El departamento es requerido.'
                                            }
                                          }
                                      },
                                      id_municipio: {
                                        validators: {
                                          notEmpty: {
                                            message: 'El municipio es requerido.'
                                          }
                                        }
                                      },
                                      direccion_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'La dirección del estudiante es requerido'
                                          },
                                          stringLength: {
                                            max: 100,
                                            message: 'La dirección del estudiante no puede tener más de 100 caracteres de longitud'
                                          },
                                          regexp: {
                                            regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\s\.\,\;\:\-]+$/,
                                            message: 'La dirección solopuede contener letras del alfabeto, numeros, ., ;, ,, :, -, ningún otro caracter'
                                          }
                                        }
                                      },
                                      colonia_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'La colonia del estudiante es requerido'
                                          },
                                          stringLength: {
                                            max:100,
                                            message: 'La colonia no puede tener más de 100 caracteres de longitud'
                                          },
                                          regexp: {
                                            regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\s\.\,\;\:\-]+$/,
                                            message: 'La dirección solopuede contener letras del alfabeto, numeros, ., ;, ,, :, -, ningún otro caracter'
                                          }
                                        }
                                      },
                                      zona_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'La zona del estudiante es requerida'
                                          }
                                        }
                                      },
                                      telefono_casa: {
                                        validators: {
                                          notEmpty: {
                                            message: 'El número de telefono es requerido'
                                          },
                                          digits: {
                                            message: 'El número de telefono solo puede tener digitos'
                                          },
                                          stringLength: {
                                            max:8,
                                            min:8,
                                            message: 'El número de telefono tiene que tener una longitud de 8 digitos'
                                          }
                                        }
                                      },
                                      empresa_telefonica: {
                                        validators: {
                                          notEmpty: {
                                            message: 'La empresa telefonica es requerida'
                                          }
                                        }
                                      },
                                      correo_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'El correo del estudiante es requerido'
                                          },
                                          emailAddress: {
                                            message: 'EL valor no es una dirección valida de correo'
                                          },
                                          regexp: {
                                              regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                              message: 'El valor no es una dirección valida de correo.'
                                          }
                                        }
                                      },
                                      enfermedad_padecida_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'Las enfermedades padecidas por el estudiante son requeridas'
                                          },
                                          stringLength: {
                                            max: 1000,
                                            message: 'Este campo no puede puede tener más de 1000 caracteres'
                                          }
                                        }
                                      },
                                      medicamento_recomendado_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'Los medicamentos recomendados para el estudiante son requeridos'
                                          },
                                          stringLength: {
                                            max: 1000,
                                            message: 'Este campo no puede puede tener más de 1000 caracteres'
                                          }
                                        }
                                      },
                                      alergico_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'Las alergias amedicamentos o sustancias son requeridos'
                                          },
                                          stringLength: {
                                            max: 500,
                                            message: 'Este campo no puede puede tener más de 500 caracteres'
                                          }
                                        }
                                      },
                                      hospital_estudiante: {
                                        validators: {
                                          notEmpty:{
                                            message: 'El hospital es requerido, no puede testar vacío'
                                          },
                                          stringLength: {
                                            max: 100,
                                            message: 'Este campo no puede tener más de 100 caracteres de longitud'
                                          }
                                        }
                                      },
                                      tipo_sangre_estudiante: {
                                        validators: {
                                          notEmpty:{
                                            message: 'El tipo de sangre esrequerido'
                                          }
                                        }
                                      },
                                      observaciones_estudiante: {
                                        validators: {
                                          notEmpty: {
                                            message: 'Las observaciones son requeridas'
                                          },
                                          stringLength: {
                                            max: 1000,
                                            message: 'Las observaciones no pueden tener más de 1000 caracteres de longitud'
                                          }
                                        }
                                      },

                                      'tutor[1][nombre]': tutorNombre,
                                      'tutor[1][apellidos]': tutorApellido,
                                      'tutor[1][direccion]': tutorDireccion,
                                      'tutor[1][telefono_primario]': tutorTelefonoPrimario,
                                      'tutor[1][empresa_telefono]': tutorEmpresaTelefono,
                                      'tutor[1][correo_electronico]': tutorCorreoElectronico,
                                      'tutor[1][cui]': tutorCui,
                                      'tutor[1][lugar_trabajo]': tutorLugarTrabajo,
                                      'tutor[1][direccion_trabajo]': tutorDireccionTrabajo,
                                      'tutor[1][telefono_trabajo]': tutorTelefonoTrabajo,
                                      'tutor[1][empresa_telefono_trabajo]': tutorEmpresaTelefono,
                                      'tutor[1][cargo]': tutorCargo,
                                      'tutor[1][parentesco]': tutorParentesco,
                                      'referencia[1][nombre]': referenciaNombre,
                                      'referencia[1][parentesco]': referenciaParentesco,
                                      'referencia[1][telefono]': referenciaTelefono,

                                    }
                                  })// Add button click handler
                                  .on('click', '#agregarTutor', function() {
                                      tutorIndex++;
                                      var $template = $('#optionTemplate'),
                                          $clone    = $template
                                                          .clone()
                                                          .removeClass('hide')
                                                          .removeAttr('id')
                                                          .attr('data-book-index', referenciaIndex)
                                                          .insertBefore($template);
                                                          // Update the name attributes
                                      $clone
                                          .find('[name="nombre"]').attr('name', 'tutor[' + tutorIndex + '][nombre]').end()
                                          .find('[name="apellidos"]').attr('name', 'tutor[' + tutorIndex + '][apellidos]').end()
                                          .find('[name="direccion"]').attr('name', 'tutor[' + tutorIndex + '][direccion]').end()
                                          .find('[name="telefono_primario"]').attr('name', 'tutor[' + tutorIndex + '][telefono_primario]').end()
                                          .find('[name="empresa_telefono"]').attr('name', 'tutor[' + tutorIndex + '][empresa_telefono]').end()
                                          .find('[name="correo_electronico"]').attr('name', 'tutor[' + tutorIndex + '][correo_electronico]').end()
                                          .find('[name="cui"]').attr('name', 'tutor[' + tutorIndex + '][cui]').end()
                                          .find('[name="lugar_trabajo"]').attr('name', 'tutor[' + tutorIndex + '][lugar_trabajo]').end()
                                          .find('[name="direccion_trabajo"]').attr('name', 'tutor[' + tutorIndex + '][direccion_trabajo]').end()
                                          .find('[name="telefono_trabajo"]').attr('name', 'tutor[' + tutorIndex + '][telefono_trabajo]').end()
                                          .find('[name="empresa_telefono_trabajo"]').attr('name', 'tutor[' + tutorIndex + '][empresa_telefono_trabajo]').end()
                                          .find('[name="cargo"]').attr('name', 'tutor[' + tutorIndex + '][cargo]').end()
                                          .find('[name="parentesco"]').attr('name', 'tutor[' + tutorIndex + '][parentesco]').end();
                                      // Add new field
                                      $('#inscripcionestudiante')
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][nombre]', tutorNombre)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][apellidos]', tutorApellido)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][direccion]', tutorDireccion)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][telefono_primario]', tutorTelefonoPrimario)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][empresa_telefono]', tutorEmpresaTelefono)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][correo_electronico]', tutorCorreoElectronico)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][cui]', tutorCui)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][lugar_trabajo]', tutorLugarTrabajo)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][direccion_trabajo]', tutorDireccionTrabajo)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][telefono_trabajo]', tutorTelefonoTrabajo)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][empresa_telefono_trabajo]', tutorEmpresaTelefono)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][cargo]', tutorCargo)
                                                                .formValidation('addField', 'tutor[' + tutorIndex + '][parentesco]', tutorParentesco);



                                  }).on('click', '#agregarReferencia', function() {

                                      referenciaIndex++;
                                     var $template = $('#referenciaTemplate'),
                                         $clone    = $template
                                                         .clone()
                                                         .removeClass('hide')
                                                         .removeAttr('id')
                                                         .attr('data-book-index', referenciaIndex)
                                                         .insertBefore($template);

                                     // Update the name attributes
                                     $clone
                                         .find('[name="nombre"]').attr('name', 'referencia[' + referenciaIndex + '][nombre]').end()
                                         .find('[name="parentesco"]').attr('name', 'referencia[' + referenciaIndex + '][parentesco]').end()
                                         .find('[name="telefono"]').attr('name', 'referencia[' + referenciaIndex + '][telefono]').end();

                                     // Add new fields
                                     // Note that we also pass the validator rules for new field as the third parameter
                                     $('#inscripcionestudiante')
                                         .formValidation('addField', 'referencia[' + referenciaIndex + '][nombre]', referenciaNombre)
                                         .formValidation('addField', 'referencia[' + referenciaIndex + '][parentesco]', referenciaParentesco)
                                         .formValidation('addField', 'referencia[' + referenciaIndex + '][telefono]', referenciaTelefono);


                                    })
                                                              // Called when a field is invalid
                                  .on('err.field.fv', function(e, data) {
                                      // data.element --> The field element

                                      var $tabPane = data.element.parents('.tab-pane'),
                                          tabId    = $tabPane.attr('id');

                                      $('a[href="#' + tabId + '"][data-toggle="tab"]')
                                          .parent()
                                          .find('i')
                                          .removeClass('fa-check')
                                          .addClass('fa-times');
                                  })
                                    // Called when a field is valid
                                    .on('success.field.fv', function(e, data) {

                                        var $tabPane = data.element.parents('.tab-pane'),
                                            tabId    = $tabPane.attr('id'),
                                            $icon    = $('a[href="#' + tabId + '"][data-toggle="tab"]')
                                                        .parent()
                                                        .find('i')
                                                        .removeClass('fa-check fa-times ');

                                        // Check if all fields in tab are valid
                                        var isValidTab = data.fv.isValidContainer($tabPane);
                                        if (isValidTab !== null) {
                                            $icon.addClass(isValidTab ? 'fa-check' : 'fa-times');
                                        }
                                    });//fin de la validación del formulario

                                    $('#unidades').formValidation({
                                        icon: {
                                            valid: 'glyphicon glyphicon-ok',
                                            invalid: 'glyphicon glyphicon-remove',
                                            validating: 'glyphicon glyphicon-refresh'
                                        },
                                        fields: {
                                          nombre_unidad: {
                                              validators: {
                                                  notEmpty: {
                                                      message: 'El nombre de la unidad es requerido, no puede estar vacío!!!'
                                                  },
                                                  regexp: {
                                                    regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ\s]+$/,
                                                    message: 'El nombre de la unidad contiene caracteres invalidos!!!'
                                                  }
                                              }
                                          },
                                          fecha_inicio: {
                                              validators: {
                                                  notEmpty: {
                                                      message: 'La fecha de inicio de la unidad es requerido, no puede estar vacío!!!'
                                                  },
                                                  date: {
                                                    format: 'YYYY-MM-DD',
                                                    message: 'El formado de la fecha es invalido, tiene que ser año-mes-día'
                                                  }
                                              }
                                          },
                                          fecha_final: {
                                              validators: {
                                                  notEmpty: {
                                                      message: 'La fecha de fin de unidad es requerido, no puede estar vacío!!!'
                                                  },
                                                  date: {
                                                    format: 'YYYY-MM-DD',
                                                    message: 'El formato de la fecha es invalido, tiene que ser año-mes-día'
                                                  }
                                              }
                                          }
                                        }
                                      });//fin de la validación del formulario

                            $('#foros').formValidation({
                              icon: {
                                  valid: 'glyphicon glyphicon-ok',
                                  invalid: 'glyphicon glyphicon-remove',
                                  validating: 'glyphicon glyphicon-refresh'
                              },
                              fields: {
                                titulo_foro: {
                                  validators: {
                                    notEmpty: {
                                      message: 'El titulo del foro es requerido, no puede estar vacío!!!'
                                    },
                                    regexp: {
                                      regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚ?¿!¡\s]+$/,
                                      message: 'El titulo del foro contiene caracteres invalidos!!!'
                                    }
                                  }
                                },
                                mensaje_foro: {
                                  validators: {
                                    notEmpty: {
                                      message: 'La descripción del foro es requerido, no puede estar vacío!!!'
                                    }
                                  }
                                }
                              }
                            });

});//fin del document ready
